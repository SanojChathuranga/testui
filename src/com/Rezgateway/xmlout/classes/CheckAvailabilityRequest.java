package com.Rezgateway.xmlout.classes;

public class CheckAvailabilityRequest {

	String	Hotel_ID		= "";
	String	Transaction_ID	= "";
	String	Supplier_ID		= "";
	String	Room_1			= "";
    String	Room_2			= "";
    String	Room_3			= "";
    String	Room_4			= "";
    
    
	public String getHotel_ID() {
		return Hotel_ID;
	}
	public void setHotel_ID(String hotel_ID) {
		Hotel_ID = hotel_ID;
	}
	public String getTransaction_ID() {
		return Transaction_ID;
	}
	public void setTransaction_ID(String transaction_ID) {
		Transaction_ID = transaction_ID;
	}
	public String getSupplier_ID() {
		return Supplier_ID;
	}
	public void setSupplier_ID(String supplier_ID) {
		Supplier_ID = supplier_ID;
	}
	public String getRoom_1() {
		return Room_1;
	}
	public void setRoom_1(String room_1) {
		Room_1 = room_1;
	}
	public String getRoom_2() {
		return Room_2;
	}
	public void setRoom_2(String room_2) {
		Room_2 = room_2;
	}
	public String getRoom_3() {
		return Room_3;
	}
	public void setRoom_3(String room_3) {
		Room_3 = room_3;
	}
	public String getRoom_4() {
		return Room_4;
	}
	public void setRoom_4(String room_4) {
		Room_4 = room_4;
	}
    
    
}
