package com.Rezgateway.main;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_HotelAvailRQ;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_HotelAvailRS;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_HotelSearchRQ;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_HotelSearchRS;
import auto.xmlout.hotel.loaders.PropertySingleton;
import auto.xmlout.hotel.pojo.GeneratedAvailabilityResponse;
import auto.xmlout.hotel.pojo.GeneratedResponse;
import auto.xmlout.hotel.pojo.Hotel;
import auto.xmlout.hotel.pojo.RoomType;
import auto.xmlout.hotel.pojo.SearchType;
import auto.xmlout.hotel.processors.ResponseProcessors;
import auto.xmlout.hotel.reqbuilders.HotelRequestBuilder;
import auto.xmlout.hotel.reqbuilders.ReservationStubBuilder;

public class TestServlet extends HttpServlet {
	
	
	Logger logger = Logger.getLogger(this.getClass());
	public TestServlet() {
		DOMConfigurator.configure("log4j.xml");
	}
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		String	userAgent		= req.getHeader("user-Agent");
		String	clientBrowser	= "Not known";
		if( userAgent != null)
		{
			clientBrowser = userAgent;
			req.setAttribute("client.browser",clientBrowser );
			//req.getRequestDispatcher("/AppHome.jsp").forward(req,resp);
		}
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		
		if(request.getParameter("SaveProp") != null)
		{
			
			String PortalCurrency = request.getParameter("PortalCurrency");
			String AXIS_CONFIG_PATH = request.getParameter("AXIS_CONFIG_PATH");
			String AXIS_XML_PATH = request.getParameter("AXIS_XML_PATH");
			String EchoToken = request.getParameter("EchoToken");
			String MaxResponses = request.getParameter("MaxResponses");
			String MinResponses = request.getParameter("MinResponses");
			String Server = request.getParameter("Server");
			String Url = request.getParameter("Url");
			String UserName = request.getParameter("UserName");
			String Password = request.getParameter("Password");
			String CompanyName = request.getParameter("CompanyName");
			String AgentID = request.getParameter("AgentID");
			
			System.out.println(PortalCurrency);
			
		}
		if(request.getParameter("Hotel_Search") != null)
		{
			
		    ReservationServiceStub			stub			= null;
			SearchType						search			= new SearchType();
			String							message			= null;
			GeneratedResponse				ServiceResponse	= null;
			String							availability	= null;
		  
			String		location				= "-";
			String		arrival					= "-";
			String		departure				= "-";
			String		rooms					= "-";
			String[]	adults					= new String[10];
			String[]	children				= new String[10];
			Map<Integer, String[]> childAges	= new HashMap<Integer, String[]>();
			String		nights					= "-";
			
			rooms			= request.getParameter("H_cmbNoOfRooms");
			System.out.println(rooms);
			
			location		= request.getParameter("H_Loc");
			System.out.println(location);
			
			int	intRooms = Integer.parseInt(rooms);
			for(int y=1; y<=intRooms; y++)
			{
				adults[y-1] = request.getParameter("R"+y+"occAdults");
			}
			System.out.println(adults);
			
			for(int y=1; y<=intRooms; y++)
			{
				children[y-1] = request.getParameter("R"+y+"occChi");
			}
			System.out.println(children);
			
			try {
				for(int u=1; u<=children.length; u++)
				{
					int c = Integer.parseInt(children[u-1]);
					String[] age = new String[10];
					for(int h = 1; h<=c; h++)
					{
						age[h-1]= request.getParameter("R"+u+"occAge"+h+"");
					}
					childAges.put(u, age);
				}
			} catch (Exception e) {
				
			}
			System.out.println(childAges);
			
			nights = request.getParameter("H_nights");
			System.out.println("Nights : "+nights);
			
			String date = request.getParameter("hotel-arrival-date");
			System.out.println("Date : "+date);
			arrival = date.split("-")[0].trim();
			System.out.println("Arrival : "+arrival);
			departure = date.split("-")[1].trim();
			System.out.println("Departure: "+departure);
			String cityCode = request.getParameter("hid_H_Loc");
			System.out.println("CODE: "+cityCode);
			
			
			//SET SEARCH DATA TO SEARCHTYPE OBJECT
			search.setLocation(location);
			search.setRooms(rooms);
			search.setAdults(adults);
			search.setChildren(children);
			search.setChildAges(childAges);
			search.setBookingDate(date);
			search.setCityCode(cityCode.split(",")[0].trim());
			//TODO: Change this code segment and call the service properly
			//==================================================================================================//
			
			System.setProperty("javax.net.ssl.trustStore", "C:/rezsystem/jboss-4.0.3SP1/bin/certs/QA.keystore");
			System.setProperty("javax.net.ssl.trustStorePassword", "123456");
			
			//String message   = null;
			try {
				String Url = PropertySingleton.getInstance().getProperty("XmlOut.Url");
				String Username = PropertySingleton.getInstance().getProperty("XmlOut.UserName");
				String Pass = PropertySingleton.getInstance().getProperty("XmlOut.PassWord");

				//logger.info("Building Reservation Stub using  Url---->"+Url+" User--->"+Username +" Pass--->"+Pass);
				
				//logger.info("============Building Reservation Stub=====================");
				ReservationStubBuilder StubBuilder = new ReservationStubBuilder(Url, Username, Pass);
				try {
					stub = StubBuilder.buildReservationServiceStub();
				} catch (Exception e) {
				    //logger.fatal("Reservation Stub build failed - Treminating the test suit", e);
				    throw new Exception();
				}
				
				HotelRequestBuilder		ReqBuilder	= new HotelRequestBuilder();
				OTA_HotelSearchRQ       Search    	= null;
				ResponseProcessors      Processor 	= new ResponseProcessors();
				// Search
				
				if(search.isScenarioExcecutionState()){
					//logger.info("================= Building the search request=============== ");
					try {
						 Search = ReqBuilder.buildHotelSearchRequest(search);
					} catch (Exception e) {
					    //logger.fatal("Reservation Stub build failed - Treminating the test suit", e);
				        throw new Exception();
					}
					
					//logger.info("================= Sending  the search request=============== ");
					
					OTA_HotelSearchRS SearchRS = stub.HotelSearch(Search);
					
					//logger.info("================= Processing the response=============== ");
					
					ServiceResponse = Processor.processSearchResponse(SearchRS);
					
					//===================================================================================//
					
					StringBuffer  outputbuffer = new StringBuffer();
					
					String AvailabilityMessage = ServiceResponse.isErrorResponse() ? ServiceResponse.getErrorLog() : "No of Hotels : "+ServiceResponse.getNumberOfHotels() + "  " + "Transaction Id : " +ServiceResponse.getTransactionIdentifier();
					outputbuffer.append("<br><p><b><center>"+AvailabilityMessage+"</center></b></p>") ;
					
					if(ServiceResponse.isResultsAvailable()){
						ArrayList<Hotel>  ResultsHotelList   = ServiceResponse.getResponseHotelList();	
					    search.setAvailabilityTracer(ServiceResponse.getTransactionIdentifier().getStringLength1To128());
						//System.out.println(ResultsHotelList.size());
					    
					    HttpSession session = request.getSession();
					    session.setAttribute("PerformedSearch",search);
					    session.setAttribute("HotelList",ResultsHotelList);
						
						outputbuffer.append("<center>===========================================================</center><br>");
						int count = 1;
						//outputbuffer.append(b);
						
						for (Iterator<Hotel> iterator = ResultsHotelList.iterator(); iterator
								.hasNext();) {
							
							Hotel hotel = (Hotel) iterator.next();
							//outputbuffer.append("<br>*************************************************<br>");
							//outputbuffer.append("<input type="+checkbox+" name="+"hotel_"+count+""+" value="+"hotel_"+count+""+" onchange="+jscript+"> HOTEL "+count+"");
							//outputbuffer.append("<input type="+checkbox+" name="+Hname+" onclick="+jscript+" id="+"hotel_"+count+""+"> HOTEL "+count+"");
							outputbuffer.append("<br> Hotel Name		 : "+hotel.getHotelName());
							outputbuffer.append("<br> Hotel Code		 : "+hotel.getHotelCode());
							outputbuffer.append("<br> Hotel Vendor		 : "+hotel.getHotelVendor());
							outputbuffer.append("<br> Hotel Supplier	 : "+hotel.getSupplier());
							outputbuffer.append("<br> Hotel Address		 : "+hotel.getAddressLine1());
							outputbuffer.append("<br> Hotel City		 : "+hotel.getCity());
							outputbuffer.append("<br> Hotel State		 : "+hotel.getState());
							outputbuffer.append("<br> Hotel Country		 : "+hotel.getCountry());
							outputbuffer.append("<br> Hotel Star Rate	 : "+hotel.getStarCategory()+"<br>");
							
							Map<String, ArrayList<RoomType>> ResultsRooms = new HashMap<String, ArrayList<RoomType>>();
							ResultsRooms = hotel.getResultsRooms();
							outputbuffer.append("<br><b>======Room Details======</b>");
							for (int i = 0; i < ResultsRooms.size(); i++) {
								
								outputbuffer.append("<br>Room : "+(i+1));
								ArrayList<RoomType> roomlist = ResultsRooms.get(String.valueOf(i+1));
								
								for (int j = 0; j < roomlist.size(); j++) {
									
									RoomType r = roomlist.get(j);
									
									outputbuffer.append("<br>Room Code     : "+r.getRoomCode());
									outputbuffer.append("<br> Room Type     : "+r.getRoomType());
									outputbuffer.append("<br> Bed Type      : "+r.getBedType());
									outputbuffer.append("<br> Rate Type     : "+r.getRateType());
									outputbuffer.append("<br> Standard Fee  : "+r.getStdFee());
									outputbuffer.append("<br> No show Fee   : "+r.getNoShowFee());
									outputbuffer.append("<br> Total Rate    : "+r.getTotalRate());
									outputbuffer.append("<br> Currency      : "+r.getCurrency()+"<br>");
								}
							}
							
							outputbuffer.append("<br>*************************************************<br>");
							count = count + 1;
						}
						
						outputbuffer.append("<br>====================================================");
					}
					
					message = outputbuffer.toString();
					System.out.println(message);
				}
				} catch (Exception e) {
				
				e.printStackTrace();
			}
			
			
			//==================================================================================================//
			//JSONObject json      = new JSONObject();
			//json.put("message", message);
			
			//response.setContentType("text/html");
			request.setAttribute("message", message);
			
			request.getRequestDispatcher("Results.jsp").forward(request, response);
			//response.getWriter().write(json.toString());
			
			
		}
		if (request.getParameter("HCheck") != null) {
			
			StringBuffer  outputbuffer              = new StringBuffer();
			String strindex                         = request.getParameter("Hotel_ID");
			int index                               = Integer.parseInt(strindex) - 1;
			ReservationServiceStub	stub2			= null;
			
			HttpSession session = request.getSession();
			SearchType   CurrentSearch              =  (SearchType) session.getAttribute("PerformedSearch");
			ArrayList<Hotel>  ResultsHotelList      =  (ArrayList<Hotel>) session.getAttribute("HotelList");
			Hotel SelectedHotel = null;
		
			try {
			 SelectedHotel   = ResultsHotelList.get(index);
			} catch (Exception e) {
				System.out.println("selected index not available in the results set");
			}
			System.setProperty("javax.net.ssl.trustStore", "C:/rezsystem/jboss-4.0.3SP1/bin/certs/QA.keystore");
			System.setProperty("javax.net.ssl.trustStorePassword", "123456");
			
			if(SelectedHotel != null){
				try {

					
					HotelRequestBuilder ReqBuilder = new HotelRequestBuilder();
					
					String Url = PropertySingleton.getInstance().getProperty("XmlOut.Url");
					String Username = PropertySingleton.getInstance().getProperty("XmlOut.UserName");
					String Pass = PropertySingleton.getInstance().getProperty("XmlOut.PassWord");

					logger.info("Building Reservation Stub using  Url---->" + Url + " User--->" + Username + " Pass--->" + Pass);

					logger.info("============Building Reservation Stub=====================");
					ReservationStubBuilder StubBuilder = new ReservationStubBuilder(Url, Username, Pass);
					try {
						stub2 = StubBuilder.buildReservationServiceStub();
					} catch (Exception e) {
						logger.fatal("Reservation Stub build failed - Treminating the test suit", e);
						throw new Exception();
					}
					
					
					OTA_HotelAvailRS oTA_HotelAvailRS = new OTA_HotelAvailRS();
					OTA_HotelAvailRQ oTA_HotelAvailRQ = new OTA_HotelAvailRQ();
					
					CurrentSearch.setHotelID(SelectedHotel.getHotelCode().trim());
					CurrentSearch.setHotelVendor(SelectedHotel.getHotelVendor());
					
					   Map<String, ArrayList<RoomType>> AvailableRooms = SelectedHotel.getResultsRooms();
						
					   Iterator<Map.Entry<String,ArrayList<RoomType>>>  itr = AvailableRooms.entrySet().iterator();
					   while (itr.hasNext()) {
						Map.Entry<java.lang.String, java.util.ArrayList<auto.xmlout.hotel.pojo.RoomType>> entry = (Map.Entry<java.lang.String, java.util.ArrayList<auto.xmlout.hotel.pojo.RoomType>>) itr.next();
						String RoomNumber          = entry.getKey().trim();
						int incr = (entry.getValue().size()==1) ? 0: new Random().nextInt(entry.getValue().size()-1);
						String SelectedCode        = entry.getValue().get(incr).getRoomCode();
						CurrentSearch.addToSelectedRoomCodes(RoomNumber,SelectedCode);
					   }
					
					logger.info("================= Building the availability request=============== ");
					try {
						oTA_HotelAvailRQ = ReqBuilder.buildHotelAvailRequest(CurrentSearch);
					} catch (Exception e) {
						logger.fatal("Reservation Stub build failed - Treminating the test suit", e);
						throw new Exception();
					}

					logger.info("================= Sending  the search request====================== ");

					oTA_HotelAvailRS = stub2.HotelAvailability(oTA_HotelAvailRQ);
					
					ResponseProcessors      Processor 	                        = new ResponseProcessors();
					GeneratedAvailabilityResponse ProcessedAvailabilityResponse = Processor.processAvailabilityResponse(oTA_HotelAvailRS);
					
					
					System.out.println("Hotel Availability ----->" + ProcessedAvailabilityResponse.isHotelAvailable());
					System.out.println("Hotel Transaction ID ----->" + ProcessedAvailabilityResponse.getTransactionIdentifier());
					System.out.println("Available Hotel Name ----->" + ProcessedAvailabilityResponse.getAvailableHotel().getHotelName());
					
					Hotel AvailableHotel = ProcessedAvailabilityResponse.getAvailableHotel();
					outputbuffer.append("<p>Transaction Identifier : "+ProcessedAvailabilityResponse.getTransactionIdentifier()+"</p><br>");
					outputbuffer.append("<p>Hotel Name : "+AvailableHotel.getHotelName()+"</p><br>");
					outputbuffer.append("<p>Hotel Code : "+AvailableHotel.getHotelCode()+"</p><br>");
					outputbuffer.append("<p>Hotel Vendor : "+AvailableHotel.getHotelVendor()+"</p><br>");
					
					String availability = outputbuffer.toString();
				    request.setAttribute("availability", availability);
					request.getRequestDispatcher("AvailabilityResults.jsp").forward(request, response);
					
					
				
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			
			
		    
		}
		if(request.getParameter("Flight_Search") != null)
		{
			StringBuffer	outputbuffer	= new StringBuffer();
			SearchType		search			= new SearchType();
			
			String	tripType			= "-";
			boolean	isFlexible			= false;
			String	from				= "-";
			String	to					= "-";
			String	departuredate		= "-";
			String	returndate			= "-";
			String	depTime				= "-";
			String	retTime				= "-";
			String	dep_flexx			= "-";
			String	ret_flexx			= "-";
			String	adults				= "-";
			String	children			= "-";
			String	infants				= "-";
			ArrayList<String> childAges	= new ArrayList<String>();
			String	cabin				= "-";
			
			String flex = request.getParameter("isflex");
			if ("exact".equals(flex)) {
		    	isFlexible = false;
		    }
		    if ("flexible".equals(flex)) {
		    	isFlexible = true;
		    }
			
			String q2 = request.getParameter("Air_TripType");
			
		    if ("roundtrip".equals(q2)) {
		    	tripType = "Round trip";
		    }
		    if ("oneway".equals(q2)) {
		    	tripType = "One way";
		    }
		    if ("multi".equals(q2)) {
		    	tripType = "Multi Destination";
		    }
		    
		    from	= request.getParameter("air_Loc_mul_0");
		    to		= request.getParameter("air_Loc1_mul_0");
		    String dateElement = "";
		    
		    if(tripType.equals("Round trip"))
		    {
		    	dateElement = "flightdateRT";
		    }
		    if(tripType.equals("One way"))
		    {
		    	dateElement = "flightdateOW";
		    }
		    
		    String date = request.getParameter(dateElement);
		    if(tripType.equals("Round trip"))
		    {
		    	departuredate	= date.split("-")[0].trim();
		    	returndate		= date.split("-")[1].trim();
		    }
		    else if(tripType.equals("One way"))
		    {
		    	departuredate	= date.trim();
		    }
		    
		    if(!isFlexible)
		    {
		    	depTime = request.getParameter("Air_DepTime_0");
			    retTime = request.getParameter("Air_RetTime_0");
		    }
		    if(isFlexible)
		    {
		    	dep_flexx = request.getParameter("dep_flexx");
		    	ret_flexx = request.getParameter("ret_flexx");
		    }
		    
		    String cabinClass	= request.getParameter("Aira_FlightClass");
	    	cabin = cabinClass;
	    	
	    	adults		= request.getParameter("Air_cmbNoOfAdults");
	    	children	= request.getParameter("Air_cmbNoOfChildren");
	    	infants		= request.getParameter("Air_cmbNoOfInfants");
	    	
			for(int c=1; c<=Integer.parseInt(children); c++)
			{	
				int h	= 2;
				h		= h + c;
				if(h<=11)
				{
					childAges.add(String.valueOf(h));
				}
				else
				{
					h	= 2;
					childAges.add(String.valueOf(h));
				}
			}
		    
		    System.out.println("Trip Type : "+tripType);
		    System.out.println("From : "+from);
		    System.out.println("To : "+to);
		    System.out.println("Departure date : "+departuredate);
		    System.out.println("Return date : "+returndate);
		    System.out.println("depTime : "+depTime);
		    System.out.println("retTime : "+retTime);
		    System.out.println("dep_flexx : "+dep_flexx);
		    System.out.println("ret_flexx : "+ret_flexx);
		    System.out.println("Cabin : "+cabin);
		    System.out.println("ret_flexx : "+ret_flexx);
		    System.out.println("adults : "+adults);
		    System.out.println("children : "+children);
		    for(int b=0; b<childAges.size(); b++)
		    {
		    	System.out.println("Child "+b+" : "+childAges.get(b));
		    }
		    System.out.println("infants : "+infants);
		    
		    
		}
		if (request.getParameter("FCheck") != null) {
			
			StringBuffer  outputbuffer = new StringBuffer();
			
			String	Flight_ID		= request.getParameter("Flight_ID");
		    
		    System.out.println("=============================");
		    System.out.println("Flight Index: "+Flight_ID);
		    System.out.println("=============================");
		}
		if(request.getParameter("Activity_Search") != null)
		{
			StringBuffer  outputbuffer = new StringBuffer();
			
			String	country					= "-";
			String	location				= "-";
			String	checkInFrom				= "-";
			String	checkInTo				= "-";
			String	nights					= "-";
			String	adults					= "-";
			String	children				= "-";
			ArrayList<String> childrenAge	= new ArrayList<String>();
			
			country		= request.getParameter("A_Country");
			location	= request.getParameter("activity_Loc");
			String date	= request.getParameter("act-chk-in-date");
			checkInFrom	= date.split("-")[0].trim();
			checkInTo	= date.split("-")[1].trim();
			nights		= request.getParameter("activity_nights");
			adults		= request.getParameter("R1occAdults_A");
			children	= request.getParameter("R1occChi_A");
			try {
				for(int u=1; u<=Integer.parseInt(children); u++)
				{
					String age = request.getParameter("R1occAge"+u+"_A");
					System.out.println("Age : "+age);
					childrenAge.add(age);
				}
			} catch (Exception e) {
				
			}
			
			System.out.println("=============================================");
			System.out.println("Country: "+country);
			System.out.println("location: "+location);
			System.out.println("date: "+date);
			System.out.println("checkInFrom: "+checkInFrom);
			System.out.println("checkInTo: "+checkInTo);
			System.out.println("nights: "+nights);
			System.out.println("adults: "+adults);
			System.out.println("children: "+children);
			System.out.println("age size: "+childrenAge.size());
			
			
		}
		if(request.getParameter("ACheck") != null)
		{
			StringBuffer outputbuffer = new StringBuffer();
			
			String Activity_ID	= request.getParameter("Activity_ID");
			
			System.out.println("=============================");
		    System.out.println("Activity Index: "+Activity_ID);
		    System.out.println("=============================");
		    
		}
		
		if(request.getParameter("Reservation") != null)
		{
			boolean	isOnline			= false;
			
			String flex = request.getParameter("PayType");
			if ("Online".equals(flex)) {
				isOnline = true;
		    }
		    if ("Offline".equals(flex)) {
		    	isOnline = false;
		    }
		    
		    System.out.println(isOnline);
		}
	}	
	
}
