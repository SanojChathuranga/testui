package auto.xmlout.hotel.processors;
import auto.xmlout.hotel.pojo.GeneratedAvailabilityResponse;
import auto.xmlout.hotel.pojo.GeneratedResponse;
import auto.xmlout.hotel.pojo.Hotel;
import auto.xmlout.hotel.pojo.RoomType;
import auto.xmlout.hotel.pojo.XmlOutError;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.ErrorType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.ErrorsType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.HotelStay_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_HotelAvailRS;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_HotelAvailRSChoice_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_HotelSearchRS;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.Property_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.RoomStay_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.RoomTypeType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.RoomTypes_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.TPA_ExtensionsType;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.VendorMessageType;

public class ResponseProcessors {
	
	
public GeneratedResponse processSearchResponse(OTA_HotelSearchRS response) {
		GeneratedResponse ExtractedResponse = new GeneratedResponse();
		Property_type0[] Property_type0 = null;
		try {
			Property_type0 = response.getOTA_HotelSearchRSChoice_type0()
					.getOTA_HotelSearchRSSequence_type0().getProperties()
					.getProperty();
			ExtractedResponse.setResultsAvailable(Property_type0.length > 0);
			ExtractedResponse.setTransactionIdentifier(response
					.getTransactionIdentifier());
			ExtractedResponse.setNumberOfHotels(Property_type0.length);

		} catch (Exception e) {
			ExtractedResponse.setErrorResponse(true);
			ExtractedResponse.setErrorLog(e.toString());

		}

		if (ExtractedResponse.isResultsAvailable()) {
			try {
				for (int i = 0; i < Property_type0.length; i++) {

					Property_type0 CurrentProperty = Property_type0[i];
					Hotel CurrentHotel = new Hotel();
					CurrentHotel.setHotelCode(CurrentProperty.getHotelCode()
							.toString().trim());
					CurrentHotel.setHotelName(CurrentProperty.getHotelName()
							.toString().trim());
					CurrentHotel.setHotelVendor(CurrentProperty.getBrandCode()
							.toString().trim());
					CurrentHotel.setAddressLine1(CurrentProperty.getAddress()
							.getAddressLine().toString());
					CurrentHotel.setCity(CurrentProperty.getAddress()
							.getCityName().toString().trim());
					CurrentHotel
							.setHotelCurrency(CurrentProperty.getRateRange()
									.getCurrencyCode().toString().trim());
					CurrentHotel.setLatitude(CurrentProperty.getPosition()
							.getLatitude().toString().trim());
					CurrentHotel.setLongitude(CurrentProperty.getPosition()
							.getLongitude().toString().trim());
					try {
						CurrentHotel.setStarCategory(CurrentProperty.getAward()[0].getRating());
					} catch (Exception e) {
						// TODO Auto-generated catch block
						CurrentHotel.setStarCategory("N/A");
					}

					TPA_ExtensionsType extentions = CurrentProperty
							.getTPA_Extensions();
					RoomTypes_type0 rooms = extentions.getRoomTypes();
					RoomTypeType[] types = rooms.getRoomType();

					for (int j = 0; j < types.length; j++) {

						RoomTypeType CurrentRoom = types[j];
						String RoomTypeId = CurrentRoom.getRoomID().toString()
								.trim();
						RoomType ResultsRoom = new RoomType();
						ResultsRoom.setRoomType(CurrentRoom.getRoomType()
								.toString().trim());
						ResultsRoom.setRoomCode(CurrentRoom.getRoomTypeCode()
								.toString());
						ResultsRoom
								.setTotalRate(CurrentRoom
										.getAdditionalDetails()
										.getAdditionalDetail()[0].getAmount()
										.toString().trim());
						ResultsRoom
								.setCurrency(CurrentRoom.getAdditionalDetails()
										.getAdditionalDetail()[0]
										.getCurrencyCode().toString());
						ResultsRoom
								.setRateType(CurrentRoom.getAdditionalDetails()
										.getAdditionalDetail()[0]
										.getDetailDescription()
										.getParagraphTypeChoice()[0].getText()
										.toString().trim());
						ResultsRoom
								.setBedType(CurrentRoom.getAdditionalDetails()
										.getAdditionalDetail()[1]
										.getDetailDescription()
										.getParagraphTypeChoice()[0].getText()
										.toString().trim());

						CurrentHotel.addToResultsRooms(RoomTypeId, ResultsRoom);
					}

					ExtractedResponse.addToHotelList(CurrentHotel);

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return ExtractedResponse;
	}

public GeneratedAvailabilityResponse processAvailabilityResponse(OTA_HotelAvailRS response) {
	GeneratedAvailabilityResponse ExtractedResponse = new GeneratedAvailabilityResponse();

	if (response == null) {
		ExtractedResponse.setHotelAvailable(false);
		return ExtractedResponse;
	}
	OTA_HotelAvailRSChoice_type0 RequestType = response.getOTA_HotelAvailRSChoice_type0();
	ErrorsType ErrorsList = RequestType.getErrors();
	HotelStay_type0 hotelStay_type0 = null;

	if (ErrorsList != null) {
		ExtractedResponse.setHotelAvailable(false);
		ErrorType[] Errors = ErrorsList.getError();

		for (int i = 0; i < Errors.length; i++) {
			ErrorType Error = Errors[i];
			XmlOutError Err = new XmlOutError();
			Err.setErrorCode(Error.getType().toString());
			Err.setErrorDescription(Error.getShortText().toString());
			ExtractedResponse.addToErrors(Err);

		}
		return ExtractedResponse;
	} else {
		ExtractedResponse.setHotelAvailable(true);
	}

	// ================================================================================================//

	if (ExtractedResponse.isHotelAvailable()) {
		try {
			Hotel ResultHotel = new Hotel();
			ExtractedResponse.setTransactionIdentifier(response.getTransactionIdentifier().toString());
			hotelStay_type0 = response.getOTA_HotelAvailRSChoice_type0().getOTA_HotelAvailRSSequence_type0().getHotelStays().getHotelStay()[0];

			ResultHotel.setHotelCode(hotelStay_type0.getBasicPropertyInfo().getHotelCode().getStringLength1To16());
			ResultHotel.setHotelName(hotelStay_type0.getBasicPropertyInfo().getHotelName().getStringLength1To128());
			ResultHotel.setHotelVendor(hotelStay_type0.getBasicPropertyInfo().getBrandCode().getStringLength1To64());
			ResultHotel.setHotelAvailability(hotelStay_type0.getAvailability().toString());

			VendorMessageType[] VendorMessages = hotelStay_type0.getBasicPropertyInfo().getVendorMessages()[0].getVendorMessage();

			for (VendorMessageType vendorMessageType : VendorMessages) {
				String MessageType = vendorMessageType.getSubSection()[0].getSubTitle().toString();
				String Message = vendorMessageType.getSubSection()[0].getParagraph()[0].getParagraphTypeChoice()[0].getText().toString();

				if (MessageType.contains("Short"))
					ResultHotel.setShortDescription(Message);
				else if (MessageType.contains("long"))
					ResultHotel.setLongDescription(Message);
			}

			RoomStay_type0[] roomStay_type0s = response.getOTA_HotelAvailRSChoice_type0().getOTA_HotelAvailRSSequence_type0().getRoomStays().getRoomStay();

			for (int k = 0; k < roomStay_type0s.length; k++) {

				String RoomTypeCode = roomStay_type0s[k].getRoomTypes().getRoomType()[0].getRoomTypeCode().getStringLength1To64();
				String RoomType = roomStay_type0s[k].getRoomTypes().getRoomType()[0].getRoomType().getStringLength1To64();

				System.out.println("RoomTypeCode-->" + RoomTypeCode);
				System.out.println("RoomType-->" + RoomType);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	return ExtractedResponse;
}



}
