package auto.xmlout.hotel.loaders;

import java.io.IOException;
import java.util.HashMap;

import auto.xmlout.com.utilities.ReadProperties;
import auto.xmlout.common.configure.HotelConfigurator;

public class PropertySingleton {

	private static PropertySingleton SingletonInstance = null;
	private HashMap<String, String>  propertyList      = null;
	
	private PropertySingleton() throws Exception {

		try {
			propertyList = ReadProperties.readpropeties(HotelConfigurator.PROPERTY_FILE_PATH);
		} catch (IOException e) {
			e.printStackTrace();
			throw new Exception("Properties not loaded");
		}
	}


	public static PropertySingleton getInstance() throws Exception {
		if (SingletonInstance == null) {
			synchronized (PropertySingleton.class) {
				SingletonInstance = new PropertySingleton();
			}
		}

		return SingletonInstance;
	}
	
	
	public String getProperty(String property){
		
		return propertyList.get(property);
		
	}

}
