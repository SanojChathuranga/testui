package auto.xmlout.hotel.pojo;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import auto.xmlout.com.types.ChargeByType;

public class CancellationPolicy {
	

	
private Date                                 From ;
private Date                                 To   ;
private int                                  ApplyLessThan         = 0;
private int                                  Buffer                = 0;
private ChargeByType                         standardChargeByType   = ChargeByType.PERCENTAGE;
private String                               StandardValue         = "0";
private ChargeByType                         NoShowChargeByType    = ChargeByType.PERCENTAGE;
private String                               NoShowValue          = "";
private Date                                 DeadLine ;

private SimpleDateFormat sdf  = new SimpleDateFormat("MMM-dd-yyyy",Locale.ENGLISH);
private SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MMM-yyyy",Locale.ENGLISH);


public Date getFrom() {
	return From;
}
public void setFrom(String from) {
	
	try {
		From = sdf.parse(from);
	} catch (Exception e) {
		// TODO: handle exception
	}
	
}
public Date getTo() {
	return To;
}
public void setTo(String to) {
	
	try {
		To= sdf.parse(to);
	} catch (Exception e) {
		// TODO: handle exception
	}
}
public int getApplyLessThan() {
	return ApplyLessThan;
}
public void setApplyLessThan(int applyLessThan) {
	ApplyLessThan = applyLessThan;
}
public int getBuffer() {
	return Buffer;
}
public void setBuffer(int buffer) {
	Buffer = buffer;
}
public ChargeByType getStandardChargeByType() {
	return standardChargeByType;
}
public void setStandardChargeByType(ChargeByType standardChargeByType) {
	this.standardChargeByType = standardChargeByType;
}
public String getStandardValue() {
	return StandardValue;
}
public void setStandardValue(String standardValue) {
	StandardValue = standardValue;
}
public ChargeByType getNoShowChargeByType() {
	return NoShowChargeByType;
}
public void setNoShowChargeByType(ChargeByType noShowChargeByType) {
	NoShowChargeByType = noShowChargeByType;
}
public String getNoShowValue() {
	return NoShowValue;
}
public void setNoShowValue(String noShowValue) {
	NoShowValue = noShowValue;
}


public void setCancellationDeadLine(Date CheckIn)
{
	Calendar calendar = Calendar.getInstance();
	calendar.setTime(CheckIn);
	int TotalReduce = ApplyLessThan + Buffer + 1;
	calendar.add(Calendar.DATE, -TotalReduce);
	DeadLine      = calendar.getTime();
	
	
}

public  Map<Integer, String>  CalculatePolicy (Date CheckIn, Double BookingValue , int NON)

{
	
	this.setCancellationDeadLine(CheckIn);
	
	Map<Integer, String> PolicyMap = new HashMap<Integer, String>();
	Calendar DeadLine             = Calendar.getInstance();
	Calendar checkin              = Calendar.getInstance();
	checkin.setTime(CheckIn);
	checkin.set(Calendar.HOUR_OF_DAY, 0);
	checkin.set(Calendar.MINUTE, 0);
	checkin.set(Calendar.SECOND, 0);
	checkin.set(Calendar.MILLISECOND, 0);
	
	String ChekinString = sdf2.format(checkin.getTime());
	
	DeadLine.setTime(this.DeadLine);
	
	DeadLine.set(Calendar.HOUR_OF_DAY, 23);
	DeadLine.set(Calendar.MINUTE, 59);
	DeadLine.set(Calendar.SECOND, 0);
	DeadLine.set(Calendar.MILLISECOND, 0);
	
	Calendar CurrentDate  = Calendar.getInstance();
	CurrentDate.set(Calendar.HOUR_OF_DAY, 0);
	CurrentDate.set(Calendar.MINUTE, 0);
	CurrentDate.set(Calendar.SECOND, 0);
	CurrentDate.set(Calendar.MILLISECOND, 0);


	
	if(CurrentDate.before(DeadLine))
	{
		String FirstChunk  = "If you cancel between "+sdf2.format(CurrentDate.getTime()) +" and "+sdf2.format(this.DeadLine)+" you will be refunded your purchase price.";
		PolicyMap.put(0, FirstChunk);
		DeadLine.add(Calendar.DATE, 1);
		checkin.add(Calendar .DATE, -1);
		String SecondChunk = "";
		String ThirdChunk  = "";
		
		if(sdf2.format(checkin.getTime()) == sdf2.format(DeadLine.getTime()))
		 SecondChunk = "If you cancel on "+sdf2.format(DeadLine.getTime()) +" you will be charged "+getStandardPolicy(BookingValue);
		else
		 SecondChunk = "If you cancel between "+sdf2.format(DeadLine.getTime()) +" and "+sdf2.format(checkin.getTime())+" you will be charged "+getStandardPolicy(BookingValue);
		
		 ThirdChunk   = "If cancelled on or after the "+ChekinString+" No Show Fee "+getNoshowPolicy(BookingValue, NON)+" applies.";
		
		 PolicyMap.put(1, SecondChunk);
		 PolicyMap.put(2, ThirdChunk);
		
		
		
	}else
	{
		
		checkin.add(Calendar .DATE, -1);
		String SecondChunk = "";
		String ThirdChunk  = "";
		
		if(sdf2.format(checkin.getTime()) == sdf2.format(CurrentDate.getTime()))
		SecondChunk = "If you cancel on "+sdf2.format(checkin.getTime()) +" you will be charged "+getStandardPolicy(BookingValue);
		else
		SecondChunk = "If you cancel between "+sdf2.format(CurrentDate.getTime()) +" and "+sdf2.format(checkin.getTime())+" you will be charged "+getStandardPolicy(BookingValue);
		
		 ThirdChunk   = "If cancelled on or after the "+ChekinString+" No Show Fee "+getNoshowPolicy(BookingValue, NON)+" applies.";
		
		 PolicyMap.put(0, SecondChunk);
		 PolicyMap.put(1, ThirdChunk);
		
	}

	
	return PolicyMap;
	
}



public String  getStandardPolicy(Double BookingValue)
{
	String  returnString = ""; 
	if(standardChargeByType == ChargeByType.PERCENTAGE)
	{
	  returnString = this.StandardValue+" % of the purchase price";
		
	}
	else if(standardChargeByType == ChargeByType.VALUE)
	{
	 returnString = Double.parseDouble(this.StandardValue)+" % of the purchase price";	
	}else if(standardChargeByType == ChargeByType.NIGHTRATE)
	{
	 returnString =  this.StandardValue+" night's room and tax charge";	
	}
	
	return returnString;
	
}


public String  getNoshowPolicy(Double BookingValue,int NON)
{

	String  returnString = ""; 
	if(standardChargeByType == ChargeByType.PERCENTAGE)
	{
	  Double ReturnValue = Math.ceil(BookingValue * ((Double.parseDouble(this.NoShowValue))/100));
	  returnString ="Currency " + Double.toString(ReturnValue).split(".")[0];
		
	}
	else if(standardChargeByType == ChargeByType.VALUE)
	{
		returnString ="Currency " + this.NoShowValue ;
	}else if(standardChargeByType == ChargeByType.NIGHTRATE)
	{
	 Double ReturnValue = Math.ceil(BookingValue /NON);
	 returnString ="Currency " + Double.toString(ReturnValue * Integer.parseInt(this.NoShowValue)).split(".")[0];
	}
	
	return returnString;
	

	
	
}



}
