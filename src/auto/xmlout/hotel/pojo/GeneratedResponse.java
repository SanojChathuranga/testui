package auto.xmlout.hotel.pojo;

import java.util.ArrayList;

import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.StringLength1To128;

public class GeneratedResponse {
	
private boolean ResultsAvailable      = false;
private StringLength1To128  TransactionIdentifier = null;
private int  NumberOfHotels        = 0;
private boolean   ErrorResponse    = false;
private String    ErrorCode        = "N/A";
private String    ErrorMessage     = "N/A";
private String    ErrorLog         = "N/A";
ArrayList<Hotel> ResponseHotelList = new ArrayList<Hotel>();




public ArrayList<Hotel> getResponseHotelList() {
	return ResponseHotelList;
}
public void setResponseHotelList(ArrayList<Hotel> responseHotelList) {
	ResponseHotelList = responseHotelList;
}

public void addToHotelList(Hotel hotel) {
	ResponseHotelList.add(hotel);
}
public boolean isResultsAvailable() {
	return ResultsAvailable;
}
public void setResultsAvailable(boolean resultsAvailable) {
	ResultsAvailable = resultsAvailable;
}
public StringLength1To128 getTransactionIdentifier() {
	return TransactionIdentifier;
}
public void setTransactionIdentifier(StringLength1To128 stringLength1To128) {
	TransactionIdentifier = stringLength1To128;
}
public int getNumberOfHotels() {
	return NumberOfHotels;
}
public void setNumberOfHotels(int numberOfHotels) {
	NumberOfHotels = numberOfHotels;
}
public boolean isErrorResponse() {
	return ErrorResponse;
}
public void setErrorResponse(boolean errorResponse) {
	ErrorResponse = errorResponse;
}
public String getErrorCode() {
	return ErrorCode;
}
public void setErrorCode(String errorCode) {
	ErrorCode = errorCode;
}
public String getErrorMessage() {
	return ErrorMessage;
}
public void setErrorMessage(String errorMessage) {
	ErrorMessage = errorMessage;
}
public String getErrorLog() {
	return ErrorLog;
}
public void setErrorLog(String errorLog) {
	ErrorLog = errorLog;
}




}
