package auto.xmlout.hotel.pojo;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.SortBy_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.SortOrder_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.Target_type0;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.Target_type10;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.Target_type2;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.Target_type4;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.Target_type6;
import auto.xmlout.hotel.pojo.Hotel;
import auto.xmlout.com.types.HotelXmlSearchByType;
import auto.xmlout.com.types.ServerType;
import auto.xmlout.com.utilities.Calender;

public class SearchType {

	private String SearchIndex = "";
	private String CountryOFResidance = "LK";
	private String Location = "";
	private String HiddenLoc = "";
	private auto.xmlout.com.types.BookingType BookingType = auto.xmlout.com.types.BookingType.NONE;
	private String Nights = "";
	private String Rooms = "";
	private String[] Adults = null;
	private String[] Children = null;
	// private String Children = "";
	private Map<Integer, String[]> ChildAges = null;
	private ArrayList<String> RoomTypes = null;
	private String Description = "";
	private String Expected = "";
	private String SearchCurrency = "USD";
	private String BaseCurrency = "USD";
	private String ArrivalDate = "";
	private String CheckOutDate = "";
	private Calendar Indate = null;
	private Calendar OutDate = null;
	private Map<String, String> DefaultRooms = new TreeMap<String, String>();
	private Map<String, String> SelectedRooms = new TreeMap<String, String>();
	private Map<String, String> SelectedRoomCodes = new TreeMap<String, String>();
	private boolean HotelAvailability;
	@SuppressWarnings("unused")
	private auto.xmlout.com.types.BookingType bookingType = null;
	private String HotelName = "";
	private String BookingDate = "";
	private String Availability = "";
	private String TotalRate = "";
	private String BookinDateType = "";
	private Hotel ValidationHotel = new Hotel();
	private Map<String, ArrayList<RoomType>> ResultsRoomMap = new HashMap<String, ArrayList<RoomType>>();
	private CartDetails Cart = new CartDetails();
	private CancellationPolicy policy = new CancellationPolicy();
	private CancellationPolicy CartPolicy = new CancellationPolicy();
	private ArrayList<RoomType> SelectedRoomObj = new ArrayList<RoomType>();
	private boolean ScenarioExcecutionState = true;
	private String BookingChannel = "Call Center";
	private String Pay_CustomerNote = "Automation Customer Note Automation Customer Note Automation Customer Note Automation Customer Note Automation Customer Note Automation Customer Note ";
	private String Pay_InternalNote = "Auto Internal Note Auto Internal Note Auto Internal Note Auto Internal Note Auto Internal Note Auto Internal Note Auto Internal Note Auto Internal Note Auto Internal Note";
	private String Pay_HotelNote = "Auto Hotel Note Auto Hotel Note Auto Hotel Note Auto Hotel Note Auto Hotel Note Auto Hotel Note Auto Hotel Note Auto Hotel Note Auto Hotel Note Auto Hotel Note ";
	private Boolean isCopyMails = true;
	private String PaymentType = "-";
	private String CardType = "-";
	private String OfflineMethod = "-";
	private String PaymentRef = "-";
	private boolean isOptionalSupplementarySelected = false;
	private boolean BestRateApplied = false;
	private boolean DealOfTheDayApplied = false;

	private Target_type6 hotel_info_Target = Target_type6.Test;
	private Target_type2 hotel_room_Target = Target_type2.Test;
	private Target_type0 hotel_search_Target = Target_type0.Test;
	private Target_type4 hotel_res_Target = Target_type4.Test;
	private Target_type10 hotel_cncl_Target = Target_type10.Test;
	private boolean SingleHotelSearch = false;
	private String AgentID = "REZBASE_V3_TEST_CHARITHA";
	private String UserName = "Default";
	private String PassWord = "Default";
	private HotelXmlSearchByType serachBy = HotelXmlSearchByType.CITYCODE;
	private String CityCode = "";
	private String AirPortCode = "";
	private String HotelID = "000";
	private String HotelVendor = "rezbase_V3";
	private SortBy_type0 ResultsSortByType = SortBy_type0.ASC;
	private SortOrder_type0 ResultsSortOrderType = SortOrder_type0.PRICE;
	private String AvailabilityTracer = "";
    private ServerType   servertype              = ServerType.STAGING;
	

    
    
    public ServerType getServertype() {
		return servertype;
	}

	public void setServertype(ServerType servertype) {
		this.servertype = servertype;
	}

	public String getAvailabilityTracer() {
		return AvailabilityTracer;
	}

	public void setAvailabilityTracer(String availabilityTracer) {
		AvailabilityTracer = availabilityTracer;
	}

	public SortBy_type0 getResultsSortByType() {
		return ResultsSortByType;
	}

	public void setResultsSortByType(String resultsSortByType) {
		if (resultsSortByType.trim().equalsIgnoreCase("ASC"))
			ResultsSortByType = SortBy_type0.ASC;
		else if (resultsSortByType.trim().equalsIgnoreCase("DSC"))
			ResultsSortByType = SortBy_type0.DESC;
	}

	public SortOrder_type0 getResultsSortOrderType() {
		return ResultsSortOrderType;
	}

	public void setResultsSortOrderType(String resultsSortOrder) {

		if (resultsSortOrder.equalsIgnoreCase("Price"))
			ResultsSortOrderType = SortOrder_type0.PRICE;
		else if (resultsSortOrder.equalsIgnoreCase("StarRating"))
			ResultsSortOrderType = SortOrder_type0.RATING;
		else if (resultsSortOrder.equalsIgnoreCase("Rank"))
			ResultsSortOrderType = SortOrder_type0.HOTELRANK;
		else if (resultsSortOrder.equalsIgnoreCase("HotelName"))
			ResultsSortOrderType = SortOrder_type0.HOTELNAME;
	}

	public String getHotelID() {
		return HotelID;
	}

	public void setHotelID(String hotelID) {
		HotelID = hotelID;
	}

	public String getHotelVendor() {
		return HotelVendor;
	}

	public void setHotelVendor(String hotelVendor) {
		HotelVendor = hotelVendor;
	}

	public Calendar getIndate() {
		return Indate;
	}

	public Calendar getOutDate() {
		return OutDate;
	}

	public String getCityCode() {
		return CityCode;
	}

	public void setCityCode(String cityCode) {
		CityCode = cityCode;
	}

	public String getAirPortCode() {
		return AirPortCode;
	}

	public void setAirPortCode(String airPortCode) {
		AirPortCode = airPortCode;
	}

	public HotelXmlSearchByType getSerachBy() {
		return serachBy;
	}

	public void setSerachBy(HotelXmlSearchByType serachBy) {
		this.serachBy = serachBy;
	}

	public String getAgentID() {
		return AgentID;
	}

	public void setAgentID(String agentID) {
		AgentID = agentID;
	}

	public String getUserName() {
		return UserName;
	}

	public void setUserName(String userName) {
		UserName = userName;
	}

	public String getPassWord() {
		return PassWord;
	}

	public void setPassWord(String passWord) {
		PassWord = passWord;
	}

	public boolean isSingleHotelSearch() {
		return SingleHotelSearch;
	}

	public void setSingleHotelSearch(boolean singleHotelSearch) {
		SingleHotelSearch = singleHotelSearch;
	}

	public Target_type6 getHotel_info_Target() {
		return hotel_info_Target;
	}

	public Target_type2 getHotel_room_Target() {
		return hotel_room_Target;
	}

	public Target_type0 getHotel_search_Target() {
		return hotel_search_Target;
	}

	public Target_type4 getHotel_res_Target() {
		return hotel_res_Target;
	}

	public Target_type10 getHotel_cncl_Target() {
		return hotel_cncl_Target;
	}

	public void setTargetTypes(String target) {
		if (target.equalsIgnoreCase("Staging")) {
			hotel_info_Target = Target_type6.Test;
			hotel_room_Target = Target_type2.Test;
			hotel_search_Target = Target_type0.Test;
			hotel_res_Target = Target_type4.Test;
			hotel_cncl_Target = Target_type10.Test;

		} else if (target.equalsIgnoreCase("live")) {
			hotel_info_Target = Target_type6.Production;
			hotel_room_Target = Target_type2.Production;
			hotel_search_Target = Target_type0.Production;
			hotel_res_Target = Target_type4.Production;
			hotel_cncl_Target = Target_type10.Production;

		}

	}

	public boolean isBestRateApplied() {
		return BestRateApplied;
	}

	public void setBestRateApplied(String bestRateApplied) {
		if (bestRateApplied.equalsIgnoreCase("yes"))
			BestRateApplied = true;
		else
			BestRateApplied = false;
	}

	public boolean isDealOfTheDayApplied() {
		return DealOfTheDayApplied;
	}

	public void setDealOfTheDayApplied(String dealOfTheDayApplied) {
		if (dealOfTheDayApplied.equalsIgnoreCase("yes"))
			DealOfTheDayApplied = true;
		else
			DealOfTheDayApplied = false;
	}

	public boolean isOptionalSupplementarySelected() {
		return isOptionalSupplementarySelected;
	}

	public void setOptionalSupplementarySelected(boolean isOptionalSupplementarySelected) {
		this.isOptionalSupplementarySelected = isOptionalSupplementarySelected;
	}

	public ArrayList<RoomType> getSelectedRoomObj() {
		return SelectedRoomObj;
	}

	public void addToSelectedRoomObj(RoomType selectedRoom) {
		SelectedRoomObj.add(selectedRoom);
	}

	public String getBaseCurrency() {
		return BaseCurrency;
	}

	public void setBaseCurrency(String baseCurrency) {
		BaseCurrency = baseCurrency;
	}

	public String getBookingChannel() {
		return BookingChannel;
	}

	public void setBookingChannel(String bookingChannel) {
		BookingChannel = bookingChannel;
	}

	public boolean isScenarioExcecutionState() {
		return ScenarioExcecutionState;
	}

	public void setScenarioExcecutionState(String scenarioExcecutionState) {
		if (scenarioExcecutionState.trim().equalsIgnoreCase("True"))
			ScenarioExcecutionState = true;
		else if (scenarioExcecutionState.trim().equalsIgnoreCase("false"))
			ScenarioExcecutionState = false;
	}

	public Map<String, String> getSelectedRooms() {
		return SelectedRooms;
	}

	public void addToSelectedRooms(String roomnum, String RoomString) {
		SelectedRooms.put(roomnum, RoomString);
	}

	public Map<String, String> getDefaultRooms() {
		return DefaultRooms;
	}

	public void addToDefaultRooms(String roomnum, String RoomString) {
		DefaultRooms.put(roomnum, RoomString);
	}

	public String getPaymentType() {
		return PaymentType;
	}

	public void setPaymentType(String paymentType) {
		PaymentType = paymentType;
	}

	public String getCardType() {
		return CardType;
	}

	public void setCardType(String cardType) {
		CardType = cardType;
	}

	public String getOfflineMethod() {
		return OfflineMethod;
	}

	public void setOfflineMethod(String offlineMethod) {
		OfflineMethod = offlineMethod;
	}

	public String getPaymentRef() {
		return PaymentRef;
	}

	public void setPaymentRef(String paymentRef) {
		PaymentRef = paymentRef;
	}

	public ArrayList<String> getRoomTypes() {
		return RoomTypes;
	}

	public void setRoomTypes(ArrayList<String> roomTypes) {
		RoomTypes = roomTypes;
	}

	public String getPay_CustomerNote() {
		return Pay_CustomerNote;
	}

	public void setPay_CustomerNote(String pay_CustomerNote) {
		Pay_CustomerNote = pay_CustomerNote;
	}

	public String getPay_InternalNote() {
		return Pay_InternalNote;
	}

	public void setPay_InternalNote(String pay_InternalNote) {
		Pay_InternalNote = pay_InternalNote;
	}

	public String getPay_HotelNote() {
		return Pay_HotelNote;
	}

	public void setPay_HotelNote(String pay_HotelNote) {
		Pay_HotelNote = pay_HotelNote;
	}

	public Boolean getIsCopyMails() {
		return isCopyMails;
	}

	public void setIsCopyMails(Boolean isCopyMails) {
		this.isCopyMails = isCopyMails;
	}

	public void setAdults(String[] adults) {
		Adults = adults;
	}

	public void setChildren(String[] children) {
		Children = children;
	}

	public void setChildAges(Map<Integer, String[]> childAges) {
		ChildAges = childAges;
	}

	public void setCheckOutDate(String checkOutDate) throws ParseException {
		CheckOutDate = checkOutDate;
		OutDate = Calender.getCalendarDate(checkOutDate, "dd-MMM-yyyy");
	}

	public void setResultsRoomMap(Map<String, ArrayList<RoomType>> resultsRoomMap) {
		ResultsRoomMap = resultsRoomMap;
	}

	public String getSearchIndex() {
		return SearchIndex;
	}

	public void setSearchIndex(String searchIndex) {
		SearchIndex = searchIndex;
	}

	public Hotel getValidationHotel() {
		return ValidationHotel;
	}

	public void setValidationHotel(Hotel validationHotel) {
		ValidationHotel = validationHotel;
	}

	public Map<String, ArrayList<RoomType>> getResultsRoomMap() {
		return ResultsRoomMap;
	}

	public void addToResultsRoomMap(ArrayList<RoomType> RoomList, String Room) {
		ResultsRoomMap.put(Room, RoomList);
	}

	public CartDetails getCart() {
		return Cart;
	}

	public void setCart(CartDetails cart) {
		Cart = cart;
	}

	public CancellationPolicy getPolicy() {
		return policy;
	}

	public void setPolicy(CancellationPolicy policy) {
		this.policy = policy;
	}

	public String getBookinDateType() {
		return BookinDateType;
	}

	public void setBookinDateType(String bookinDateType) {
		BookinDateType = bookinDateType;
	}

	public String getBookingDate() {
		return BookingDate;
	}

	public void setBookingDate(String bookingDate) {

		if (!bookingDate.trim().equalsIgnoreCase("N/A")) {
		   try {
			
				this.ArrivalDate = bookingDate.split("-")[0].trim();
			    this.CheckOutDate =bookingDate.split("-")[1].trim();
				Indate = Calender.getCalendarDate(ArrivalDate, "dd/MM/yyyy");
				OutDate = Calender.getCalendarDate(CheckOutDate, "dd/MM/yyyy");
				System.out.println(Indate);
				System.out.println(OutDate);
			} catch (Exception e) {
				System.out.println("Pharse Error: " + bookingDate + " " + e.toString());
			}
		} else {
			BookingDate = "-";
		}

	}

	public String getAvailability() {
		return Availability;
	}

	public void setAvailability(String availability) {
		Availability = availability;
	}

	public String getTotalRate() {
		return TotalRate;
	}

	public void setTotalRate(String totalRate) {
		TotalRate = totalRate;
	}

	public void setHotelAvailability(boolean hotelAvailability) {
		HotelAvailability = hotelAvailability;
	}

	public String getHotelName() {
		return HotelName;
	}

	public void setHotelName(String hotelName) {
		HotelName = hotelName;
	}

	public void setBookingType(auto.xmlout.com.types.BookingType bookingType) {
		this.bookingType = bookingType;
	}

	public boolean isHotelAvailability() {
		return HotelAvailability;
	}

	public void setHotelAvailability(String hotelAvailability) {
		if (hotelAvailability.equalsIgnoreCase("Available"))
			HotelAvailability = true;
		else
			HotelAvailability = false;
	}

	public String getCheckOutDate() {
		return CheckOutDate;
	}

	public void setSearchCurrency(String Currency) {
		SearchCurrency = Currency;
	}

	public String getSearchCurrency() {
		return SearchCurrency;
	}

	public String getCountryOFResidance() {
		return CountryOFResidance;
	}

	public void setCountryOFResidance(String countryOFResidance) {
		CountryOFResidance = countryOFResidance;
	}

	public String getLocation() {
		return Location;
	}

	public void setLocation(String location) {
		Location = location;
	}

	public String getHiddenLoc() {
		return HiddenLoc;
	}

	public void setHiddenLoc(String hiddenLoc) {
		HiddenLoc = hiddenLoc;
	}

	public auto.xmlout.com.types.BookingType getBookingType() {
		return BookingType;
	}

	public void setBookingType(String bookingType) throws ParseException {
		BookingType = auto.xmlout.com.types.BookingType.getBookingType(bookingType.trim());

		if (this.BookingDate.equalsIgnoreCase("-")) {

			if (BookingType == auto.xmlout.com.types.BookingType.OUTSIDECANCELLATION) {
				ArrivalDate = Calender.getDate(Calendar.MONTH, 1, "MM/dd/yyyy");
				CheckOutDate = Calender.getDate(1, Integer.parseInt(Nights), "MM/dd/yyyy", "");
			} else {
				ArrivalDate = Calender.getDate(Calendar.DATE, 1, "MM/dd/yyyy");
				CheckOutDate = Calender.getDate(Calendar.DATE, 1 + Integer.parseInt(Nights), "MM/dd/yyyy");
			}

			Indate = Calender.getCalendarDate(ArrivalDate, "MM/dd/yyyy");
			OutDate = Calender.getCalendarDate(CheckOutDate, "MM/dd/yyyy");
		}

	}
	
	public Map<String, String> getSelectedRoomCodes() {
		return SelectedRoomCodes;
	}
	public void addToSelectedRoomCodes(String roomnum, String RoomCode) {
		SelectedRoomCodes.put(roomnum, RoomCode);
	}


	public String getNights() {
		return Nights;
	}

	public void setNights(String nights) {
		Nights = nights;
	}

	public String getRooms() {
		return Rooms;
	}

	public void setRooms(String rooms) {
		Rooms = rooms;
	}

	public String[] getAdults() {
		return Adults;
	}

	public void setAdults(String adults) {
		Adults = adults.split(";");
	}

	public String[] getChildren() {
		return Children;
	}

	public void setChildren(String children) {
		Children = children.split(";");
	}

	public Map<Integer, String[]> getChildAges() {
		return ChildAges;
	}

	public void setChildAges(String childAges) {
		ChildAges = new HashMap<Integer, String[]>();
		String[] RoomChild = childAges.trim().split("/");
		int Count = 1;
		for (String string : RoomChild) {
			ChildAges.put(Count, string.split(";"));
			Count++;
		}
	}

	public ArrayList<String> getRoomType() {
		return RoomTypes;
	}

	public void setRoomTypes(String roomTypes) {
		RoomTypes = new ArrayList<String>();
		String[] Rooms = roomTypes.split("/");

		for (String string : Rooms) {
			RoomTypes.add(string);
		}

	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public String getExpected() {
		return Expected;
	}

	public void setExpected(String expected) {
		Expected = expected;
	}

	public void setExpected(String expected, String Rate) {
		Double ExpectedValue = Double.parseDouble(expected) * Double.parseDouble(Rate);
		DecimalFormat df = new DecimalFormat("#.##");
		ExpectedValue = Double.valueOf(df.format(ExpectedValue));
		Expected = Double.toString(ExpectedValue);
	}

	public String getArrivalDate() {
		return ArrivalDate;
	}

	public void setArrivalDate(String arrivalDate) throws ParseException {
		ArrivalDate = arrivalDate;
		Indate = Calender.getCalendarDate(arrivalDate, "dd-MMM-yyyy");
	}

	public CancellationPolicy getCartPolicy() {
		return CartPolicy;
	}

	public void setCartPolicy(CancellationPolicy cartPolicy) {
		CartPolicy = cartPolicy;
	}

}
