package auto.xmlout.hotel.reqbuilders;

import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.context.ConfigurationContext;
import org.apache.axis2.context.ConfigurationContextFactory;
import auto.xmlout.hotel.loaders.PropertySingleton;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub;

public class ReservationStubBuilder {

private String username = null;
private String password = null;
private String url = null;
private ReservationServiceStub stub = null;

public ReservationStubBuilder(String url,String user,String pass) {
		username = user;
		this.url = url;
		password = pass;
	}

	
	
	public ReservationServiceStub buildReservationServiceStub( ) throws Exception {

		System.out.println("buildTheStub().Start=========================================================");

		ConfigurationContext ctx = ConfigurationContextFactory.createConfigurationContextFromFileSystem(PropertySingleton.getInstance().getProperty("Xmlout.AXIS_CONFIG_PATH"),PropertySingleton.getInstance().getProperty("Xmlout.AXIS_XML_PATH"));
		stub = new ReservationServiceStub(ctx, url);
		stub._getServiceClient().getOptions().setTimeOutInMilliSeconds(1000 * 60 * 2);

		ServiceClient sc = stub._getServiceClient();
		sc.engageModule("rampart");
		sc.engageModule("xmloutlogging");
		Options options = sc.getOptions();

		System.out.println("username--->" + username + " password-->" + password);

		options.setUserName(username);
		options.setPassword(password);

		options.setProperty(org.apache.axis2.transport.http.HTTPConstants.MC_GZIP_REQUEST, Boolean.FALSE); // Content-Encoding: gzip
		options.setProperty(org.apache.axis2.transport.http.HTTPConstants.MC_GZIP_RESPONSE, Boolean.TRUE);
		options.setProperty(org.apache.axis2.transport.http.HTTPConstants.MC_ACCEPT_GZIP, Boolean.TRUE); // Accept-Encoding: gzip

		System.out.println("buildTheStub().End=========================================================");

		return stub;

	}

}
