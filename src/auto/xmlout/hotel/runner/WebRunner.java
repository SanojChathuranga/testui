package auto.xmlout.hotel.runner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import auto.xmlout.com.utilities.ReadExcel;
import auto.xmlout.hotel.loaders.HotelDataLoader;
import auto.xmlout.hotel.loaders.PropertySingleton;
import auto.xmlout.hotel.loaders.SearchDataLoader;
import auto.xmlout.hotel.pojo.GeneratedResponse;
import auto.xmlout.hotel.pojo.SearchType;
import auto.xmlout.hotel.pojo.Hotel;
import auto.xmlout.hotel.processors.ResponseProcessors;
import auto.xmlout.hotel.reqbuilders.HotelRequestBuilder;
import auto.xmlout.hotel.reqbuilders.ReservationStubBuilder;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_HotelSearchRQ;
import xmlout.org.opentravel.www.ota._2003._05.ReservationServiceStub.OTA_HotelSearchRS;

public class WebRunner {

	private ReservationServiceStub stub = null;
	private HashMap<String, SearchType> SearchList = null;
	private Map<String, String>         CurrencyMap= null;
	private Logger logger = null;
	private TreeMap<String, Hotel> HotelList = null;

	private ArrayList<Map<Integer, String>> SearchSheetList = null;
	private ArrayList<Map<Integer, String>> HotelSheetList = null;


	public void setUp() throws Exception {

		DOMConfigurator.configure("log4j.xml");
		logger = Logger.getLogger(this.getClass());

		try {
			ReadExcel readExcel = new ReadExcel();
			logger.info("Initializing Excel WorkBooks -");
			logger.info("WorkBooks Paths -Search Excel---> " + PropertySingleton.getInstance().getProperty("XmlOut.SearchExcelPath") + "-Hotel Excel-->" + PropertySingleton.getInstance().getProperty("XmlOut.HotelExcelPath"));
			SearchSheetList = readExcel.init(PropertySingleton.getInstance().getProperty("XmlOut.SearchExcelPath"));
			HotelSheetList  = readExcel.init(PropertySingleton.getInstance().getProperty("XmlOut.HotelExcelPath"));
		} catch (Exception e) {
			logger.fatal("Error when initializing the Excel WorkBooks -", e);
			throw new Exception("Error when initializing the Excel WorkBooks");
		}

		try {
			HotelDataLoader Hotelloader = new HotelDataLoader();
			HotelList = Hotelloader.getHotelData(HotelSheetList);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error when initializing the Hotel Details");
		}

		try {
			SearchDataLoader SearchLoder = new SearchDataLoader(SearchSheetList.get(3), PropertySingleton.getInstance().getProperty("Xmlout.PortalCurrency"));
			SearchList = (HashMap<String, SearchType>) SearchLoder.getSearchDetails(SearchSheetList, HotelList);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error when initializing the Search Details");
		}
		
		System.setProperty("javax.net.ssl.trustStore", "C:/rezsystem/jboss-4.0.3SP1/bin/certs/QA.keystore");
		System.setProperty("javax.net.ssl.trustStorePassword", "123456");

	
		
	}


	public void run() throws Exception {

		String Url = PropertySingleton.getInstance().getProperty("XmlOut.Url");
		String Username = PropertySingleton.getInstance().getProperty("XmlOut.UserName");
		String Pass = PropertySingleton.getInstance().getProperty("XmlOut.PassWord");

		logger.info("Building Reservation Stub using  Url---->"+Url+" User--->"+Username +" Pass--->"+Pass);
		
		logger.info("============Building Reservation Stub=====================");
		ReservationStubBuilder StubBuilder = new ReservationStubBuilder(Url, Username, Pass);
		try {
			stub = StubBuilder.buildReservationServiceStub();
		} catch (Exception e) {
	        logger.fatal("Reservation Stub build failed - Treminating the test suit", e);
	        throw new Exception();
		}
		
		
		Iterator<Map.Entry<String, SearchType>>  ReservationScenarioItr = SearchList.entrySet().iterator();
		while (ReservationScenarioItr.hasNext()) {
			HotelRequestBuilder ReqBuilder    = new HotelRequestBuilder();
			Map.Entry<java.lang.String, auto.xmlout.hotel.pojo.SearchType> entry = (Map.Entry<java.lang.String, auto.xmlout.hotel.pojo.SearchType>) ReservationScenarioItr.next();
			SearchType              CurrentSearch   = entry.getValue();
			OTA_HotelSearchRQ       Search          = null;
			ResponseProcessors      Processor       = new ResponseProcessors();
			// Search
			
			if(CurrentSearch.isScenarioExcecutionState()){
				logger.info("================= Building the search request=============== ");
				try {
					 Search = ReqBuilder.buildHotelSearchRequest(CurrentSearch);
				} catch (Exception e) {
				    logger.fatal("Reservation Stub build failed - Treminating the test suit", e);
			        throw new Exception();
				}
				
				logger.info("================= Sending  the search request=============== ");
				
				OTA_HotelSearchRS SearchRS = stub.HotelSearch(Search);
				
				logger.info("================= Processing the response=============== ");
				
				GeneratedResponse ServiceResponse = Processor.processSearchResponse(SearchRS);
				
				logger.info(ServiceResponse.getTransactionIdentifier());
				logger.info(ServiceResponse.getNumberOfHotels());
			}else {
				logger.info("Scenario--->"+entry.getKey()+"  Skipped ");
			}
		
			
			
			
		}

	}


}
