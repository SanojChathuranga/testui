package auto.xmlout.com.types;

public enum ServerType {
	
 STAGING,LIVE,NONE;
 
 public ServerType getChargeType(String ChargeType)
 {
 	if(ChargeType.equalsIgnoreCase("Staging"))return STAGING;
 	else if(ChargeType.equalsIgnoreCase("Live"))return LIVE;
 	
 	else return NONE;
 	
 }

}
