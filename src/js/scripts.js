//more details
$(document).ready(function() {
	
	
	moment.lang('fr', {
	    
	    longDateFormat : {
	        LT : "HH:mm",
	        L : "DD/MM/YYYY",
	        LL : "D MMMM YYYY",
	        LLL : "D MMMM YYYY LT",
	        LLLL : "dddd D MMMM YYYY LT"
	    }
	  
	});
	
	
	$('.show_more_vac').click(function() {
		$('#show_more_vac').slideToggle();
	});
	$('.show_more').click(function() {
		$('#show_detail').slideToggle();
	});
	$('.show_more').click(function() {
		if($('.show_more i').hasClass('fa-angle-double-down')) 
			$('.show_more i').removeClass('fa-angle-double-down').addClass('fa-angle-double-up');
		 else if($('.show_more i').hasClass('fa-angle-double-up')) 
			$('.show_more i').removeClass('fa-angle-double-up').addClass('fa-angle-double-down');	
	});
	$('.show_more_vac').click(function() {
		if($('.show_more_vac i').hasClass('fa-angle-double-down')) 
			$('.show_more_vac i').removeClass('fa-angle-double-down').addClass('fa-angle-double-up');
		 else if($('.show_more_vac i').hasClass('fa-angle-double-up')) 
			$('.show_more_vac i').removeClass('fa-angle-double-up').addClass('fa-angle-double-down');	
	});	
});

//multidestination
$(document).ready(function() {

	
	
	$(".arrow1").click(function() {
	
		$("#multi_des_02").slideDown("slow");
		if ($(".arrow1").hasClass("arrow_min")) {
			$("#multi_des_02").slideUp("slow");
			$("#multi_des_03").slideUp("slow");
			$("#multi_des_04").slideUp("slow");
			$(".arrow1").removeClass("arrow_min").addClass("arrow_plu");
			$(".arrow2").removeClass("arrow_min").addClass("arrow_plu");
			$(".arrow3").removeClass("arrow_min").addClass("arrow_plu");
		} else {
			$(".arrow1").removeClass("arrow_plu").addClass("arrow_min");
		}
	});

	$(".arrow2").click(function() {
		$("#multi_des_03").slideDown("slow");
		if ($(".arrow2").hasClass("arrow_min")) {
			$("#multi_des_03").slideUp("slow");
			$("#multi_des_04").slideUp("slow");
			$(".arrow2").removeClass("arrow_min").addClass("arrow_plu");
			$(".arrow3").removeClass("arrow_min").addClass("arrow_plu");
			$(".arrow4").removeClass("arrow_min").addClass("arrow_plu");
		} else {
			$(".arrow2").removeClass("arrow_plu").addClass("arrow_min");
		}
	});
	
	$(".arrow3").click(function() {
		$("#multi_des_04").slideToggle("slow");
		$(".arrow3").toggleClass('arrow_min');
	});
});

$(document).ready(function(){

	$("#Air_TripType3").click(function (){ 
		if ($('.mul_d_1 i').hasClass('fa-minus-square')) {
			 $('.mul_d_1 i').removeClass('fa-minus-square').addClass('fa-plus-square');	
		}
	});
	$('.mul_d_1').click(function(){
	$('#multi_des_02').css('display', 'block');	
			if ($('.mul_d_1 i').hasClass('fa-plus-square')) 
			$('.mul_d_1 i').removeClass('fa-plus-square').addClass('fa-minus-square');
					
		 else if ($('.mul_d_1 i').hasClass('fa-minus-square')) 
			 $('.mul_d_1 i').removeClass('fa-minus-square').addClass('fa-plus-square');		
	});
	$('.mul_d_2').click(function(){
		$('#multi_des_03').css('display', 'block');	
		if ($('.mul_d_2 i').hasClass('fa-plus-square')) 
			$('.mul_d_2 i').removeClass('fa-plus-square').addClass('fa-minus-square');
		 else if ($('.mul_d_2 i').hasClass('fa-minus-square')) 
			 $('.mul_d_2 i').removeClass('fa-minus-square').addClass('fa-plus-square');		
	});
	$('.mul_d_3').click(function(){
		$('#multi_des_04').css('display', 'block');	
		if ($('.mul_d_3 i').hasClass('fa-plus-square')) 
			$('.mul_d_3 i').removeClass('fa-plus-square').addClass('fa-minus-square');
		 else if ($('.mul_d_3 i').hasClass('fa-minus-square')) 
			 $('.mul_d_3 i').removeClass('fa-minus-square').addClass('fa-plus-square');		
	});
});

$(document).ready(function(){
	$('#alertbox').click(function(){
		$('#alertbox').css('display', 'none');			
	});	
}); 
//$(document).ready(function() {
//	$('#hotel-arrival-date, #f-dep-date, #f-ret-date, #h-dep-date, #f-des1-dep-date, #f-des2-dep-date, #f-des3-dep-date, #f-des4-dep-date, #pk-up-date, #c-ret-date, #act-chk-in-date,#act-chk-out-date, #vac-dep-date, #vac-ret-date').datepicker({
//        numberOfMonths: 2
//    });
//}); 

$(document).ready(function() {
	(function() {
		var implementDate = function () {
		
			var now = moment().format('L');
			//alert("Now--------->"+now);
			
			//  var endDate = moment(now, ["MM-DD-YYYY", "YYYY-MM-DD"],'fr').add('days', 2).calendar();
			var endDate = moment().add('days', 2, 'fr');
			
			var enddateformated = endDate.format('L');
		
				defaultDateRange = now+'-' +enddateformated;
				
			//changing e on nights
			$('#h-nights').on('change',  function() {
				
				setDeaprtureDateToDateRange();
			});		
			
			//initializing range date picker	
			$(".date-range103")
								.dateRangePicker({
									startDate: now,
									autoClose: true
								})
								.val(defaultDateRange);		

		}
		implementDate();
	})();
});
$(document).ready(function() {
	$('.mul_d .date-range103').datepicker();
});
// browser detection 
$(document).ready(function() {
	if(typeof String.prototype.trim !== 'function') {
		  String.prototype.trim = function() {
		    return this.replace(/^\s+|\s+$/g, ''); 
		  }
		}
	console.log($.browser.browser() + ' ' + $.browser.version.string());
	var browser = $.browser.browser();
    if (browser == 'Internet Explorer' && $.browser.version.string() == '10.0') {
	  $("html").addClass("ie10");	  
	}
    if (browser == 'Internet Explorer' && $.browser.version.string() == '9.0') {
	  $("html").addClass("ie9");	  
	}
    if (browser == 'Internet Explorer' && $.browser.version.string() == '8.0') {
		$("html").addClass("ie8");
	
		$('.date-range103').on('click', function() {
			if($('#setCalType').val() == "H") {
				$('.date-picker-wrapper').addClass('cal_H');
			} else if($('#setCalType').val() == "F") {
				$('.date-picker-wrapper').addClass('cal_F');
			} else if($('#setCalType').val() == "C") {
				$('.date-picker-wrapper').addClass('cal_C');
			} else if($('#setCalType').val() == "A") {
				$('.date-picker-wrapper').addClass('cal_A');
			} else if($('#setCalType').val() == "V") {
				$('.date-picker-wrapper').addClass('cal_V');
			} 
		});
        
        $("document").ready(function() {
            $("#Air_TripType1").on("change", function(){
                if(!($("#Air_TripType1").is("checked"))) {                    
                    $('#Air_DepTime_0').css({
                        position: 'relative',
                        left: '-20px'
                    });
                } 	
            });
            $("#Air_TripType2").on("change", function(){
                if(!($("#Air_TripType2").is("checked"))) {                   
                    $('#Air_DepTime_0').css({
                        position: 'static'
                    });
                } 	
            });
            $("#Air_TripType3").on("change", function(){
                if(!($("#Air_TripType3").is("checked"))) {
                    $('#Air_DepTime_0').css({
                        position: 'relative',
                        left: '-20px'
                    });
                } 	
            });
        });
	  
	}
    if (browser == 'Firefox') {
	  $("html").addClass("ff");	  
	}
	
	
});

$("document").ready(function() {
	$("#Air_TripType1").on("change", function(){
		if(!($("#Air_TripType1").is("checked"))) {
			$("#dep_time_txt").css("marginBottom", "5px");            
		} 	
	});
	$("#Air_TripType2").on("change", function(){
		if(!($("#Air_TripType2").is("checked"))) {
			$("#dep_time_txt").css("marginBottom", "0px");            
		} 	
	});
	$("#Air_TripType3").on("change", function(){
		if(!($("#Air_TripType3").is("checked"))) {
			$("#dep_time_txt").css("marginBottom", "5px");
		} 	
	});
});

function setDeaprtureDateToDateRange() {
	//alert("setDeaprtureDateToDateRange");
	var rangedate = document.getElementById('hotel-arrival-date').value;
	//alert(rangedate);
	var chekingcheckoutdate = rangedate.split("-");
	var checkinDate = chekingcheckoutdate[0];
	//alert("Checkin--------->"+checkinDate);
	var checkoutDate = chekingcheckoutdate[1];
	//alert("checkoutDate--------->"+checkoutDate);

	var datesplit = checkinDate;
	var res = datesplit.split("/");
	var arrdateformed = res[2].trim() + "-" + res[1].trim() + "-" + res[0].trim();
	//alert("arrdateformed "+arrdateformed);

	if(typeof String.prototype.trim !== 'function') {
		  String.prototype.trim = function() {
		    return this.replace(/^\s+|\s+$/g, ''); 
		  }
		}
	
	var nightsCmb = document.getElementById('h-nights');
	var nights = nightsCmb.options[nightsCmb.selectedIndex].value;
	var numberofnights = Number(nights);

	
	// Moment
	
	 var momentjsdate =	moment(arrdateformed, ["MM-DD-YYYY", "YYYY-MM-DD"]);
	  //alert("momentjsdate-------->"+momentjsdate);
	  
	   var newdepdarture = moment(arrdateformed, ["MM-DD-YYYY", "YYYY-MM-DD"],'fr').add('days', numberofnights).calendar();
	   
	   //alert("newdepdarture-------->"+newdepdarture);
	
	
	var checkoutDate = newdepdarture;

	// var setdepdate = document.getElementById("depart_date");
	var newrangedate = checkinDate + "-" + checkoutDate;

	// alert("new range-------->"+newrangedate);
	var newdaterangetext = document.getElementById("hotel-arrival-date");
	newdaterangetext.value = newrangedate;
	// setdepdate.value = newfordepdate;
}



function setNoOfNightsToDateRange() {

	var rangedate = document.getElementById('hotel-arrival-date').value;
	// alert("date range---------->"+rangedate);

//	var rangedate = document.getElementById('date-range103').value;
	var chekingcheckoutdate = rangedate.split("-");
	var checkinDate = chekingcheckoutdate[0].trim();
	var checkoutDate = chekingcheckoutdate[1].trim();
	
	

	var cin = checkinDate.split('/');
	var cout = checkoutDate.split('/');

	var newcin = cin[1] + "/" + cin[0] + "/" + cin[2];
	var newcout = cout[1] + "/" + cout[0] + "/" + cout[2];

	// alert(newcin);
	// alert(newcout);

	var date1 = new Date(newcin);
	var date2 = new Date(newcout);
	var diffDays = date2.getTime() - date1.getTime();
	// alert(diffDays/(1000 * 60 * 60 * 24));
	var dif = diffDays / (1000 * 60 * 60 * 24)
	var numberofngt = Number(dif) - 1;
	var selectnights = document.getElementById("h-nights");
	selectnights[numberofngt].selected = true;


}

function validateInfantcount(){
	//alert("validateInfantcount------------->");
	
	var adultcmb = document.getElementById('air_adultCmb');
	var adultCount = adultcmb.options[adultcmb.selectedIndex].value;
	//alert("adultCount------------->"+adultCount);
	
	
	var infantCmb = document.getElementById('air_infantCmb');
	var infantCount = infantCmb.options[infantCmb.selectedIndex].value;
//	alert("infantCount------------->"+infantCount);
	//var numberofnights = Number(nights);
	
	var adultCountN = Number(adultCount);
	var infantCountN = Number(infantCount);
	
	if(infantCountN> adultCountN){
		
		show_error_msg("Only 1 infant per adult permitted.");
	}
	
	
}
