/**
 * Used to resolve confilicting $ handler with other libraries.
 */
//jQuery.noConflict();

var jsonDataList = '';
var serUrl = 'http://bookings.flightsite.co.za/flightsightv3/admin/common/LocationData';

/**
 * Sort by a given field of a json object in an ascending or descending order
 * performing a specified operation on values.
 * 
 * @param field - string - name of the field the sort is performed.
 * @param reverse - boolean - specify the sorted order is ascending or descending.
 * @param primer - function - operation to be performed to the sorting value.
 */
function sort_by(field, reverse, primer){
//	console.log("Call")
	   reverse = (reverse) ? -1 : 1;

	   return function(a,b){
	       a = a[field];
	       b = b[field];

	       if (typeof(primer) != 'undefined'){
	           a = primer(a);
	           b = primer(b);
	       }

	       if (a<b) return reverse * -1;
	       if (a>b) return reverse * 1;
	       return 0;

	   }
	};

/**
 * This defines the type of list to load according to the search engine type.
 * 
 * @param engine
 * @return
 */
function searchTypeSelector(engine){
	$(".selector").autocomplete({ autoFocus: true });
    var bookingType = document.getElementsByName("engType1");
    var type = "citylist";
//    console.log(engine);
    for (var index = 0; index < bookingType.length; index++) {
        if(jQuery(bookingType[index]).is(':checked')){
            var value = jQuery(bookingType[index]).val();
            switch(value){
                     case 'H':
                    type = "citylist";
                    break;
                case 'A':
                    type = "activitylocationlist";
                    break;
                case 'V':
                    type = "airportlist";
                    break;
                case 'C':
                    type = "carlocationlist";
                    break;
                case 'F':
                    type = "airportlist";
                    break;
                case 'FH':
                    type = "airportlist";
                    break;
                case 'FC':
                    type = "airportlist";
                    break;
                case 'FHC':
                    type = "airportlist";
                    break;
                case 'FHA':
                    type = "airportlist";
                    break;
            }
        } else {
//        	console.log(engine);
        	switch(value){
            	case 'H':
                	type = "citylist";
                	break;
            	case 'A':
                	type = "activitylocationlist";
                	break;
            	case 'V':
                	type = "airportlist";
                	break;
            	case 'C':
                	type = "carlocationlist";
                	break;
            	case 'F':
                	type = "airportlist";
                	break;
            	case 'FH':
                	type = "airportlist";
                	break;
            	case 'FC':
                	type = "airportlist";
                	break;
            	case 'FHC':
                	type = "airportlist";
                	break;
            	case 'FHA':
                	type = "airportlist";
                	break;
        	}
        }
    }
	
    if(bookingType.length == 0 || bookingType == null || bookingType == "undefined"){
        switch(engine){
            case 'H':
                type = "citylist";
                break;
            case 'A':
                type = "activitylocationlist";
                break;
            case 'V':
                type = "airportlist";
                break;
            case 'C':
                type = "carlocationlist";
                break;
            case 'F':
                type = "airportlist";
                break;
            case 'HC':
                type = "citylist";
                break;
            case 'FH':
                type = "airportlist";
                break;
            case 'FC':
                type = "airportlist";
                break;
            case 'FHC':
                type = "airportlist";
                break;
            case 'FHA':
                type = "airportlist";
                break;
        }
    } else {
    	switch(engine){
        	case 'H':
            	type = "citylist";
            	break;
        	case 'A':
            	type = "activitylocationlist";
            	break;
        	case 'V':
            	type = "airportlist";
            	break;
        	case 'C':
            	type = "carlocationlist";
            	break;
        	case 'F':
            	type = "airportlist";
            	break;
        	case 'HC':
                type = "citylist";
                break;
        	case 'FH':
            	type = "airportlist";
            	break;
        	case 'FC':
            	type = "airportlist";
            	break;
        	case 'FHC':
            	type = "airportlist";
            	break;
        	case 'FHA':
            	type = "airportlist";
            	break;
    	}
    }
    return type;
};

function matcher(regex, text){
    var filter = new RegExp(
        "(^)(" +
        regex +
        ")(?![^<>]*>)(?![^&;]+;)", "gi"
        ).test(text);
    return filter;
};

function getDestinationCountry(){
	try{
		var sel = document.getElementById("destinationcountry");
		if (sel != null || sel != "" || sel != "undefined") {
			var country = sel.options[sel.selectedIndex].innerHTML;
			if (country == null || country == "" || country == "undefined" || jQuery.trim(country) == "Select Country"){
				return;
			} else {
				return jQuery.trim(country);
			}
		} else {
			return;
		}
	}catch(e){}
}

/**
 * Create label to display in the lookup list
 * @param item
 * @return
 */
function createLabel(item, type){
	var label = '';
	if(type == 'city'){
		if (item.sna != ''){
			label = item.cnm + ",  " + item.sna + ",  " + item.cna;
		} else {
			label = item.cnm + ",  " + item.cna;
		}
	} else if(type == 'car'){
		label = item.cna + " - "  + item.cnm;
	} else {
		label = item.cna + " - " + item.apcd + " - " + item.apnm + " - " + item.cnm;
	}
	return label;
}

function sortByProperty(property) {
    'use strict';
    return function (a, b) {
        var sortStatus = 0;
        if (a[property] < b[property]) {
            sortStatus = -1;
        } else if (a[property] > b[property]) {
            sortStatus = 1;
        }
 
        return sortStatus;
    };
}

$( document ).ready(function() {
	
	airport.sort(sortByProperty('pid'));
	
	$( "#air_Loc_mul_0" ).autocomplete({
		minLength : 1,
		source:function( request, response ) {
		        	var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( request.term ), "i" );    		        	
		                    response($.map(airport, function(item) {    		                    			                    	
		                    	 if(matcher.test( item.cna)|| matcher.test( item.apcd)){
		                    		 var text = createLabel(item, 'airport');           
		                    		 return {
		    		                        label: text.replace(new RegExp(
		                                            "(?![^&;]+;)(?!<[^<>]*)("
		                                            + request.term
		                                            + ")(?![^<>]*>)(?![^&;]+;)",
		                                            "gi"),
		                                        "$1"),
		    		                        value: item.cna,
		    		                        searchvalue : item.cna + "," + item.apcd + "," + item.cid + "," + item.apnm + "," + item.sid + "," + item.cnid  + "," + item.sna + "," + item.cnm
		                                     };    		                             
		                         }    	
		                    	
		                }).slice(0,15));    		        
		    },
		    selectFirst: true,
		    autoFocus: true,    		    
		    focus: function( event, ui ) { event.preventDefault(); }   	
		});
	
	  
	$( "#air_Loc1_mul_0" ).autocomplete({
		minLength : 1,
		source:function( request, response ) {
		        	var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( request.term ), "i" );    		        	
		                    response($.map(airport, function(item) {    		                    			                    	
		                    	 if(matcher.test( item.cna)|| matcher.test( item.apcd)){
		                    		 var text = createLabel(item, 'airport');           
		                    		 return {
		    		                        label: text.replace(new RegExp(
		                                            "(?![^&;]+;)(?!<[^<>]*)("
		                                            + request.term
		                                            + ")(?![^<>]*>)(?![^&;]+;)",
		                                            "gi"),
		                                        "$1"),
		    		                        value: item.cna,
		    		                        searchvalue : item.cna + "," + item.apcd + "," + item.cid + "," + item.apnm + "," + item.sid + "," + item.cnid  + "," + item.sna + "," + item.cnm
		                                     };    		                             
		                         }    	
		                    	
		                }).slice(0,15));    		        
		    },
		    selectFirst: true,
		    autoFocus: true,    		    
		    focus: function( event, ui ) { event.preventDefault(); }   	
		});
	
	
	
	$( "#air_Loc_mul_1" ).autocomplete({
		minLength : 1,
		source:function( request, response ) {
		        	var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( request.term ), "i" );    		        	
		                    response($.map(airport, function(item) {    		                    			                    	
		                    	 if(matcher.test( item.cna)|| matcher.test( item.apcd)){
		                    		 var text = createLabel(item, 'airport');           
		                    		 return {
		    		                        label: text.replace(new RegExp(
		                                            "(?![^&;]+;)(?!<[^<>]*)("
		                                            + request.term
		                                            + ")(?![^<>]*>)(?![^&;]+;)",
		                                            "gi"),
		                                        "$1"),
		    		                        value: item.cna,
		    		                        searchvalue : item.cna + "," + item.apcd + "," + item.cid + "," + item.apnm + "," + item.sid + "," + item.cnid  + "," + item.sna + "," + item.cnm
		                                     };    		                             
		                         }    	
		                    	
		                }).slice(0,15));    		        
		    },
		    selectFirst: true,
		    autoFocus: true,    		    
		    focus: function( event, ui ) { event.preventDefault(); }   	
		});
	
	
	
	$( "#air_Loc1_mul_1" ).autocomplete({
		minLength : 1,
		source:function( request, response ) {
		        	var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( request.term ), "i" );    		        	
		                    response($.map(airport, function(item) {    		                    			                    	
		                    	 if(matcher.test( item.cna)|| matcher.test( item.apcd)){
		                    		 var text = createLabel(item, 'airport');           
		                    		 return {
		    		                        label: text.replace(new RegExp(
		                                            "(?![^&;]+;)(?!<[^<>]*)("
		                                            + request.term
		                                            + ")(?![^<>]*>)(?![^&;]+;)",
		                                            "gi"),
		                                        "$1"),
		    		                        value: item.cna,
		    		                        searchvalue : item.cna + "," + item.apcd + "," + item.cid + "," + item.apnm + "," + item.sid + "," + item.cnid  + "," + item.sna + "," + item.cnm
		                                     };    		                             
		                         }    	
		                    	
		                }).slice(0,15));    		        
		    },
		    selectFirst: true,
		    autoFocus: true,    		    
		    focus: function( event, ui ) { event.preventDefault(); }   	
		});
	
	
	
	$( "#air_Loc_mul_2" ).autocomplete({
		minLength : 1,
		source:function( request, response ) {
		        	var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( request.term ), "i" );    		        	
		                    response($.map(airport, function(item) {    		                    			                    	
		                    	 if(matcher.test( item.cna)|| matcher.test( item.apcd)){
		                    		 var text = createLabel(item, 'airport');           
		                    		 return {
		    		                        label: text.replace(new RegExp(
		                                            "(?![^&;]+;)(?!<[^<>]*)("
		                                            + request.term
		                                            + ")(?![^<>]*>)(?![^&;]+;)",
		                                            "gi"),
		                                        "$1"),
		    		                        value: item.cna,
		    		                        searchvalue : item.cna + "," + item.apcd + "," + item.cid + "," + item.apnm + "," + item.sid + "," + item.cnid  + "," + item.sna + "," + item.cnm
		                                     };    		                             
		                         }    	
		                    	
		                }).slice(0,15));    		        
		    },
		    selectFirst: true,
		    autoFocus: true,    		    
		    focus: function( event, ui ) { event.preventDefault(); }   	
		});
	
	
	
	$( "#air_Loc1_mul_2" ).autocomplete({
		minLength : 1,
		source:function( request, response ) {
		        	var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( request.term ), "i" );    		        	
		                    response($.map(airport, function(item) {    		                    			                    	
		                    	 if(matcher.test( item.cna)|| matcher.test( item.apcd)){
		                    		 var text = createLabel(item, 'airport');           
		                    		 return {
		    		                        label: text.replace(new RegExp(
		                                            "(?![^&;]+;)(?!<[^<>]*)("
		                                            + request.term
		                                            + ")(?![^<>]*>)(?![^&;]+;)",
		                                            "gi"),
		                                        "$1"),
		    		                        value: item.cna,
		    		                        searchvalue : item.cna + "," + item.apcd + "," + item.cid + "," + item.apnm + "," + item.sid + "," + item.cnid  + "," + item.sna + "," + item.cnm
		                                     };    		                             
		                         }    	
		                    	
		                }).slice(0,15));    		        
		    },
		    selectFirst: true,
		    autoFocus: true,    		    
		    focus: function( event, ui ) { event.preventDefault(); }   	
		});
	
	
	
	$( "#air_Loc_mul_3" ).autocomplete({
		minLength : 1,
		source:function( request, response ) {
		        	var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( request.term ), "i" );    		        	
		                    response($.map(airport, function(item) {    		                    			                    	
		                    	 if(matcher.test( item.cna)|| matcher.test( item.apcd)){
		                    		 var text = createLabel(item, 'airport');           
		                    		 return {
		    		                        label: text.replace(new RegExp(
		                                            "(?![^&;]+;)(?!<[^<>]*)("
		                                            + request.term
		                                            + ")(?![^<>]*>)(?![^&;]+;)",
		                                            "gi"),
		                                        "$1"),
		    		                        value: item.cna,
		    		                        searchvalue : item.cna + "," + item.apcd + "," + item.cid + "," + item.apnm + "," + item.sid + "," + item.cnid  + "," + item.sna + "," + item.cnm
		                                     };    		                             
		                         }    	
		                    	
		                }).slice(0,15));    		        
		    },
		    selectFirst: true,
		    autoFocus: true,    		    
		    focus: function( event, ui ) { event.preventDefault(); }   	
		});
	
	
	
	$( "#air_Loc1_mul_3" ).autocomplete({
		minLength : 1,
		source:function( request, response ) {
		        	var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( request.term ), "i" );    		        	
		                    response($.map(airport, function(item) {    		                    			                    	
		                    	 if(matcher.test( item.cna)|| matcher.test( item.apcd)){
		                    		 var text = createLabel(item, 'airport');           
		                    		 return {
		    		                        label: text.replace(new RegExp(
		                                            "(?![^&;]+;)(?!<[^<>]*)("
		                                            + request.term
		                                            + ")(?![^<>]*>)(?![^&;]+;)",
		                                            "gi"),
		                                        "$1"),
		    		                        value: item.cna,
		    		                        searchvalue : item.cna + "," + item.apcd + "," + item.cid + "," + item.apnm + "," + item.sid + "," + item.cnid  + "," + item.sna + "," + item.cnm
		                                     };    		                             
		                         }    	
		                    	
		                }).slice(0,15));    		        
		    },
		    selectFirst: true,
		    autoFocus: true,    		    
		    focus: function( event, ui ) { event.preventDefault(); }   	
		});
	
	
	$( "#air_Loc_mul_4" ).autocomplete({
		minLength : 1,
		source:function( request, response ) {
		        	var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( request.term ), "i" );    		        	
		                    response($.map(airport, function(item) {    		                    			                    	
		                    	 if(matcher.test( item.cna)|| matcher.test( item.apcd)){
		                    		 var text = createLabel(item, 'airport');           
		                    		 return {
		    		                        label: text.replace(new RegExp(
		                                            "(?![^&;]+;)(?!<[^<>]*)("
		                                            + request.term
		                                            + ")(?![^<>]*>)(?![^&;]+;)",
		                                            "gi"),
		                                        "$1"),
		    		                        value: item.cna,
		    		                        searchvalue : item.cna + "," + item.apcd + "," + item.cid + "," + item.apnm + "," + item.sid + "," + item.cnid  + "," + item.sna + "," + item.cnm
		                                     };    		                             
		                         }    	
		                    	
		                }).slice(0,15));    		        
		    },
		    selectFirst: true,
		    autoFocus: true,    		    
		    focus: function( event, ui ) { event.preventDefault(); }   	
		});
	
	$( "#air_Loc1_mul_4" ).autocomplete({
		minLength : 1,
		source:function( request, response ) {
		        	var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( request.term ), "i" );    		        	
		                    response($.map(airport, function(item) {    		                    			                    	
		                    	 if(matcher.test( item.cna)|| matcher.test( item.apcd)){
		                    		 var text = createLabel(item, 'airport');           
		                    		 return {
		    		                        label: text.replace(new RegExp(
		                                            "(?![^&;]+;)(?!<[^<>]*)("
		                                            + request.term
		                                            + ")(?![^<>]*>)(?![^&;]+;)",
		                                            "gi"),
		                                        "$1"),
		    		                        value: item.cna,
		    		                        searchvalue : item.cna + "," + item.apcd + "," + item.cid + "," + item.apnm + "," + item.sid + "," + item.cnid  + "," + item.sna + "," + item.cnm
		                                     };    		                             
		                         }    	
		                    	
		                }).slice(0,15));    		        
		    },
		    selectFirst: true,
		    autoFocus: true,    		    
		    focus: function( event, ui ) { event.preventDefault(); }   	
		});


/**
	* new Autocomplete for hotels
	 */


$( "#H_Loc" ).autocomplete({
	minLength : 1,
	source:function( request, response ) {
	        	var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( request.term ), "i" );    		        	
	                    response($.map(hotels, function(item) {    		                    			                    	
	                    	 if(matcher.test( item.cna)){
	                    		 var text = createLabel(item, 'city');           
	                    		 return {
	    		                        label: text.replace(new RegExp(
	                                            "(?![^&;]+;)(?!<[^<>]*)("
	                                            + request.term
	                                            + ")(?![^<>]*>)(?![^&;]+;)",
	                                            "gi"),
	                                        "$1"),
	    		                        value: item.cna,
	    		                        searchvalue : item.cid + "," + item.cna + "," + item.sid + "," + item.cnid + "," + item.sna + "," + item.cnm
	                                     };    		                             
	                         }    	
	                    	
	                }).slice(0,15));    		        
	    },
	    selectFirst: true,
	    autoFocus: true,    		    
	    focus: function( event, ui ) { event.preventDefault(); }   	
	});
	
	
//     }
// });    	
	});




/* Autocomplete widgets */
jQuery(function() {
	
    var cache = {};
    var search_type = searchTypeSelector();
    var selected_data = null;
//    var country = jQuery("#destinationcountry option:selected").text();
//    var country = getDestinationCountry();
    
    /**
		 * Autocomplete for hotels
		 */
    /*jQuery( "#H_Loc" ).autocomplete({
        minLength : 3, delay : 500,
        source: function(request, response) {
            if ( request.term in cache ) {
                response( cache[ request.term ] );
                return;
            }
					
            jQuery.ajax({
                url: serUrl,
                dataType: "script",
                xhr: function () {
                    if (jQuery.browser.msie && jQuery.browser.msie){
                        return new ActiveXObject("Microsoft.XMLHTTP");
                    } else {
                        return new XMLHttpRequest();
                    }
                },
                data: {
                    term: request.term,
                    search_type: search_type,
					bookingEngine: "Y",
					callBack: "callbackCities"
                    //search_country: getDestinationCountry()
                },
                success: function() {
                   
                    var returnData = jQuery.map(
			jsonDataList.list.sort(sort_by('pid', false, function(a){return a})),
			function(item) {
                        var text = createLabel(item, 'city');
                        if(matcher(request.term, item.cna) || matcher(request.term, item.sbna)){
                            return {
                                label: text.replace(new RegExp(
                                    "(?![^&;]+;)(?!<[^<>]*)(" +
                                    request.term +
                                    ")(?![^<>]*>)(?![^&;]+;)", "gi"
                                    ), "$1"),
                                value: item.cna,
                                searchvalue:item.cid+","+item.cna+","+item.sid+","+item.cnid+","+item.sna+","+item.cnm+","+item.sbid
                            }
                        }
                    }); 
                    response(returnData);
                    cache[ request.term ] = returnData;

                }
            });
        }
    });*/
		
    /**
		 * Autocomplete for activities
		 */
	jQuery( "#activity_Loc" ).autocomplete({
        minLength : 3, delay : 500,
        source: function(request, response) {
            if ( request.term in cache ) {
                response( cache[ request.term ] );
                return;
            }
					
            jQuery.ajax({
                url: serUrl,
                dataType: "script",
                xhr: function () {
                    if (jQuery.browser.msie && jQuery.browser.msie){
                        return new ActiveXObject("Microsoft.XMLHTTP");
                    } else {
                        return new XMLHttpRequest();
                    }
                },
                data: {
                    term: request.term,
                    search_type: search_type,
					bookingEngine: "Y",
					callBack: "callbackCities"
                    //search_country: getDestinationCountry()
                },
                success: function() {
                   
                    var returnData = jQuery.map(jsonDataList.list.sort(sort_by('cna', false, function(a){return a})), function(item) {
                        var text = createLabel(item, 'city');
                        if(matcher(request.term, item.cna) || matcher(request.term, item.sbna)){
                            return {
                                label: text.replace(new RegExp(
                                    "(?![^&;]+;)(?!<[^<>]*)(" +
                                    request.term +
                                    ")(?![^<>]*>)(?![^&;]+;)", "gi"
                                    ), "$1"),
                                value: item.cna,
                                searchvalue:item.cid+","+item.cna+","+item.sid+","+item.cnid+","+item.sna+","+item.cnm+","+item.sbid
                            }
                        }
                    }); 
                    response(returnData);
                    cache[ request.term ] = returnData;

                }
            });
        }
    });
    
		
    /**
		 * Autocomplete for hotels and activities.
		 */
    jQuery( "#autocomplete_HA" ).autocomplete({
        minLength : 3, delay : 500,
        source: function(request, response) {
            if ( request.term in cache ) {
                response( cache[ request.term ] );
                return;
            }
				
            jQuery.ajax({
                url: "../../admin/common/LocationData",
                dataType: "json",
                xhr: function () {
                    if (jQuery.browser.msie && jQuery.browser.msie){
                        return new ActiveXObject("Microsoft.XMLHTTP");
                    } else {
                        return new XMLHttpRequest();
                    }
                },
                data: {
                    term: request.term,
                    search_type: search_type,
                    search_country: getDestinationCountry()
                },
                success: function(data) {
                	var returnData = jQuery.map(data.list.sort(sort_by('cna', false, function(a){return a})), function(item) {
                    	var text = createLabel(item, 'city');
                        if(matcher(request.term, item.cna) || matcher(request.term, item.sbna)){
                            return {
                                label: text.replace(new RegExp(
                                    "(?![^&;]+;)(?!<[^<>]*)(" +
                                    request.term +
                                    ")(?![^<>]*>)(?![^&;]+;)", "gi"
                                    ), "<strong>$1</strong>"),
                                value: item.cna,
                                searchvalue:item.cid+"|"+item.cna+"|"+item.sid+"|"+item.cnid+"|"+item.sna+"|"+item.cnm+"|"+item.sbid
                            }
                        }
                    });
                    cache[ request.term ] = returnData;
                    response(returnData)

                }
            });
        }
    });
		
    /**
		 * Autocomplete for hotels and activities.
		 */
    jQuery( "#autocomplete" ).autocomplete({
        minLength : 3, delay : 500,
        source: function(request, response) {
            if ( request.term in cache ) {
                response( cache[ request.term ] );
                return;
            }
				
            jQuery.ajax({
                url: "../../admin/common/LocationData",
                dataType: "json",
                xhr: function () {
                    if (jQuery.browser.msie && jQuery.browser.msie){
                        return new ActiveXObject("Microsoft.XMLHTTP");
                    } else {
                        return new XMLHttpRequest();
                    }
                },
                data: {
                    term: request.term,
                    search_type: search_type,
                    search_country: getDestinationCountry()
                },
                success: function(data) {
                	var returnData = jQuery.map(data.list.sort(sort_by('cna', false, function(a){return a})), function(item) {
                    	var text = createLabel(item, 'city');
                        if(matcher(request.term, item.cna) || matcher(request.term, item.sbna)){
                            return {
                                label: text.replace(new RegExp(
                                    "(?![^&;]+;)(?!<[^<>]*)(" +
                                    request.term +
                                    ")(?![^<>]*>)(?![^&;]+;)", "gi"
                                    ), "<strong>$1</strong>"),
                                value: item.cna,
                                searchvalue:item.cid+"|"+item.cna+"|"+item.sid+"|"+item.cnid+"|"+item.sna+"|"+item.cnm+"|"+item.sbid
                            }
                        }
                    });
                    cache[ request.term ] = returnData;
                    response(returnData);

                }
            });
        }
    });
		
    /**
		 * Autocomplete for Holidays departure location.
		 */
    jQuery("#V_DepFrom").autocomplete({
        minLength : 3, delay : 500,
        source : function(request, response) {
            if (request.term in cache) {
                response(cache[request.term]);
                return;
            }

            jQuery
            .ajax( {
                url: serUrl,
                dataType : "script",
                xhr: function () {
                    if (jQuery.browser.msie && jQuery.browser.msie){
                        return new ActiveXObject("Microsoft.XMLHTTP");
                    } else {
                        return new XMLHttpRequest();
                    }
                },
                data : {
                    term : request.term,
                    search_type : searchTypeSelector('V'),
					callBack: "callbackCities",
					bookingEngine: "Y"
                    //search_country: getDestinationCountry()
                },
                success : function(data) {
                   
                    var returnData = jQuery.map(
                    		jsonDataList.list.sort(sort_by('pid', false, function(a){return a})),
                            function(item) {
                                var text = createLabel(item, 'airport');                                
                                if (matcher(request.term, item.cna) || matcher(request.term, item.apcd)) {
                                    return {
                                        label : text.replace(new RegExp(
                                                "(?![^&;]+;)(?!<[^<>]*)("
                                                + request.term
                                                + ")(?![^<>]*>)(?![^&;]+;)",
                                                "gi"),
                                            "$1"),
                                        value : item.cna,
                                        searchvalue : item.cna + "," + item.apcd + "," + item.cid + "," + item.apnm + "," + item.sid + "," + item.cnid  + "," + item.sna + "," + item.cnm
                                    }
                                }
                            });
                    cache[request.term] = returnData;
                    response(returnData)

                }
            });
        }
    });

    /**
		 * Autocomplete for Holidays return location.
		 */
    jQuery("#V_RetLoc").autocomplete({
        minLength : 3, delay : 500,
        source : function(request, response) {
            if (request.term in cache) {
                response(cache[request.term]);
                return;
            }

            jQuery
            .ajax( {
                url: serUrl,
                dataType : "script",
                xhr: function () {
                    if (jQuery.browser.msie && jQuery.browser.msie){
                        return new ActiveXObject("Microsoft.XMLHTTP");
                    } else {
                        return new XMLHttpRequest();
                    }
                },
                data : {
                    term : request.term,
                    search_type : searchTypeSelector('V'),
					callBack: "callbackCities",
					bookingEngine: "Y"
                    //search_country: getDestinationCountry()
                },
                success : function(data) {
                	var returnData = jQuery.map(
                    		jsonDataList.list.sort(sort_by('pid', false, function(a){return a})),
                            function(item) {
                    			var text = createLabel(item, 'airport');                                
                                if (matcher(request.term, item.cna) || matcher(request.term, item.apcd)) {
                                    return {
                                        label : text.replace(new RegExp(
                                                "(?![^&;]+;)(?!<[^<>]*)("
                                                + request.term
                                                + ")(?![^<>]*>)(?![^&;]+;)",
                                                "gi"),
                                            "$1"),
                                        value : item.cna,
                                        searchvalue : item.cna + "," + item.apcd + "," + item.cid + "," + item.apnm + "," + item.sid + "," + item.cnid  + "," + item.sna + "," + item.cnm
                                    }
                                }
                            });
                    cache[request.term] = returnData;
                    response(returnData)

                }
            });
        }
    });
    
    /**
	 * Autocomplete for Flights departure location.
	 */
//jQuery("#air_Loc_mul_0").autocomplete({
//    minLength : 3, delay : 500,
//    source : function(request, response) {
//        if (request.term in cache) {
//            response(cache[request.term]);
//            return;
//        }
//
//        jQuery
//        .ajax( {
//            url: serUrl,
//            dataType : "script",
//            xhr: function () {
//                if (jQuery.browser.msie && jQuery.browser.msie){
//                    return new ActiveXObject("Microsoft.XMLHTTP");
//                } else {
//                    return new XMLHttpRequest();
//                }
//            },
//            data : {
//                term : request.term,
//                search_type : searchTypeSelector('F'),
//							bookingEngine: "Y",
//					callBack: "callbackCities"                
//		//search_country: getDestinationCountry()
//            },
//            success : function() {
//            	var returnData = jQuery.map(
//                		jsonDataList.list.sort(sort_by('pid', false, function(a){return a})),
//                        function(item) {
//                			var text = createLabel(item, 'airport');                                
//                            if (matcher(request.term, item.cna) || matcher(request.term, item.apcd)) {
//                                return {
//                                    label : text.replace(new RegExp(
//                                            "(?![^&;]+;)(?!<[^<>]*)("
//                                            + request.term
//                                            + ")(?![^<>]*>)(?![^&;]+;)",
//                                            "gi"),
//                                        "<strong>$1</strong>"),
//                                    value : item.cna,
//                                    searchvalue : item.cna + "," + item.apcd + "," + item.cid + "," + item.apnm + "," + item.sid + "," + item.cnid  + "," + item.sna + "," + item.cnm
//                                }
//                            }
//                        });
//                cache[request.term] = returnData;
//                response(returnData);
//
//            }
//        });
//    }
//});

/**
	 * Autocomplete for Flights return location.
	 */
//jQuery("#air_Loc1_mul_0").autocomplete({
//    minLength : 3, delay : 500,
//    source : function(request, response) {
//        if (request.term in cache) {
//            response(cache[request.term]);
//            return;
//        }
//
//        jQuery
//        .ajax( {
//            url: serUrl,
//            dataType : "script",
//            xhr: function () {
//                if (jQuery.browser.msie && jQuery.browser.msie){
//                    return new ActiveXObject("Microsoft.XMLHTTP");
//                } else {
//                    return new XMLHttpRequest();
//                }
//            },
//            data : {
//                term : request.term,
//                search_type : searchTypeSelector('F'),
//		bookingEngine: "Y",
//		callBack: "callbackCities" 
//               // search_country: getDestinationCountry()
//            },
//            success : function(data) {
//            	var returnData = jQuery.map(
//                		jsonDataList.list.sort(sort_by('pid', false, function(a){return a})),
//                        function(item) {
//                			var text = createLabel(item, 'airport');                                
//                            if (matcher(request.term, item.cna) || matcher(request.term, item.apcd)) {
//                                return {
//                                    label : text.replace(new RegExp(
//                                            "(?![^&;]+;)(?!<[^<>]*)("
//                                            + request.term
//                                            + ")(?![^<>]*>)(?![^&;]+;)",
//                                            "gi"),
//                                        "<strong>$1</strong>"),
//                                    value : item.cna,
//                                    searchvalue : item.cna + "," + item.apcd + "," + item.cid + "," + item.apnm + "," + item.sid + "," + item.cnid  + "," + item.sna + "," + item.cnm
//                                }
//                            }
//                        });
//                cache[request.term] = returnData;
//                response(returnData)
//
//            }
//        });
//    }
//});

/**
 * Autocomplete search again for Flights departure location.
 */
jQuery("#depLocNameNew").autocomplete({
minLength : 3, delay : 500,
source : function(request, response) {
    if (request.term in cache) {
        response(cache[request.term]);
        return;
    }

    jQuery
    .ajax( {
        url : "../../admin/common/LocationData",
        dataType : "json",
        xhr: function () {
            if (jQuery.browser.msie && jQuery.browser.msie){
                return new ActiveXObject("Microsoft.XMLHTTP");
            } else {
                return new XMLHttpRequest();
            }
        },
        data : {
            term : request.term,
            search_type : searchTypeSelector('F'),
            search_country: getDestinationCountry()
        },
        success : function(data) {
        	var returnData = jQuery.map(
            		data.list.sort(sort_by('pid', false, function(a){return a})),
                    function(item) {
            			var text = createLabel(item, 'airport');                                
                        if (matcher(request.term, item.cna) || matcher(request.term, item.apcd)) {
                            return {
                                label : text.replace(new RegExp(
                                        "(?![^&;]+;)(?!<[^<>]*)("
                                        + request.term
                                        + ")(?![^<>]*>)(?![^&;]+;)",
                                        "gi"),
                                    "<strong>$1</strong>"),
                                value : item.cna,
                                searchvalue : item.cna + "|" + item.apcd + "|" + item.cid + "|" + item.apnm + "|" + item.sid + "|" + item.cnid  + "|" + item.sna + "|" + item.cnm
                            }
                        }
                    });
            cache[request.term] = returnData;
            response(returnData);

        }
    });
}
});

/**
 * Autocomplete search again for Flights return location.
 */
jQuery("#arrLocNameNew").autocomplete({
minLength : 3, delay : 500,
source : function(request, response) {
    if (request.term in cache) {
        response(cache[request.term]);
        return;
    }

    jQuery
    .ajax( {
        url : "../../admin/common/LocationData",
        dataType : "json",
        xhr: function () {
            if (jQuery.browser.msie && jQuery.browser.msie){
                return new ActiveXObject("Microsoft.XMLHTTP");
            } else {
                return new XMLHttpRequest();
            }
        },
        data : {
            term : request.term,
            search_type : searchTypeSelector('F'),
            search_country: getDestinationCountry()
        },
        success : function(data) {
        	var returnData = jQuery.map(
            		data.list.sort(sort_by('pid', false, function(a){return a})),
                    function(item) {
            			var text = createLabel(item, 'airport');                                
                        if (matcher(request.term, item.cna) || matcher(request.term, item.apcd)) {
                            return {
                                label : text.replace(new RegExp(
                                        "(?![^&;]+;)(?!<[^<>]*)("
                                        + request.term
                                        + ")(?![^<>]*>)(?![^&;]+;)",
                                        "gi"),
                                    "<strong>$1</strong>"),
                                value : item.cna,
                                searchvalue : item.cna + "|" + item.apcd + "|" + item.cid + "|" + item.apnm + "|" + item.sid + "|" + item.cnid  + "|" + item.sna + "|" + item.cnm
                            }
                        }
                    });
            cache[request.term] = returnData;
            response(returnData)

        }
    });
}
});
 /**
	 * Autocomplete for Flights departure location.
	 */

//jQuery("#air_Loc_mul_1").autocomplete({
//    minLength : 3, delay : 500,
//    source : function(request, response) {
//        if (request.term in cache) {
//            response(cache[request.term]);
//            return;
//        }
//
//        jQuery
//        .ajax( {
//            url: serUrl,
//            dataType : "script",
//            xhr: function () {
//                if (jQuery.browser.msie && jQuery.browser.msie){
//                    return new ActiveXObject("Microsoft.XMLHTTP");
//                } else {
//                    return new XMLHttpRequest();
//                }
//            },
//            data : {
//                term : request.term,
//                search_type : searchTypeSelector('F'),
//							bookingEngine: "Y",
//					callBack: "callbackCities"                
//		//search_country: getDestinationCountry()
//            },
//            success : function() {
//            	var returnData = jQuery.map(
//                		jsonDataList.list.sort(sort_by('pid', false, function(a){return a})),
//                        function(item) {
//                			var text = createLabel(item, 'airport');                                
//                            if (matcher(request.term, item.cna) || matcher(request.term, item.apcd)) {
//                                return {
//                                    label : text.replace(new RegExp(
//                                            "(?![^&;]+;)(?!<[^<>]*)("
//                                            + request.term
//                                            + ")(?![^<>]*>)(?![^&;]+;)",
//                                            "gi"),
//                                        "<strong>$1</strong>"),
//                                    value : item.cna,
//                                    searchvalue : item.cna + "," + item.apcd + "," + item.cid + "," + item.apnm + "," + item.sid + "," + item.cnid  + "," + item.sna + "," + item.cnm
//                                }
//                            }
//                        });
//                cache[request.term] = returnData;
//                response(returnData);
//
//            }
//        });
//    }
//});
//
///**
//	 * Autocomplete for Flights return location.
//	 */
//
//jQuery("#air_Loc1_mul_1").autocomplete({
//    minLength : 3, delay : 500,
//    source : function(request, response) {
//        if (request.term in cache) {
//            response(cache[request.term]);
//            return;
//        }
//
//        jQuery
//        .ajax( {
//            url: serUrl,
//            dataType : "script",
//            xhr: function () {
//                if (jQuery.browser.msie && jQuery.browser.msie){
//                    return new ActiveXObject("Microsoft.XMLHTTP");
//                } else {
//                    return new XMLHttpRequest();
//                }
//            },
//            data : {
//                term : request.term,
//                search_type : searchTypeSelector('F'),
//		bookingEngine: "Y",
//		callBack: "callbackCities" 
//               // search_country: getDestinationCountry()
//            },
//            success : function(data) {
//            	var returnData = jQuery.map(
//                		jsonDataList.list.sort(sort_by('pid', false, function(a){return a})),
//                        function(item) {
//                			var text = createLabel(item, 'airport');                                
//                            if (matcher(request.term, item.cna) || matcher(request.term, item.apcd)) {
//                                return {
//                                    label : text.replace(new RegExp(
//                                            "(?![^&;]+;)(?!<[^<>]*)("
//                                            + request.term
//                                            + ")(?![^<>]*>)(?![^&;]+;)",
//                                            "gi"),
//                                        "<strong>$1</strong>"),
//                                    value : item.cna,
//                                    searchvalue : item.cna + "," + item.apcd + "," + item.cid + "," + item.apnm + "," + item.sid + "," + item.cnid  + "," + item.sna + "," + item.cnm
//                                }
//                            }
//                        });
//                cache[request.term] = returnData;
//                response(returnData)
//
//            }
//        });
//    }
//});  
//jQuery("#air_Loc_mul_2").autocomplete({
//    minLength : 3, delay : 500,
//    source : function(request, response) {
//        if (request.term in cache) {
//            response(cache[request.term]);
//            return;
//        }
//
//        jQuery
//        .ajax( {
//            url: serUrl,
//            dataType : "script",
//            xhr: function () {
//                if (jQuery.browser.msie && jQuery.browser.msie){
//                    return new ActiveXObject("Microsoft.XMLHTTP");
//                } else {
//                    return new XMLHttpRequest();
//                }
//            },
//            data : {
//                term : request.term,
//                search_type : searchTypeSelector('F'),
//							bookingEngine: "Y",
//					callBack: "callbackCities"                
//		//search_country: getDestinationCountry()
//            },
//            success : function() {
//            	var returnData = jQuery.map(
//                		jsonDataList.list.sort(sort_by('pid', false, function(a){return a})),
//                        function(item) {
//                			var text = createLabel(item, 'airport');                                
//                            if (matcher(request.term, item.cna) || matcher(request.term, item.apcd)) {
//                                return {
//                                    label : text.replace(new RegExp(
//                                            "(?![^&;]+;)(?!<[^<>]*)("
//                                            + request.term
//                                            + ")(?![^<>]*>)(?![^&;]+;)",
//                                            "gi"),
//                                        "<strong>$1</strong>"),
//                                    value : item.cna,
//                                    searchvalue : item.cna + "," + item.apcd + "," + item.cid + "," + item.apnm + "," + item.sid + "," + item.cnid  + "," + item.sna + "," + item.cnm
//                                }
//                            }
//                        });
//                cache[request.term] = returnData;
//                response(returnData);
//
//            }
//        });
//    }
//});
//
///**
//	 * Autocomplete for Flights return location.
//	 */
//jQuery("#air_Loc1_mul_2").autocomplete({
//    minLength : 3, delay : 500,
//    source : function(request, response) {
//        if (request.term in cache) {
//            response(cache[request.term]);
//            return;
//        }
//
//        jQuery
//        .ajax( {
//            url: serUrl,
//            dataType : "script",
//            xhr: function () {
//                if (jQuery.browser.msie && jQuery.browser.msie){
//                    return new ActiveXObject("Microsoft.XMLHTTP");
//                } else {
//                    return new XMLHttpRequest();
//                }
//            },
//            data : {
//                term : request.term,
//                search_type : searchTypeSelector('F'),
//		bookingEngine: "Y",
//		callBack: "callbackCities" 
//               // search_country: getDestinationCountry()
//            },
//            success : function(data) {
//            	var returnData = jQuery.map(
//                		jsonDataList.list.sort(sort_by('pid', false, function(a){return a})),
//                        function(item) {
//                			var text = createLabel(item, 'airport');                                
//                            if (matcher(request.term, item.cna) || matcher(request.term, item.apcd)) {
//                                return {
//                                    label : text.replace(new RegExp(
//                                            "(?![^&;]+;)(?!<[^<>]*)("
//                                            + request.term
//                                            + ")(?![^<>]*>)(?![^&;]+;)",
//                                            "gi"),
//                                        "<strong>$1</strong>"),
//                                    value : item.cna,
//                                    searchvalue : item.cna + "," + item.apcd + "," + item.cid + "," + item.apnm + "," + item.sid + "," + item.cnid  + "," + item.sna + "," + item.cnm
//                                }
//                            }
//                        });
//                cache[request.term] = returnData;
//                response(returnData)
//
//            }
//        });
//    }
//});
//jQuery("#air_Loc_mul_3").autocomplete({
//    minLength : 3, delay : 500,
//    source : function(request, response) {
//        if (request.term in cache) {
//            response(cache[request.term]);
//            return;
//        }
//
//        jQuery
//        .ajax( {
//            url: serUrl,
//            dataType : "script",
//            xhr: function () {
//                if (jQuery.browser.msie && jQuery.browser.msie){
//                    return new ActiveXObject("Microsoft.XMLHTTP");
//                } else {
//                    return new XMLHttpRequest();
//                }
//            },
//            data : {
//                term : request.term,
//                search_type : searchTypeSelector('F'),
//							bookingEngine: "Y",
//					callBack: "callbackCities"                
//		//search_country: getDestinationCountry()
//            },
//            success : function() {
//            	var returnData = jQuery.map(
//                		jsonDataList.list.sort(sort_by('pid', false, function(a){return a})),
//                        function(item) {
//                			var text = createLabel(item, 'airport');                                
//                            if (matcher(request.term, item.cna) || matcher(request.term, item.apcd)) {
//                                return {
//                                    label : text.replace(new RegExp(
//                                            "(?![^&;]+;)(?!<[^<>]*)("
//                                            + request.term
//                                            + ")(?![^<>]*>)(?![^&;]+;)",
//                                            "gi"),
//                                        "<strong>$1</strong>"),
//                                    value : item.cna,
//                                    searchvalue : item.cna + "," + item.apcd + "," + item.cid + "," + item.apnm + "," + item.sid + "," + item.cnid  + "," + item.sna + "," + item.cnm
//                                }
//                            }
//                        });
//                cache[request.term] = returnData;
//                response(returnData);
//
//            }
//        });
//    }
//});
//
///**
//	 * Autocomplete for Flights return location.
//	 */
//jQuery("#air_Loc1_mul_3").autocomplete({
//    minLength : 3, delay : 500,
//    source : function(request, response) {
//        if (request.term in cache) {
//            response(cache[request.term]);
//            return;
//        }
//
//        jQuery
//        .ajax( {
//            url: serUrl,
//            dataType : "script",
//            xhr: function () {
//                if (jQuery.browser.msie && jQuery.browser.msie){
//                    return new ActiveXObject("Microsoft.XMLHTTP");
//                } else {
//                    return new XMLHttpRequest();
//                }
//            },
//            data : {
//                term : request.term,
//                search_type : searchTypeSelector('F'),
//		bookingEngine: "Y",
//		callBack: "callbackCities" 
//               // search_country: getDestinationCountry()
//            },
//            success : function(data) {
//            	var returnData = jQuery.map(
//                		jsonDataList.list.sort(sort_by('pid', false, function(a){return a})),
//                        function(item) {
//                			var text = createLabel(item, 'airport');                                
//                            if (matcher(request.term, item.cna) || matcher(request.term, item.apcd)) {
//                                return {
//                                    label : text.replace(new RegExp(
//                                            "(?![^&;]+;)(?!<[^<>]*)("
//                                            + request.term
//                                            + ")(?![^<>]*>)(?![^&;]+;)",
//                                            "gi"),
//                                        "<strong>$1</strong>"),
//                                    value : item.cna,
//                                    searchvalue : item.cna + "," + item.apcd + "," + item.cid + "," + item.apnm + "," + item.sid + "," + item.cnid  + "," + item.sna + "," + item.cnm
//                                }
//                            }
//                        });
//                cache[request.term] = returnData;
//                response(returnData)
//
//            }
//        });
//    }
//});
//jQuery("#air_Loc_mul_4").autocomplete({
//    minLength : 3, delay : 500,
//    source : function(request, response) {
//        if (request.term in cache) {
//            response(cache[request.term]);
//            return;
//        }
//
//        jQuery
//        .ajax( {
//            url: serUrl,
//            dataType : "script",
//            xhr: function () {
//                if (jQuery.browser.msie && jQuery.browser.msie){
//                    return new ActiveXObject("Microsoft.XMLHTTP");
//                } else {
//                    return new XMLHttpRequest();
//                }
//            },
//            data : {
//                term : request.term,
//                search_type : searchTypeSelector('F'),
//							bookingEngine: "Y",
//					callBack: "callbackCities"                
//		//search_country: getDestinationCountry()
//            },
//            success : function() {
//            	var returnData = jQuery.map(
//                		jsonDataList.list.sort(sort_by('pid', false, function(a){return a})),
//                        function(item) {
//                			var text = createLabel(item, 'airport');                                
//                            if (matcher(request.term, item.cna) || matcher(request.term, item.apcd)) {
//                                return {
//                                    label : text.replace(new RegExp(
//                                            "(?![^&;]+;)(?!<[^<>]*)("
//                                            + request.term
//                                            + ")(?![^<>]*>)(?![^&;]+;)",
//                                            "gi"),
//                                        "<strong>$1</strong>"),
//                                    value : item.cna,
//                                    searchvalue : item.cna + "," + item.apcd + "," + item.cid + "," + item.apnm + "," + item.sid + "," + item.cnid  + "," + item.sna + "," + item.cnm
//                                }
//                            }
//                        });
//                cache[request.term] = returnData;
//                response(returnData);
//
//            }
//        });
//    }
//});
//
///**
//	 * Autocomplete for Flights return location.
//	 */
//jQuery("#air_Loc1_mul_4").autocomplete({
//    minLength : 3, delay : 500,
//    source : function(request, response) {
//        if (request.term in cache) {
//            response(cache[request.term]);
//            return;
//        }
//
//        jQuery
//        .ajax( {
//            url: serUrl,
//            dataType : "script",
//            xhr: function () {
//                if (jQuery.browser.msie && jQuery.browser.msie){
//                    return new ActiveXObject("Microsoft.XMLHTTP");
//                } else {
//                    return new XMLHttpRequest();
//                }
//            },
//            data : {
//                term : request.term,
//                search_type : searchTypeSelector('F'),
//		bookingEngine: "Y",
//		callBack: "callbackCities" 
//               // search_country: getDestinationCountry()
//            },
//            success : function(data) {
//            	var returnData = jQuery.map(
//                		jsonDataList.list.sort(sort_by('pid', false, function(a){return a})),
//                        function(item) {
//                			var text = createLabel(item, 'airport');                                
//                            if (matcher(request.term, item.cna) || matcher(request.term, item.apcd)) {
//                                return {
//                                    label : text.replace(new RegExp(
//                                            "(?![^&;]+;)(?!<[^<>]*)("
//                                            + request.term
//                                            + ")(?![^<>]*>)(?![^&;]+;)",
//                                            "gi"),
//                                        "<strong>$1</strong>"),
//                                    value : item.cna,
//                                    searchvalue : item.cna + "," + item.apcd + "," + item.cid + "," + item.apnm + "," + item.sid + "," + item.cnid  + "," + item.sna + "," + item.cnm
//                                }
//                            }
//                        });
//                cache[request.term] = returnData;
//                response(returnData)
//
//            }
//        });
//    }
//});
/**
 * Autocomplete search again for Flights departure location.
 */
/**
 * Autocomplete for Flights + Hotels departure location.
 */
jQuery("#FH_DepFrom").autocomplete({
minLength : 3, delay : 500,
source : function(request, response) {
    if (request.term in cache) {
        response(cache[request.term]);
        return;
    }

    jQuery
    .ajax( {
        url : "../../admin/common/LocationData",
        dataType : "json",
        xhr: function () {
            if (jQuery.browser.msie && jQuery.browser.msie){
                return new ActiveXObject("Microsoft.XMLHTTP");
            } else {
                return new XMLHttpRequest();
            }
        },
        data : {
            term : request.term,
            search_type : searchTypeSelector('F'),
            search_country: getDestinationCountry()
        },
        success : function(data) {
        	var returnData = jQuery.map(
            		data.list.sort(sort_by('pid', false, function(a){return a})),
                    function(item) {
            			var text = createLabel(item, 'airport');                                
                        if (matcher(request.term, item.cna) || matcher(request.term, item.apcd)) {
                            return {
                                label : text.replace(new RegExp(
                                        "(?![^&;]+;)(?!<[^<>]*)("
                                        + request.term
                                        + ")(?![^<>]*>)(?![^&;]+;)",
                                        "gi"),
                                    "<strong>$1</strong>"),
                                value : item.cna,
                                searchvalue : item.cna + "|" + item.apcd + "|" + item.cid + "|" + item.apnm + "|" + item.sid + "|" + item.cnid  + "|" + item.sna + "|" + item.cnm
                            }
                        }
                    });
            cache[request.term] = returnData;
            response(returnData);

        }
    });
}
});

/**
 * Autocomplete for Flights + Hotels return location.
 */
jQuery("#FH_RetLoc").autocomplete({
minLength : 3, delay : 500,
source : function(request, response) {
    if (request.term in cache) {
        response(cache[request.term]);
        return;
    }

    jQuery
    .ajax( {
        url : "../../admin/common/LocationData",
        dataType : "json",
        xhr: function () {
            if (jQuery.browser.msie && jQuery.browser.msie){
                return new ActiveXObject("Microsoft.XMLHTTP");
            } else {
                return new XMLHttpRequest();
            }
        },
        data : {
            term : request.term,
            search_type : searchTypeSelector('F'),
            search_country: getDestinationCountry()
        },
        success : function(data) {
        	var returnData = jQuery.map(
            		data.list.sort(sort_by('pid', false, function(a){return a})),
                    function(item) {
            			var text = createLabel(item, 'airport');                                
                        if (matcher(request.term, item.cna) || matcher(request.term, item.apcd)) {
                            return {
                                label : text.replace(new RegExp(
                                        "(?![^&;]+;)(?!<[^<>]*)("
                                        + request.term
                                        + ")(?![^<>]*>)(?![^&;]+;)",
                                        "gi"),
                                    "<strong>$1</strong>"),
                                value : item.cna,
                                searchvalue : item.cna + "|" + item.apcd + "|" + item.cid + "|" + item.apnm + "|" + item.sid + "|" + item.cnid  + "|" + item.sna + "|" + item.cnm
                            }
                        }
                    });
            cache[request.term] = returnData;
            response(returnData)

        }
    });
}
});

/**
 * Autocomplete for Flights + Cars departure location.
 */
jQuery("#FC_DepFrom").autocomplete({
minLength : 3, delay : 500,
source : function(request, response) {
    if (request.term in cache) {
        response(cache[request.term]);
        return;
    }

    jQuery
    .ajax( {
        url : "../../admin/common/LocationData",
        dataType : "json",
        xhr: function () {
            if (jQuery.browser.msie && jQuery.browser.msie){
                return new ActiveXObject("Microsoft.XMLHTTP");
            } else {
                return new XMLHttpRequest();
            }
        },
        data : {
            term : request.term,
            search_type : searchTypeSelector('F'),

            search_country: getDestinationCountry()
        },
        success : function(data) {
        	var returnData = jQuery.map(
            		data.list.sort(sort_by('pid', false, function(a){return a})),
                    function(item) {
            			var text = createLabel(item, 'airport');                                
                        if (matcher(request.term, item.cna) || matcher(request.term, item.apcd)) {
                            return {
                                label : text.replace(new RegExp(
                                        "(?![^&;]+;)(?!<[^<>]*)("
                                        + request.term
                                        + ")(?![^<>]*>)(?![^&;]+;)",
                                        "gi"),
                                    "<strong>$1</strong>"),
                                value : item.cna,
                                searchvalue : item.cna + "|" + item.apcd + "|" + item.cid + "|" + item.apnm + "|" + item.sid + "|" + item.cnid  + "|" + item.sna + "|" + item.cnm
                            }
                        }
                    });
            cache[request.term] = returnData;
            response(returnData)

        }
    });
}
});

/**
 * Autocomplete for Flights + Cars return location.
 */
jQuery("#FC_RetLoc").autocomplete({
minLength : 3, delay : 500,
source : function(request, response) {
    if (request.term in cache) {
        response(cache[request.term]);
        return;
    }

    jQuery
    .ajax( {
        url : "../../admin/common/LocationData",
        dataType : "json",
        xhr: function () {
            if (jQuery.browser.msie && jQuery.browser.msie){
                return new ActiveXObject("Microsoft.XMLHTTP");
            } else {
                return new XMLHttpRequest();
            }
        },
        data : {
            term : request.term,
            search_type : searchTypeSelector('F'),
            search_country: getDestinationCountry()
        },
        success : function(data) {
        	var returnData = jQuery.map(
            		data.list.sort(sort_by('pid', false, function(a){return a})),
                    function(item) {
            			var text = createLabel(item, 'airport');                                
                        if (matcher(request.term, item.cna) || matcher(request.term, item.apcd)) {
                            return {
                                label : text.replace(new RegExp(
                                        "(?![^&;]+;)(?!<[^<>]*)("
                                        + request.term
                                        + ")(?![^<>]*>)(?![^&;]+;)",
                                        "gi"),
                                    "<strong>$1</strong>"),
                                value : item.cna,
                                searchvalue : item.cna + "|" + item.apcd + "|" + item.cid + "|" + item.apnm + "|" + item.sid + "|" + item.cnid  + "|" + item.sna + "|" + item.cnm
                            }
                        }
                    });
            cache[request.term] = returnData;
            response(returnData)

        }
    });
}
});

/**
 * Autocomplete for Flights + Hotels + Cars departure location.
 */
jQuery("#FHC_DepFrom").autocomplete({
minLength : 3, delay : 500,
source : function(request, response) {
    if (request.term in cache) {
        response(cache[request.term]);
        return;
    }

    jQuery
    .ajax( {
        url : "../../admin/common/LocationData",
        dataType : "json",
        xhr: function () {
            if (jQuery.browser.msie && jQuery.browser.msie){
                return new ActiveXObject("Microsoft.XMLHTTP");
            } else {
                return new XMLHttpRequest();
            }
        },
        data : {
            term : request.term,
            search_type : searchTypeSelector('F'),
            search_country: getDestinationCountry()
        },
        success : function(data) {
        	var returnData = jQuery.map(
            		data.list.sort(sort_by('pid', false, function(a){return a})),
                    function(item) {
            			var text = createLabel(item, 'airport');                                
                        if (matcher(request.term, item.cna) || matcher(request.term, item.apcd)) {
                            return {
                                label : text.replace(new RegExp(
                                        "(?![^&;]+;)(?!<[^<>]*)("
                                        + request.term
                                        + ")(?![^<>]*>)(?![^&;]+;)",
                                        "gi"),
                                    "<strong>$1</strong>"),
                                value : item.cna,
                                searchvalue : item.cna + "|" + item.apcd + "|" + item.cid + "|" + item.apnm + "|" + item.sid + "|" + item.cnid  + "|" + item.sna + "|" + item.cnm
                            }
                        }
                    });
            cache[request.term] = returnData;
            response(returnData)

        }
    });
}
});

/**
 * Autocomplete for Flights + Cars return location.
 */
jQuery("#FHC_RetLoc").autocomplete({
minLength : 3, delay : 500,
source : function(request, response) {
    if (request.term in cache) {
        response(cache[request.term]);
        return;
    }

    jQuery
    .ajax( {
        url : "../../admin/common/LocationData",
        dataType : "json",
        xhr: function () {
            if (jQuery.browser.msie && jQuery.browser.msie){
                return new ActiveXObject("Microsoft.XMLHTTP");
            } else {
                return new XMLHttpRequest();
            }
        },
        data : {
            term : request.term,
            search_type : searchTypeSelector('F'),
            search_country: getDestinationCountry()
        },
        success : function(data) {
        	var returnData = jQuery.map(
            		data.list.sort(sort_by('pid', false, function(a){return a})),
                    function(item) {
            			var text = createLabel(item, 'airport');                                
                        if (matcher(request.term, item.cna) || matcher(request.term, item.apcd)) {
                            return {
                                label : text.replace(new RegExp(
                                        "(?![^&;]+;)(?!<[^<>]*)("
                                        + request.term
                                        + ")(?![^<>]*>)(?![^&;]+;)",
                                        "gi"),
                                    "<strong>$1</strong>"),
                                value : item.cna,
                                searchvalue : item.cna + "|" + item.apcd + "|" + item.cid + "|" + item.apnm + "|" + item.sid + "|" + item.cnid  + "|" + item.sna + "|" + item.cnm
                            }
                        }
                    });
            cache[request.term] = returnData;
            response(returnData);

        }
    });
}
});

/**
 * Autocomplete for Flights + Hotels + Activities departure location.
 */
jQuery("#FHA_DepFrom").autocomplete({
minLength : 3, delay : 500,
source : function(request, response) {
    if (request.term in cache) {
        response(cache[request.term]);
        return;
    }

    jQuery
    .ajax( {
        url : "../../admin/common/LocationData",
        dataType : "json",
        xhr: function () {
            if (jQuery.browser.msie && jQuery.browser.msie){
                return new ActiveXObject("Microsoft.XMLHTTP");
            } else {
                return new XMLHttpRequest();
            }
        },
        data : {
            term : request.term,
            search_type : searchTypeSelector('F'),
            search_country: getDestinationCountry()
        },
        success : function(data) {
        	var returnData = jQuery.map(
            		data.list.sort(sort_by('pid', false, function(a){return a})),
                    function(item) {
            			var text = createLabel(item, 'airport');                                
                        if (matcher(request.term, item.cna) || matcher(request.term, item.apcd)) {
                            return {
                                label : text.replace(new RegExp(
                                        "(?![^&;]+;)(?!<[^<>]*)("
                                        + request.term
                                        + ")(?![^<>]*>)(?![^&;]+;)",
                                        "gi"),
                                    "<strong>$1</strong>"),
                                value : item.cna,
                                searchvalue : item.cna + "|" + item.apcd + "|" + item.cid + "|" + item.apnm + "|" + item.sid + "|" + item.cnid  + "|" + item.sna + "|" + item.cnm
                            }
                        }
                    });
            cache[request.term] = returnData;
            response(returnData);

        }
    });
}
});

/**
 * Autocomplete for Flights + Hotels + Activities return location.
 */
jQuery("#FHA_RetLoc").autocomplete({
minLength : 3, delay : 500,
source : function(request, response) {
    if (request.term in cache) {
        response(cache[request.term]);
        return;
    }

    jQuery
    .ajax( {
        url : "../../admin/common/LocationData",
        dataType : "json",
        xhr: function () {
            if (jQuery.browser.msie && jQuery.browser.msie){
                return new ActiveXObject("Microsoft.XMLHTTP");
            } else {
                return new XMLHttpRequest();
            }
        },
        data : {
            term : request.term,
            search_type : searchTypeSelector('F'),
            search_country: getDestinationCountry()
        },
        success : function(data) {
        	var returnData = jQuery.map(
            		data.list.sort(sort_by('pid', false, function(a){return a})),
                    function(item) {
            			var text = createLabel(item, 'airport');                                
                        if (matcher(request.term, item.cna) || matcher(request.term, item.apcd)) {
                            return {
                                label : text.replace(new RegExp(
                                        "(?![^&;]+;)(?!<[^<>]*)("
                                        + request.term
                                        + ")(?![^<>]*>)(?![^&;]+;)",
                                        "gi"),
                                    "<strong>$1</strong>"),
                                value : item.cna,
                                searchvalue : item.cna + "|" + item.apcd + "|" + item.cid + "|" + item.apnm + "|" + item.sid + "|" + item.cnid  + "|" + item.sna + "|" + item.cnm
                            }
                        }
                    });
            cache[request.term] = returnData;
            response(returnData)

        }
    });
}
});


/**
 * Autocomplete for activities
 */
jQuery( "#cityLookup" ).autocomplete({
minLength : 3, delay : 500,
source: function(request, response) {
    if ( request.term in cache ) {
        response( cache[ request.term ] );
        return;
    }
		
    jQuery.ajax({
        url: "../../admin/common/LocationData",
        dataType: "json",
        xhr: function () {
            if (jQuery.browser.msie && jQuery.browser.msie){
                return new ActiveXObject("Microsoft.XMLHTTP");
            } else {
                return new XMLHttpRequest();
            }
        },
        data: {
            term: request.term,
            search_type: search_type,
            search_country: getDestinationCountry()
        },
        success: function(data) {
        	var returnData = jQuery.map(data.list.sort(sort_by('cna', false, function(a){return a})), function(item) {
            	var text = createLabel(item, 'city');
                if(matcher(request.term, item.cna) || matcher(request.term, item.sbna)){
                    return {
                        label: text.replace(new RegExp(
                            "(?![^&;]+;)(?!<[^<>]*)(" +
                            request.term +
                            ")(?![^<>]*>)(?![^&;]+;)", "gi"
                            ), "<strong>$1</strong>"),
                        value: item.cna,
                        searchvalue:item.cid+"|"+item.cna+"|"+item.sid+"|"+item.cnid+"|"+item.sna+"|"+item.cnm+"|"+item.sbid
                    }
                }
            });
            cache[ request.term ] = returnData;
            response(returnData)

        }
    });
}
});

/**
 * Autocomplete for hotels, cars, activities
 */
jQuery( "#autocomplete_HCA" ).autocomplete({
minLength : 3, delay : 500,
source: function(request, response) {
    if ( request.term in cache ) {
        response( cache[ request.term ] );
        return;
    }
			
    jQuery.ajax({
        url: "../../admin/common/LocationData",
        dataType: "json",
        xhr: function () {
            if (jQuery.browser.msie && jQuery.browser.msie){
                return new ActiveXObject("Microsoft.XMLHTTP");
            } else {
                return new XMLHttpRequest();
            }
        },
        data: {
            term: request.term,
            search_type: searchTypeSelector('H'),
            search_country: getDestinationCountry()
        },
        success: function(data) {
        	var returnData = jQuery.map(data.list.sort(sort_by('cna', false, function(a){return a})), function(item) {
                var text = createLabel(item, 'city');
                if(matcher(request.term, item.cna) || matcher(request.term, item.sbna)){
                    return {
                        label: text.replace(new RegExp(
                            "(?![^&;]+;)(?!<[^<>]*)(" +
                            request.term +
                            ")(?![^<>]*>)(?![^&;]+;)", "gi"
                            ), "<strong>$1</strong>"),
                        value: item.cna,
                        searchvalue:item.cid+"|"+item.cna+"|"+item.sid+"|"+item.cnid+"|"+item.sna+"|"+item.cnm+"|"+item.sbid
                    }
                }
            });
            cache[ request.term ] = returnData;
            response(returnData)

        }
    });
}
});

/**
 * Autocomplete for Car departure location.
 */
jQuery("#car_Loc").autocomplete({
minLength : 3, delay : 500,
source : function(request, response) {
    if (request.term in cache) {
        response(cache[request.term]);
        return;
    }

    jQuery
    .ajax( {
       	url: serUrl,
        dataType : "script",
        xhr: function () {
            if (jQuery.browser.msie && jQuery.browser.msie){
                return new ActiveXObject("Microsoft.XMLHTTP");
            } else {
                return new XMLHttpRequest();
            }
        },
        data : {
            term : request.term,
            search_type : searchTypeSelector('C'),
		bookingEngine: "Y",
					callBack: "callbackCities" 
          //  search_country: getDestinationCountry()
        },
        success : function(data) {
        	var returnData = jQuery.map(
            		jsonDataList.list.sort(sort_by('cna', false, function(a){return a})),
                    function(item) {
                        var text = createLabel(item, 'car');                                
                        if (matcher(request.term, item.cna) || matcher(request.term, item.apcd)) {
                            return {
                                label : text.replace(new RegExp(
                                        "(?![^&;]+;)(?!<[^<>]*)("
                                        + request.term
                                        + ")(?![^<>]*>)(?![^&;]+;)",
                                        "gi"),
                                    "<strong>$1</strong>"),
                                value : item.cna,
                                searchvalue : item.cid + "," + item.cna + "," + item.sid + "," + item.cnid  + "," + item.sna + "," + item.cnm  
                            }
                        }
                    });
            cache[request.term] = returnData;
//            console.log("success");
            response(returnData)

        }
    });
}
});

/**
 * Autocomplete for Car return location.
 */
jQuery("#car_Loc1").autocomplete({
minLength : 3, delay : 500,
source : function(request, response) {
    if (request.term in cache) {
        response(cache[request.term]);
        return;
    }

    jQuery
    .ajax( {
       	url: serUrl,
        dataType : "script",
        xhr: function () {
            if (jQuery.browser.msie && jQuery.browser.msie){
                return new ActiveXObject("Microsoft.XMLHTTP");
            } else {
                return new XMLHttpRequest();
            }
        },
        data : {
            term : request.term,
            search_type : searchTypeSelector('C'),
		bookingEngine: "Y",
					callBack: "callbackCities" 
          //  search_country: getDestinationCountry()
        },
        success : function(data) {
        	var returnData = jQuery.map(
            		jsonDataList.list.sort(sort_by('cna', false, function(a){return a})),
                    function(item) {
                        var text = createLabel(item, 'car');                                
                        if (matcher(request.term, item.cna) || matcher(request.term, item.apcd)) {
                            return {
                                label : text.replace(new RegExp(
                                        "(?![^&;]+;)(?!<[^<>]*)("
                                        + request.term
                                        + ")(?![^<>]*>)(?![^&;]+;)",
                                        "gi"),
                                    "<strong>$1</strong>"),
                                value : item.cna,
                                searchvalue : item.cid + "," + item.cna + "," + item.sid + "," + item.cnid+ "," + item.sna + "," + item.cnm  
                            }
                        }
                    });
            cache[request.term] = returnData;
//            console.log("success");
            response(returnData)

        }
    });
}
});

/**
 * Autocomplete for hotels, cars, activities
 */
jQuery( "#autocomplete_HC" ).autocomplete({
minLength : 3, delay : 500,
source: function(request, response) {
    if ( request.term in cache ) {
        response( cache[ request.term ] );
        return;
    }
			
    jQuery.ajax({
        url: "../../admin/common/LocationData",
        dataType: "json",
        xhr: function () {
            if (jQuery.browser.msie && jQuery.browser.msie){
                return new ActiveXObject("Microsoft.XMLHTTP");
            } else {
                return new XMLHttpRequest();
            }
        },
        data: {
            term: request.term,
            search_type: searchTypeSelector('HC'),
            search_country: getDestinationCountry()
        },
        success: function(data) {
        	var returnData = jQuery.map(data.list.sort(sort_by('cna', false, function(a){return a})), function(item) {
                var text = createLabel(item, 'city');
                if(matcher(request.term, item.cna) || matcher(request.term, item.sbna)){
                    return {
                        label: text.replace(new RegExp(
                            "(?![^&;]+;)(?!<[^<>]*)(" +
                            request.term +
                            ")(?![^<>]*>)(?![^&;]+;)", "gi"
                            ), "<strong>$1</strong>"),
                        value: item.cna,
                        searchvalue:item.cid+"|"+item.cna+"|"+item.sid+"|"+item.cnid+"|"+item.sna+"|"+item.cnm+"|"+item.sbid
                    }
                }
            });
            cache[ request.term ] = returnData;
            response(returnData);

        }
    });
}
});

/**
 * Autocomplete for Car departure location.
 */
jQuery("#CA_PickUpLoc").autocomplete({
minLength : 3, delay : 500,
source : function(request, response) {
    if (request.term in cache) {
        response(cache[request.term]);
        return;
    }

    jQuery
    .ajax( {
        url : "../../admin/common/LocationData",
        dataType : "json",
        xhr: function () {
            if (jQuery.browser.msie && jQuery.browser.msie){
                return new ActiveXObject("Microsoft.XMLHTTP");
            } else {
                return new XMLHttpRequest();
            }
        },
        data : {
            term : request.term,
            search_type : searchTypeSelector('C'),
            search_country: getDestinationCountry()
        },
        success : function(data) {
        	var returnData = jQuery.map(
            		data.list.sort(sort_by('pid', false, function(a){return a})),
                    function(item) {
                        var text = createLabel(item, 'airport');                                
                        if (matcher(request.term, item.cna) || matcher(request.term, item.apcd)) {
                            return {
                                label : text.replace(new RegExp(
                                        "(?![^&;]+;)(?!<[^<>]*)("
                                        + request.term
                                        + ")(?![^<>]*>)(?![^&;]+;)",
                                        "gi"),
                                    "<strong>$1</strong>"),
                                value : item.cna,
                                searchvalue : item.cna + "|" + item.apcd + "|" + item.cid + "|" + item.cna + "|" + item.sid + "|" + item.cnid  + "|" + item.sna + "|" + item.cnm
                            }
                        }
                    });
            cache[request.term] = returnData;
//            console.log("success");
            response(returnData)

        }
    });
}
});

/**
 * Autocomplete for Car return location.
 */
jQuery("#CA_ReturnLoc").autocomplete({
minLength : 3, delay : 500,
source : function(request, response) {
    if (request.term in cache) {
        response(cache[request.term]);
        return;
    }

    jQuery
    .ajax( {
        url : "../../admin/common/LocationData",
        dataType : "json",
        xhr: function () {
            if (jQuery.browser.msie && jQuery.browser.msie){
                return new ActiveXObject("Microsoft.XMLHTTP");
            } else {
                return new XMLHttpRequest();
            }
        },
        data : {
            term : request.term,
            search_type : searchTypeSelector('C'),
            search_country: getDestinationCountry()
        },
        success : function(data) {
        	var returnData = jQuery.map(
            		data.list.sort(sort_by('pid', false, function(a){return a})),
                    function(item) {
            			var text = createLabel(item, 'airport');                                
                        if (matcher(request.term, item.cna) || matcher(request.term, item.apcd)) {
                            return {
                                label : text.replace(new RegExp(
                                        "(?![^&;]+;)(?!<[^<>]*)("
                                        + request.term
                                        + ")(?![^<>]*>)(?![^&;]+;)",
                                        "gi"),
                                    "<strong>$1</strong>"),
                                value : item.cna,
                                searchvalue : item.cna + "|" + item.apcd + "|" + item.cid + "|" + item.cna + "|" + item.sid + "|" + item.cnid  + "|" + item.sna + "|" + item.cnm
                            }
                        }
                    });
            cache[request.term] = returnData;
            response(returnData)

        }
    });
}
});

/**
 * Autocomplete for Flights + Hotels + Activities departure location.
 */
jQuery("#FCA_DepFrom").autocomplete({
minLength : 3, delay : 500,
source : function(request, response) {
    if (request.term in cache) {
        response(cache[request.term]);
        return;
    }

    jQuery
    .ajax( {
        url : "../../admin/common/LocationData",
        dataType : "json",
        xhr: function () {
            if (jQuery.browser.msie && jQuery.browser.msie){
                return new ActiveXObject("Microsoft.XMLHTTP");
            } else {
                return new XMLHttpRequest();
            }
        },
        data : {
            term : request.term,
            search_type : searchTypeSelector('F'),
            search_country: getDestinationCountry()
        },
        success : function(data) {
        	var returnData = jQuery.map(
            		data.list.sort(sort_by('pid', false, function(a){return a})),
                    function(item) {
            			var text = createLabel(item, 'airport');                                
                        if (matcher(request.term, item.cna) || matcher(request.term, item.apcd)) {
                            return {
                                label : text.replace(new RegExp(
                                        "(?![^&;]+;)(?!<[^<>]*)("
                                        + request.term
                                        + ")(?![^<>]*>)(?![^&;]+;)",
                                        "gi"),
                                    "<strong>$1</strong>"),
                                value : item.cna,
                                searchvalue : item.cna + "|" + item.apcd + "|" + item.cid + "|" + item.apnm + "|" + item.sid + "|" + item.cnid  + "|" + item.sna + "|" + item.cnm
                            }
                        }
                    });
            cache[request.term] = returnData;
            response(returnData);

        }
    });
}
});

/**
 * Autocomplete for Flights + Hotels + Activities return location.
 */
jQuery("#FCA_RetLoc").autocomplete({
minLength : 3, delay : 500,
source : function(request, response) {
    if (request.term in cache) {
        response(cache[request.term]);
        return;
    }

    jQuery
    .ajax( {
        url : "../../admin/common/LocationData",
        dataType : "json",
        xhr: function () {
            if (jQuery.browser.msie && jQuery.browser.msie){
                return new ActiveXObject("Microsoft.XMLHTTP");
            } else {
                return new XMLHttpRequest();
            }
        },
        data : {
            term : request.term,
            search_type : searchTypeSelector('F'),
            search_country: getDestinationCountry()
        },
        success : function(data) {
        	var returnData = jQuery.map(
            		data.list.sort(sort_by('pid', false, function(a){return a})),
                    function(item) {
            			var text = createLabel(item, 'airport');                                
                        if (matcher(request.term, item.cna) || matcher(request.term, item.apcd)) {
                            return {
                                label : text.replace(new RegExp(
                                        "(?![^&;]+;)(?!<[^<>]*)("
                                        + request.term
                                        + ")(?![^<>]*>)(?![^&;]+;)",
                                        "gi"),
                                    "<strong>$1</strong>"),
                                value : item.cna,
                                searchvalue : item.cna + "|" + item.apcd + "|" + item.cid + "|" + item.apnm + "|" + item.sid + "|" + item.cnid  + "|" + item.sna + "|" + item.cnm
                            }
                        }
                    });
            cache[request.term] = returnData;
            response(returnData);

        }
    });
}
});

/**
 * Autocomplete for Flights + Hotels + Activities departure location.
 */
jQuery("#FHCA_DepFrom").autocomplete({
minLength : 3, delay : 500,
source : function(request, response) {
    if (request.term in cache) {
        response(cache[request.term]);
        return;
    }

    jQuery
    .ajax( {
        url : "../../admin/common/LocationData",
        dataType : "json",
        xhr: function () {
            if (jQuery.browser.msie && jQuery.browser.msie){
                return new ActiveXObject("Microsoft.XMLHTTP");
            } else {
                return new XMLHttpRequest();
            }
        },
        data : {
            term : request.term,
            search_type : searchTypeSelector('F'),
            search_country: getDestinationCountry()
        },
        success : function(data) {
        	var returnData = jQuery.map(
            		data.list.sort(sort_by('pid', false, function(a){return a})),
                    function(item) {
            			var text = createLabel(item, 'airport');                                
                        if (matcher(request.term, item.cna) || matcher(request.term, item.apcd)) {
                            return {
                                label : text.replace(new RegExp(
                                        "(?![^&;]+;)(?!<[^<>]*)("
                                        + request.term
                                        + ")(?![^<>]*>)(?![^&;]+;)",
                                        "gi"),
                                    "<strong>$1</strong>"),
                                value : item.cna,
                                searchvalue : item.cna + "|" + item.apcd + "|" + item.cid + "|" + item.apnm + "|" + item.sid + "|" + item.cnid  + "|" + item.sna + "|" + item.cnm
                            }
                        }
                    })
            cache[request.term] = returnData;
            response(returnData)

        }
    });
}
});

/**
 * Autocomplete for Flights + Hotels + Activities return location.
 */
jQuery("#FHCA_RetLoc").autocomplete({
minLength : 3, delay : 500,
source : function(request, response) {
    if (request.term in cache) {
        response(cache[request.term]);
        return;
    }

    jQuery
    .ajax( {
        url : "../../admin/common/LocationData",
        dataType : "json",
        xhr: function () {
            if (jQuery.browser.msie && jQuery.browser.msie){
                return new ActiveXObject("Microsoft.XMLHTTP");
            } else {
                return new XMLHttpRequest();
            }
        },
        data : {
            term : request.term,
            search_type : searchTypeSelector('F'),
            search_country: getDestinationCountry()
        },
        success : function(data) {
        	var returnData = jQuery.map(
            		data.list.sort(sort_by('pid', false, function(a){return a})),
                    function(item) {
            			var text = createLabel(item, 'airport');                                
                        if (matcher(request.term, item.cna) || matcher(request.term, item.apcd)) {
                            return {
                                label : text.replace(new RegExp(
                                        "(?![^&;]+;)(?!<[^<>]*)("
                                        + request.term
                                        + ")(?![^<>]*>)(?![^&;]+;)",
                                        "gi"),
                                    "<strong>$1</strong>"),
                                value : item.cna,
                                searchvalue : item.cna + "|" + item.apcd + "|" + item.cid + "|" + item.apnm + "|" + item.sid + "|" + item.cnid  + "|" + item.sna + "|" + item.cnm
                            }
                        }
                    });
            cache[request.term] = returnData;
            response(returnData)

        }
    });
}
});

/**
 * Autocomplete for Car departure location.
 */
jQuery("#pickupLocNameNew").autocomplete({
minLength : 3, delay : 500,
source : function(request, response) {
    if (request.term in cache) {
        response(cache[request.term]);
        return;
    }

    jQuery
    .ajax( {
        url : "../../admin/common/LocationData",
        dataType : "json",
        xhr: function () {
            if (jQuery.browser.msie && jQuery.browser.msie){
                return new ActiveXObject("Microsoft.XMLHTTP");
            } else {
                return new XMLHttpRequest();
            }
        },
        data : {
            term : request.term,
            search_type : searchTypeSelector('C'),
            search_country: getDestinationCountry()
        },
        success : function(data) {
        	var returnData = jQuery.map(
            		data.list.sort(sort_by('pid', false, function(a){return a})),
                    function(item) {
                        var text = createLabel(item, 'airport');                                
                        if (matcher(request.term, item.cna) || matcher(request.term, item.apcd)) {
                            return {
                                label : text.replace(new RegExp(
                                        "(?![^&;]+;)(?!<[^<>]*)("
                                        + request.term
                                        + ")(?![^<>]*>)(?![^&;]+;)",
                                        "gi"),
                                    "<strong>$1</strong>"),
                                value : item.cna,
                                searchvalue : item.cna + "|" + item.apcd + "|" + item.cid + "|" + item.cna + "|" + item.sid + "|" + item.cnid  + "|" + item.sna + "|" + item.cnm
                            }
                        }
                    });
            cache[request.term] = returnData;
//            console.log("success");
            response(returnData);

        }
    });
}
});

/**
 * Autocomplete for Car return location.
 */
jQuery("#retupLocNameNew").autocomplete({
minLength : 3, delay : 500,
source : function(request, response) {
    if (request.term in cache) {
        response(cache[request.term]);
        return;
    }

    jQuery
    .ajax( {
        url : "../../admin/common/LocationData",
        dataType : "json",
        xhr: function () {
            if (jQuery.browser.msie && jQuery.browser.msie){
                return new ActiveXObject("Microsoft.XMLHTTP");
            } else {
                return new XMLHttpRequest();
            }
        },
        data : {
            term : request.term,
            search_type : searchTypeSelector('C'),
            search_country: getDestinationCountry()
        },
        success : function(data) {
        	var returnData = jQuery.map(
            		data.list.sort(sort_by('pid', false, function(a){return a})),
                    function(item) {
            			var text = createLabel(item, 'airport');                                
                        if (matcher(request.term, item.cna) || matcher(request.term, item.apcd)) {
                            return {
                                label : text.replace(new RegExp(
                                        "(?![^&;]+;)(?!<[^<>]*)("
                                        + request.term
                                        + ")(?![^<>]*>)(?![^&;]+;)",
                                        "gi"),
                                    "<strong>$1</strong>"),
                                value : item.cna,
                                searchvalue : item.cna + "|" + item.apcd + "|" + item.cid + "|" + item.cna + "|" + item.sid + "|" + item.cnid  + "|" + item.sna + "|" + item.cnm
                            }
                        }
                    });
            cache[request.term] = returnData;
            response(returnData)

        }
    });
}
});
		
    jQuery("#H_Loc").bind( "autocompleteselect", function(event, ui) {
        jQuery("#hid_H_Loc").val(ui.item.searchvalue);
        //selectDestinationCountry(ui.item.selectedcountry);
    });
		
    jQuery("#autocomplete_A").bind( "autocompleteselect", function(event, ui) {
        jQuery("#A_HotLocHid").val(ui.item.searchvalue);
        selectDestinationCountry(ui.item.selectedcountry);
    });
		
    jQuery("#autocomplete_HA").bind( "autocompleteselect", function(event, ui) {
        jQuery("#HA_HotLocHid").val(ui.item.searchvalue);
        selectDestinationCountry(ui.item.selectedcountry);
    });
		
    jQuery("#autocomplete").bind( "autocompleteselect", function(event, ui) {
        jQuery("#cityLookupHid").val(ui.item.searchvalue);
    });
		
    jQuery("#autocomplete_H").bind( "autocompleteselect", function(event, ui) {
        jQuery("#cityLookupHid").val(ui.item.searchvalue);
    });
		
    jQuery("#activity_Loc").bind( "autocompleteselect", function(event, ui) {
        jQuery("#hid_A_Loc").val(ui.item.searchvalue);
    });
		
    jQuery("#autocomplete_H").bind( "autocompleteselect", function(event, ui) {
        jQuery("#autocomplete_hidden").val(ui.item.searchvalue);
    });
    
    jQuery("#V_DepFrom").bind( "autocompleteselect", function(event, ui) {
        jQuery("#V_DepFromHid").val(ui.item.searchvalue);
    });
    
    jQuery("#V_RetLoc").bind( "autocompleteselect", function(event, ui) {
        jQuery("#V_RetLocHid").val(ui.item.searchvalue);
    });
    
    jQuery("#air_Loc_mul_0").bind( "autocompleteselect", function(event, ui) {
        jQuery("#hid_air_Loc_mul_0").val(ui.item.searchvalue);
    });
    
    jQuery("#air_Loc1_mul_0").bind( "autocompleteselect", function(event, ui) {
        jQuery("#hid_air_Loc1_mul_0").val(ui.item.searchvalue);
    });
	
	
	 jQuery("#air_Loc_mul_1").bind( "autocompleteselect", function(event, ui) {
        jQuery("#hid_air_Loc_mul_1").val(ui.item.searchvalue);
    });
    
    jQuery("#air_Loc1_mul_1").bind( "autocompleteselect", function(event, ui) {
        jQuery("#hid_air_Loc1_mul_1").val(ui.item.searchvalue);
    });
	
	jQuery("#air_Loc_mul_2").bind( "autocompleteselect", function(event, ui) {
    jQuery("#hid_air_Loc_mul_2").val(ui.item.searchvalue);
    });
    
    jQuery("#air_Loc1_mul_2").bind( "autocompleteselect", function(event, ui) {
        jQuery("#hid_air_Loc1_mul_2").val(ui.item.searchvalue);
    });
	
    jQuery("#air_Loc_mul_3").bind( "autocompleteselect", function(event, ui) {
    jQuery("#hid_air_Loc_mul_3").val(ui.item.searchvalue);
    });
    
    jQuery("#air_Loc1_mul_3").bind( "autocompleteselect", function(event, ui) {
        jQuery("#hid_air_Loc1_mul_3").val(ui.item.searchvalue);
    });
	
	   jQuery("#air_Loc_mul_4").bind( "autocompleteselect", function(event, ui) {
    jQuery("#hid_air_Loc_mul_4").val(ui.item.searchvalue);
    });
    
    jQuery("#air_Loc1_mul_4").bind( "autocompleteselect", function(event, ui) {
        jQuery("#hid_air_Loc1_mul_4").val(ui.item.searchvalue);
    })
	
    jQuery("#depLocNameNew").bind( "autocompleteselect", function(event, ui) {
        jQuery("#depLocNameNewHid").val(ui.item.searchvalue);
    });
    
    jQuery("#arrLocNameNew").bind( "autocompleteselect", function(event, ui) {
        jQuery("#arrLocNameNewHid").val(ui.item.searchvalue);
    });
    
    jQuery("#FH_DepFrom").bind( "autocompleteselect", function(event, ui) {
        jQuery("#FH_DepFromHid").val(ui.item.searchvalue);
    });
    
    jQuery("#FH_RetLoc").bind( "autocompleteselect", function(event, ui) {
        jQuery("#FH_RetLocHid").val(ui.item.searchvalue);
    });
    
    jQuery("#car_Loc").bind( "autocompleteselect", function(event, ui) {
        jQuery("#hid_car_Loc").val(ui.item.searchvalue);
    });
    
    jQuery("#car_Loc1").bind( "autocompleteselect", function(event, ui) {
        jQuery("#hid_car_Loc1").val(ui.item.searchvalue);
    });
    
    jQuery("#FHC_DepFrom").bind( "autocompleteselect", function(event, ui) {
        jQuery("#FHC_DepFromHid").val(ui.item.searchvalue);
    });
    
    jQuery("#FHC_RetLoc").bind( "autocompleteselect", function(event, ui) {
        jQuery("#FHC_RetLocHid").val(ui.item.searchvalue);
    });
    
    jQuery("#FHA_DepFrom").bind( "autocompleteselect", function(event, ui) {
        jQuery("#FHA_DepFromHid").val(ui.item.searchvalue);
    });
    
    jQuery("#FHA_RetLoc").bind( "autocompleteselect", function(event, ui) {
        jQuery("#FHA_RetLocHid").val(ui.item.searchvalue);
    });
    
    jQuery("#cityLookup").bind( "autocompleteselect", function(event, ui) {
        jQuery("#cityLookupHid").val(ui.item.searchvalue);
    });
    
    jQuery("#autocomplete_HCA").bind( "autocompleteselect", function(event, ui) {
        jQuery("#HCA_HotLocHid").val(ui.item.searchvalue);
    });
    jQuery("#C_PickUpLoc").bind( "autocompleteselect", function(event, ui) {
    	jQuery("#C_PickUpLocHid").val(ui.item.searchvalue);
    });
    
    jQuery("#C_ReturnLoc").bind( "autocompleteselect", function(event, ui) {
        jQuery("#C_ReturnLocHid").val(ui.item.searchvalue);
    });
    jQuery("#CA_PickUpLoc").bind( "autocompleteselect", function(event, ui) {
        jQuery("#CA_PickUpLocHid").val(ui.item.searchvalue);
    });
    
    jQuery("#CA_ReturnLoc").bind( "autocompleteselect", function(event, ui) {
        jQuery("#CA_ReturnLocHid").val(ui.item.searchvalue);
    });
    jQuery("#FCA_DepFrom").bind( "autocompleteselect", function(event, ui) {
        jQuery("#FCA_DepFromHid").val(ui.item.searchvalue);
    });
    jQuery("#FCA_RetLoc").bind( "autocompleteselect", function(event, ui) {
        jQuery("#FCA_RetLocHid").val(ui.item.searchvalue);
    });
    jQuery("#FHCA_DepFrom").bind( "autocompleteselect", function(event, ui) {
        jQuery("#FHCA_DepFromHid").val(ui.item.searchvalue);
    });
    
    jQuery("#FHCA_RetLoc").bind( "autocompleteselect", function(event, ui) {
        jQuery("#FHCA_RetLocHid").val(ui.item.searchvalue);
    });
    jQuery("#pickupLocNameNew").bind( "autocompleteselect", function(event, ui) {
        jQuery("#pickupLocNameNewHid").val(ui.item.searchvalue);
    });
    
    jQuery("#retupLocNameNew").bind( "autocompleteselect", function(event, ui) {
        jQuery("#retupLocNameNewHid").val(ui.item.searchvalue);
    });
		
});


function selectDestinationCountry(country){
	try{
	for(i=0; i<document.forms[0].destinationcountry.options.length-1; i++){
		if(document.forms[0].destinationcountry.options[i].text==country){
			document.getElementById("destinationcountry").selectedIndex = i;
		}

	}
	}catch(e){
	}
}




function callbackCities(jsonString) {
	//console.log(jsonString);
	jsonDataList = jsonString;
}
