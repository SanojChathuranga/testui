var schemaName="flightsite";
var subURL="http://bookings.flightsite.co.za/"+schemaName+"";

var airTxtArr = new Array();
//var _month = new Array("01","02","03","04","05","06","07","08","09","10","11","12");
var _month = new Array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");


// hotel
function loadDataNew_H(){
	var searchStr = document.forms["form1"].hid_H_Loc.value.split("|");

	document.forms["ResPkgSearchForm"].searchCityId.value		= searchStr[0];
	document.forms["ResPkgSearchForm"].searchCityName.value	= searchStr[1];
	document.forms["ResPkgSearchForm"].searchStateId.value	= searchStr[2];
	document.forms["ResPkgSearchForm"].searchCountryId.value	= searchStr[3];
	
	document.forms["ResPkgSearchForm"].cmbCty_Hotels.value	= document.forms["ResPkgSearchForm"].searchCityId.value;
	document.forms["ResPkgSearchForm"].cityLookup_Hotels.value	= document.forms["ResPkgSearchForm"].searchCityName.value;
	document.forms["ResPkgSearchForm"].cmbSte_Hotels.value	= document.forms["ResPkgSearchForm"].searchStateId.value;
	document.forms["ResPkgSearchForm"].cmbCtry_Hotels.value	= document.forms["ResPkgSearchForm"].searchCountryId.value;
	
	document.forms["ResPkgSearchForm"].cmbInYr_Hotels.value	= Number(document.forms["form1"].elements["H_InYear"].options[document.forms["form1"].elements["H_InYear"].selectedIndex].value);
	document.forms["ResPkgSearchForm"].cmbInMn_Hotels.value	= Number(document.forms["form1"].elements["H_InMonth"].options[document.forms["form1"].elements["H_InMonth"].selectedIndex].value)+1;
	document.forms["ResPkgSearchForm"].cmbInDt_Hotels.value	= document.forms["form1"].elements["H_InDate"].options[document.forms["form1"].elements["H_InDate"].selectedIndex].value;	
	document.forms["ResPkgSearchForm"].cmbNgt_Hotels.value	= Number(document.forms["form1"].elements["H_nights"].options[document.forms["form1"].elements["H_nights"].selectedIndex].value);
	
	document.forms["ResPkgSearchForm"].cmbOutYr_Hotels.value	= Number(document.forms["form1"].elements["H_OutYear"].options[document.forms["form1"].elements["H_OutYear"].selectedIndex].value);
	document.forms["ResPkgSearchForm"].cmbOutMn_Hotels.value	= Number(document.forms["form1"].elements["H_OutMonth"].options[document.forms["form1"].elements["H_OutMonth"].selectedIndex].value)+1;
	document.forms["ResPkgSearchForm"].cmbOutDt_Hotels.value	= document.forms["form1"].elements["H_OutDate"].options[document.forms["form1"].elements["H_OutDate"].selectedIndex].value;
	
	document.forms["ResPkgSearchForm"].cmbNoRm_Hotels.value	= Number(document.forms["form1"].elements["H_cmbNoOfRooms"].options[document.forms["form1"].elements["H_cmbNoOfRooms"].selectedIndex].value);	
	document.forms["ResPkgSearchForm"].totRooms_Hotels.value	= Number(document.forms["form1"].elements["H_cmbNoOfRooms"].options[document.forms["form1"].elements["H_cmbNoOfRooms"].selectedIndex].value);
	
	document.forms["ResPkgSearchForm"].cmbInMn.value=Number(document.forms["form1"].elements["H_InMonth"].options[document.forms["form1"].elements["H_InMonth"].selectedIndex].value)+1;	
	document.forms["ResPkgSearchForm"].cmbInYr.value=document.forms["form1"].elements["H_InYear"].options[document.forms["form1"].elements["H_InYear"].selectedIndex].value;
	document.forms["ResPkgSearchForm"].cmbOutMn.value=Number(document.forms["form1"].elements["H_OutMonth"].options[document.forms["form1"].elements["H_OutMonth"].selectedIndex].value)+1;	
	document.forms["ResPkgSearchForm"].cmbOutYr.value=document.forms["form1"].elements["H_OutYear"].options[document.forms["form1"].elements["H_OutYear"].selectedIndex].value;	
	document.forms["ResPkgSearchForm"].cmbInDt.value=document.forms["form1"].elements["H_InDate"].options[document.forms["form1"].elements["H_InDate"].selectedIndex].value;			
	document.forms["ResPkgSearchForm"].cmbOutDt.value=document.forms["form1"].elements["H_OutDate"].options[document.forms["form1"].elements["H_OutDate"].selectedIndex].value;

var OutMonth=document.forms["ResPkgSearchForm"].cmbOutMn.value;
var InMonth=document.forms["ResPkgSearchForm"].cmbInMn.value;
var cMonth=Number(_InDate.getMonth()+1);

var d = new Date();
var curr_year = d.getFullYear();

	var totalAdults 	= 0;
	var totalChildren	= 0;
	var occupancyStr	= "";
	//document.forms["ResPkgSearchForm"].gstLst_Hotels.value = "";
	
	setFields("H");
	document.forms["ResPkgSearchForm"].action=subURL+"/packaging/reservation/ResPkgSearchCriteria.do?ActionType=ResSearchHotels";
	if (chkDates()){showError();return;}
	
	
	if(document.forms["ResPkgSearchForm"].hotelCode_Hotels.value==""){
		document.forms["ResPkgSearchForm"].shtcd_Hotels.value="0";
	}
	if(document.forms["ResPkgSearchForm"].occupancy_Details_Hotels.value==""){
		document.forms["ResPkgSearchForm"].occupancy_Details_Hotels.value = "<room1Adult>1</room1Adults><room1Children>0</room1Children>";
	}

//	document.forms["ResPkgSearchForm"].gstLst_Hotels.value = "1|0|-|%|%@";
//	document.forms["ResPkgSearchForm"].occupancy_Details_Hotels.value = "<room1Adult>1</room1Adults><room1Children>0</room1Children>";
//	document.forms["ResPkgSearchForm"].occupancy_Details_Hotels.value ="";
//	document.forms["ResPkgSearchForm"].totAdlt_Hotels.value	= "1";
//	document.forms["ResPkgSearchForm"].totChi_Hotels.value	= "0";
	document.forms["ResPkgSearchForm"].cmbtourOpt_Hotels.value="0";
	document.forms["ResPkgSearchForm"].pkgType.value	= "H";
		
	var newHotelInDate=new Date(Number(document.forms["ResPkgSearchForm"].cmbInYr_Hotels.value),Number(document.forms["ResPkgSearchForm"].cmbInMn_Hotels.value)-1,Number(document.forms["ResPkgSearchForm"].cmbInDt_Hotels.value),0,0,0,0);
	
	var newHotelOutDate=new Date(Number(document.forms["ResPkgSearchForm"].cmbOutYr_Hotels.value),Number(document.forms["ResPkgSearchForm"].cmbOutMn_Hotels.value)-1,Number(document.forms["ResPkgSearchForm"].cmbOutDt_Hotels.value),0,0,0,0);
	
	var hotelDtDiff=Number(get_deference(newHotelInDate,newHotelOutDate));
	
	if(hotelDtDiff > 0 && hotelDtDiff < 31){
		document.forms["form1"].elements["H_nights"].options.value = hotelDtDiff;
	}
if (searchStr==""){
		alert("Please Select your Location from the drop down menu");
}else if(Number(hotelDtDiff) > 31){
		alert("Number of Nights Should be Maximum of 31. Please Modify Your Search Dates and Try Again.");			
//}else if ((OutMonth<cMonth) || (InMonth<cMonth) || (document.forms["form1"].elements["air_InYear"].value<curr_year)){
}else if(Number(hotelDtDiff) < 0){
	alert("The Correct Date Should be Selected");
	} else {
		befSubmit('H');
		document.forms["ResPkgSearchForm"].submit();
		//hidePart('pageheader');
		//hidePart('pagebody');
	}
}


//activity
function loadDataNew_A(){
	

	var searchStr = document.forms["form1"].hid_H_Loc.value.split("|");
	
	document.forms["ResPkgSearchForm"].searchCityId.value	= searchStr[0];
	document.forms["ResPkgSearchForm"].searchCityName.value	= searchStr[1];
	document.forms["ResPkgSearchForm"].searchStateId.value	= searchStr[2];
	document.forms["ResPkgSearchForm"].searchCountryId.value	= searchStr[3];
	document.forms["ResPkgSearchForm"].cmbCty_Activities.value	= document.forms["ResPkgSearchForm"].searchCityId.value;
	document.forms["ResPkgSearchForm"].cityLookup_Activities.value	= document.forms["ResPkgSearchForm"].searchCityName.value;
	document.forms["ResPkgSearchForm"].cmbCtry_Activities.value	= document.forms["ResPkgSearchForm"].searchCountryId.value;
	
	document.forms["ResPkgSearchForm"].cmbInYr_Activities.value	= Number(document.forms["form1"].elements["activity_InYear"].options[document.forms["form1"].elements["activity_InYear"].selectedIndex].value);
	document.forms["ResPkgSearchForm"].cmbInMn_Activities.value	= Number(document.forms["form1"].elements["activity_InMonth"].options[document.forms["form1"].elements["activity_InMonth"].selectedIndex].value)+1;
	document.forms["ResPkgSearchForm"].cmbInDt_Activities.value	= document.forms["form1"].elements["activity_InDate"].options[document.forms["form1"].elements["activity_InDate"].selectedIndex].value;
	
	document.forms["ResPkgSearchForm"].cmbOutYr_Activities.value	= Number(document.forms["form1"].elements["activity_OutYear"].options[document.forms["form1"].elements["activity_OutYear"].selectedIndex].value);
	document.forms["ResPkgSearchForm"].cmbOutMn_Activities.value	= Number(document.forms["form1"].elements["activity_OutMonth"].options[document.forms["form1"].elements["activity_OutMonth"].selectedIndex].value)+1;
	document.forms["ResPkgSearchForm"].cmbOutDt_Activities.value	= document.forms["form1"].elements["activity_OutDate"].options[document.forms["form1"].elements["activity_OutDate"].selectedIndex].value;
	
	document.forms["ResPkgSearchForm"].cmbInMn.value=Number(document.forms["form1"].elements["activity_InMonth"].options[document.forms["form1"].elements["activity_InMonth"].selectedIndex].value)+1;
	document.forms["ResPkgSearchForm"].cmbInYr.value=document.forms["form1"].elements["activity_InYear"].options[document.forms["form1"].elements["activity_InYear"].selectedIndex].value;
	document.forms["ResPkgSearchForm"].cmbOutMn.value=Number(document.forms["form1"].elements["activity_OutMonth"].options[document.forms["form1"].elements["activity_OutMonth"].selectedIndex].value)+1;
	document.forms["ResPkgSearchForm"].cmbOutYr.value=document.forms["form1"].elements["activity_OutYear"].options[document.forms["form1"].elements["activity_OutYear"].selectedIndex].value;
	
	document.forms["ResPkgSearchForm"].cmbInDt.value=document.forms["form1"].elements["activity_InDate"].options[document.forms["form1"].elements["activity_InDate"].selectedIndex].value;
	document.forms["ResPkgSearchForm"].cmbOutDt.value=document.forms["form1"].elements["activity_OutDate"].options[document.forms["form1"].elements["activity_OutDate"].selectedIndex].value;
	
	
	setFields("A");
	document.forms["ResPkgSearchForm"].action=subURL+"/packaging/reservation/ResPkgSearchCriteria.do?ActionType=ResSearchActivities";
	if (chkDates()){showError();return;}
	
	
	document.forms["ResPkgSearchForm"].cmbNgt_Activities.value	= Number(document.forms["form1"].elements["activity_nights"].options[document.forms["form1"].elements["activity_nights"].selectedIndex].value);
	document.forms["ResPkgSearchForm"].actType_Activities.value	= document.forms["form1"].elements["Act_type"].options[document.forms["form1"].elements["Act_type"].selectedIndex].value;
	document.forms["ResPkgSearchForm"].pkgType.value	= "A";
	befSubmit('A');
	document.forms["ResPkgSearchForm"].submit();
	
}


// Preset Package

function loadDataNew_P(){
	

	var searchStr = document.forms["form1"].hid_preset_Loc.value.split("|");
	document.forms["ResPkgSearchForm"].searchCityId.value	= searchStr[0];
	document.forms["ResPkgSearchForm"].searchCityName.value	= searchStr[1];
	document.forms["ResPkgSearchForm"].searchStateId.value	= searchStr[2];
	document.forms["ResPkgSearchForm"].searchCountryId.value	= searchStr[3];
	document.forms["ResPkgSearchForm"].cmbCty_Preset.value	= document.forms["ResPkgSearchForm"].searchCityId.value;
	document.forms["ResPkgSearchForm"].cityLookup_Preset.value	= document.forms["ResPkgSearchForm"].searchCityName.value;
	document.forms["ResPkgSearchForm"].cmbCtry_Preset.value	= document.forms["ResPkgSearchForm"].searchCountryId.value;
	
	document.forms["ResPkgSearchForm"].cmbInYr_Preset.value	= Number(document.forms["form1"].elements["preset_InYear"].options[document.forms["form1"].elements["activity_InYear"].selectedIndex].value);
	document.forms["ResPkgSearchForm"].cmbInMn_Preset.value	= Number(document.forms["form1"].elements["preset_InMonth"].options[document.forms["form1"].elements["activity_InMonth"].selectedIndex].value)+1;
	document.forms["ResPkgSearchForm"].cmbInDt_Preset.value	= document.forms["form1"].elements["preset_InDate"].options[document.forms["form1"].elements["activity_InDate"].selectedIndex].value;
	
	document.forms["ResPkgSearchForm"].cmbOutYr_Preset.value	= Number(document.forms["form1"].elements["preset_OutYear"].options[document.forms["form1"].elements["activity_OutYear"].selectedIndex].value);
	document.forms["ResPkgSearchForm"].cmbOutMn_Preset.value	= Number(document.forms["form1"].elements["preset_OutMonth"].options[document.forms["form1"].elements["activity_OutMonth"].selectedIndex].value)+1;
	document.forms["ResPkgSearchForm"].cmbOutDt_Preset.value	= document.forms["form1"].elements["preset_OutDate"].options[document.forms["form1"].elements["activity_OutDate"].selectedIndex].value;
	
	setFields("P");
	document.forms["ResPkgSearchForm"].action=subURL+"/packaging/reservation/ResSearch.do?";
	if (chkDates()){showError();return;}
		
//	document.forms["ResPkgSearchForm"].cmbNgt_v.value	= Number(document.forms["form1"].elements["preset_person"].options[document.forms["form1"].elements["activity_nights"].selectedIndex].value);
	document.forms["ResPkgSearchForm"].pkgType.value	= "P";
	befSubmit('P');
	document.forms["ResPkgSearchForm"].submit();
	
}


//car
function loadDataNew_C(){

	// 0- Airport Name
	// 1- Airport Code
	// 2- City Id
	// 3- City Name
	// 4- State Id
	// 5- Country Id
	// 6- State Name
	// 7- Country Name	
	
	var depSearchStr = document.forms["form1"].hid_car_Loc.value.split("|");
		//alert('car 1 >'+document.forms["form1"].elements["hid_car_Loc"].value);
		
	document.forms["ResPkgSearchForm"].pickupLocName_Car.value 	= depSearchStr[0];
	document.forms["ResPkgSearchForm"].pickupLoc_Car.value		= depSearchStr[1];
	
//	document.forms["ResPkgSearchForm"].pickupLocName_Car.value 	= 'Las Vegas';
//	document.forms["ResPkgSearchForm"].pickupLoc_Car.value		= 'LAS';
		var retSearchStr="";
	if(document.forms["form1"].elements["_ReturnCar"].checked){
		document.forms["ResPkgSearchForm"].retupLoc_Car.value=document.forms["ResPkgSearchForm"].pickupLoc_Car.value;
		document.forms["ResPkgSearchForm"].retupLocName_Car.value=document.forms["ResPkgSearchForm"].pickupLocName_Car.value;
	}else{
		var retSearchStr = document.forms["form1"].hid_car_Loc1.value.split("|");
		if(retSearchStr==""){
			alert("-------------");
			}
		//alert('car 2 >'+document.forms["form1"].elements["hid_car_Loc1"].value);
		document.forms["ResPkgSearchForm"].retupLocName_Car.value 	= retSearchStr[0];
		document.forms["ResPkgSearchForm"].retupLoc_Car.value		= retSearchStr[1];
	}
	
	var InDate		= document.forms["form1"].elements["car_InDate"].options[document.forms["form1"].elements["car_InDate"].selectedIndex].value;
	var InMonth 	= Number(document.forms["form1"].elements["car_InMonth"].options[document.forms["form1"].elements["car_InMonth"].selectedIndex].value)+1;
	
	var InMonth1 	= _month[Number(document.forms["form1"].elements["car_InMonth"].options[document.forms["form1"].elements["car_InMonth"].selectedIndex].value)];
	
	if(InMonth<10){
		InMonth ="0"+InMonth;
	}
	var	OutDate		= document.forms["form1"].elements["car_OutDate"].options[document.forms["form1"].elements["car_OutDate"].selectedIndex].value;
	var OutMonth 	= Number(document.forms["form1"].elements["car_OutMonth"].options[document.forms["form1"].elements["car_OutMonth"].selectedIndex].value)+1;
	
	var OutMonth1 	= _month[Number(document.forms["form1"].elements["car_OutMonth"].options[document.forms["form1"].elements["car_OutMonth"].selectedIndex].value)];
	
	if(OutMonth<10){
		OutMonth ="0"+OutMonth;
	}		

var cMonth=Number(_InDate.getMonth()+1);
var d = new Date();
var curr_year = d.getFullYear();

	document.forms["ResPkgSearchForm"].pickupDate_Car.value		= Number(document.forms["form1"].elements["car_InYear"].options[document.forms["form1"].elements["car_InYear"].selectedIndex].value)+"-"+InMonth+"-"+InDate;
	
	document.forms["ResPkgSearchForm"].pickupDate_Car_Tran.value		= ""+InDate+"-"+InMonth1+"-"+Number(document.forms["form1"].elements["car_InYear"].options[document.forms["form1"].elements["car_InYear"].selectedIndex].value);
	
	document.forms["ResPkgSearchForm"].pickupTime_Car.value		= document.forms["form1"].PickTime.value;
	document.forms["ResPkgSearchForm"].returnDate_Car.value		= Number(document.forms["form1"].elements["car_OutYear"].options[document.forms["form1"].elements["car_OutYear"].selectedIndex].value)+"-"+OutMonth+"-"+OutDate;
	
	document.forms["ResPkgSearchForm"].returnDate_Car_Tran.value		= ""+OutDate+"-"+OutMonth1+"-"+Number(document.forms["form1"].elements["car_OutYear"].options[document.forms["form1"].elements["car_OutYear"].selectedIndex].value);

	setFields("C");
	document.forms["ResPkgSearchForm"].action=subURL+"/packaging/reservation/ResPkgSearchCriteria.do?ActionType=ResSearchCar";
	if (chkDates()){showError();return;}

	
	document.forms["ResPkgSearchForm"].retTime_Car.value		= document.forms["form1"].ReturnTime.value;
	document.forms["ResPkgSearchForm"].carType_Car.value		= document.forms["form1"]._CarType.value;
	document.forms["ResPkgSearchForm"].pkgType.value			= "C";
	
	var newCarInDate=new Date(Number(document.forms["form1"].elements["car_InYear"].options[document.forms["form1"].elements["car_InYear"].selectedIndex].value),Number(InMonth)-1,Number(InDate),0,0,0,0);
	
	var newCarOutDate=new Date(Number(document.forms["form1"].elements["car_OutYear"].options[document.forms["form1"].elements["car_OutYear"].selectedIndex].value),Number(OutMonth)-1,Number(OutDate),0,0,0,0);
	
	var carDtDiff=Number(get_deference(newCarInDate,newCarOutDate));	

if (depSearchStr==""){
	alert("Please Select your Location from the drop down menu");
//}else if ((OutMonth<cMonth) || (InMonth<cMonth) || (document.forms["form1"].elements["air_InYear"].value<curr_year)){
}else if (Number(carDtDiff)<0){
	alert("The Correct Date Should be Selected");
}else if ((document.forms["form1"].elements["_ReturnCar"].checked) && (depSearchStr=="")){
	alert("Please Select your Location from the drop down menu");
}else{
	befSubmit('C');
	document.forms["ResPkgSearchForm"].submit();
	//hidePart('pageheader');
	//hidePart('pagebody');
}
}

function loadDataNew_F(){
	// 0- Airport Name
	// 1- Airport Code
	// 2- City Id
	// 3- City Name
	// 4- State Id
	// 5- Country Id
	// 6- State Name
	// 7- Country Name
	var depSearchStr = document.forms["form1"].hid_air_Loc.value.split("|");
	var retSearchStr = document.forms["form1"].hid_air_Loc1.value.split("|");

		//alert('air 1 >'+document.forms["form1"].elements["hid_air_Loc"].value);
		//alert('air 2 >'+document.forms["form1"].elements["hid_air_Loc1"].value);	

	if(document.forms["form1"].elements["Air_TripType"][0].checked){
		document.forms["ResPkgSearchForm"].tripType_Air.value = "R";
	}else if(document.forms["form1"].elements["Air_TripType"][1].checked){
		document.forms["ResPkgSearchForm"].tripType_Air.value = "O";
	}
	
	var air_InDate		= document.forms["form1"].elements["air_InDate"].options[document.forms["form1"].elements["air_InDate"].selectedIndex].value;
	var air_InMonth 	= Number(document.forms["form1"].elements["air_InMonth"].options[document.forms["form1"].elements["air_InMonth"].selectedIndex].value)+1;
	
	var air_InMonth1 	= _month[Number(document.forms["form1"].elements["air_InMonth"].options[document.forms["form1"].elements["air_InMonth"].selectedIndex].value)];
	
	if(air_InMonth<10){
		air_InMonth ="0"+air_InMonth;
	}
	var	air_OutDate		= document.forms["form1"].elements["air_OutDate"].options[document.forms["form1"].elements["air_OutDate"].selectedIndex].value;
	var air_OutMonth 	= Number(document.forms["form1"].elements["air_OutMonth"].options[document.forms["form1"].elements["air_OutMonth"].selectedIndex].value)+1;

	var air_OutMonth1 	= _month[Number(document.forms["form1"].elements["air_OutMonth"].options[document.forms["form1"].elements["air_OutMonth"].selectedIndex].value)];

	if(air_OutMonth<10){
		air_OutMonth ="0"+air_OutMonth;
	}

	document.forms["ResPkgSearchForm"].depDate_Air.value	   	= Number(document.forms["form1"].elements["air_InYear"].options[document.forms["form1"].elements["air_InYear"].selectedIndex].value)+"-"+air_InMonth+"-"+air_InDate;
	
	document.forms["ResPkgSearchForm"].depDate_Air_Tran.value	   	= ""+air_InDate+"-"+air_InMonth1+"-"+Number(document.forms["form1"].elements["air_InYear"].options[document.forms["form1"].elements["air_InYear"].selectedIndex].value);
		
		
	document.forms["ResPkgSearchForm"].cmbDepTime_Air.value 	= document.forms["form1"].Air_DepTime.value;
	document.forms["ResPkgSearchForm"].depLoc_Air.value	   		= depSearchStr[1];
	document.forms["ResPkgSearchForm"].depLocName_Air.value 	= depSearchStr[0];
	document.forms["ResPkgSearchForm"].arrLoc_Air.value	   		= retSearchStr[1];
	document.forms["ResPkgSearchForm"].arrLocName_Air.value 	= retSearchStr[0];
	document.forms["ResPkgSearchForm"].arrDate_Air.value	   	= Number(document.forms["form1"].elements["air_OutYear"].options[document.forms["form1"].elements["air_OutYear"].selectedIndex].value)+"-"+air_OutMonth+"-"+air_OutDate;
		
	document.forms["ResPkgSearchForm"].arrDate_Air_Tran.value	   	= ""+air_OutDate+"-"+air_OutMonth1+"-"+Number(document.forms["form1"].elements["air_OutYear"].options[document.forms["form1"].elements["air_OutYear"].selectedIndex].value);
		
	
	document.forms["ResPkgSearchForm"].cmbArrDt_Air.value 	= air_OutDate;
	document.forms["ResPkgSearchForm"].cmbArrMn_Air.value 	= air_OutMonth;
	document.forms["ResPkgSearchForm"].cmbArrYr_Air.value 	
	
var OutMonth=air_OutMonth;
var InMonth=air_InMonth;
var cMonth=Number(_InDate.getMonth()+1);
var d = new Date();
var curr_year = d.getFullYear();

	document.forms["ResPkgSearchForm"].cmbArrTime_Air.value 	= document.forms["form1"].Air_RetTime.value;
	document.forms["ResPkgSearchForm"].cmbAdults_Air.value		= document.forms["form1"].Air_cmbNoOfAdults.value;
	document.forms["ResPkgSearchForm"].cmbSeniors_Air.value		= document.forms["form1"].Air_cmbNoOfSeniors.value;
	document.forms["ResPkgSearchForm"].cmbChildren_Air.value	= document.forms["form1"].Air_cmbNoOfChildren.value;
	document.forms["ResPkgSearchForm"].cmbInfant_Air.value		= document.forms["form1"].Air_cmbNoOfInfants.value;

	if(document.forms["form1"].elements["Air_FlightClass"][0].checked){
		document.forms["ResPkgSearchForm"].seatClass_Air.value = "Economy";
	}else if(document.forms["form1"].elements["Air_FlightClass"][1].checked){
		document.forms["ResPkgSearchForm"].seatClass_Air.value = "Business";
	}else if(document.forms["form1"].elements["Air_FlightClass"][2].checked){
		document.forms["ResPkgSearchForm"].seatClass_Air.value = "First";	
	}
	
	//if(document.forms["form1"].elements[engType+"_StopStatus"].checked){
		document.forms["ResPkgSearchForm"].nonStopStatus_Air.value = "";	
	//}	
	document.forms["ResPkgSearchForm"].pkgType.value			= "F";

	setFields("F");
	document.forms["ResPkgSearchForm"].action=subURL+"/packaging/reservation/ResPkgSearchCriteria.do?ActionType=ResSearchAir";
	if (chkDates()){showError();return;}

	var arrcityid = retSearchStr[2]; 
      document.forms[0].cmbCty_Hotels.value  = arrcityid; 
      document.forms[0].cmbDepCityCode.value = depSearchStr[2];
      document.forms[0].cmbArrCityCode.value = arrcityid;
      document.forms[0].cmbArrCtry.value = retSearchStr[5];
      
    var newInDate=new Date(Number(document.forms["form1"].elements["air_InYear"].options[document.forms["form1"].elements["air_InYear"].selectedIndex].value),Number(air_InMonth)-1,Number(air_InDate),0,0,0,0);
	
	var newOutDate=new Date(Number(document.forms["form1"].elements["air_OutYear"].options[document.forms["form1"].elements["air_OutYear"].selectedIndex].value),Number(air_OutMonth)-1,Number(air_OutDate),0,0,0,0);
	
	var dtDiff=Number(get_deference(newInDate,newOutDate));
    
if(depSearchStr==""){
	alert("Please Select your Location from the drop down menu");
//}else if ((OutMonth<cMonth) || (InMonth<cMonth) || (document.forms["form1"].elements["air_InYear"].value<curr_year)){
//}else if ( 
//(document.forms["form1"].elements["air_InYear"].value<curr_year)
//||
//(document.forms["form1"].elements["air_InYear"].value==curr_year && (OutMonth<cMonth || InMonth<cMonth))
//){
}else if(Number(dtDiff)<0){
	alert("The Correct Date Should be Selected");
}else { 
	befSubmit('F');
	document.forms["ResPkgSearchForm"].submit();
	//hidePart('pageheader');
	//hidePart('pagebody');
}
}

var checkMore_new=0;
function setCharSecond(ch,img){
var flag =0;
var tmpArr = new Array();
	for (var j=0; j<airTxtArr.length; j++){
		if (airTxtArr[j][0].toUpperCase().indexOf(ch.value.toUpperCase())==0){
			if (!showAllLoc){checkMore_new++;if (checkMore_new>10){break;}}
			tmpArr[tmpArr.length]="<tr><td class=\"stripe"+((j%2)+3)+"\" style=\"padding-left:5px;cursor:pointer;\" onMouseOver=\"altcell('ov',this)\" onMouseOut=\"altcell('ou',this,'stripe"+((j%2)+3)+"')\" onClick=\"setValSecond("+j+",'"+ch.name+"')\">"+airTxtArr[j][0]+" - "+airTxtArr[j][1]+" - "+airTxtArr[j][3]+" - "+airTxtArr[j][7]+"</td></tr>";
			flag = 1;
		}
		if ((airTxtArr[j][1].toUpperCase().indexOf(ch.value.toUpperCase())==0) && flag==0){
			if (!showAllLoc){checkMore_new++;if (checkMore_new>10){break;}}
			tmpArr[tmpArr.length]="<tr><td class=\"stripe"+((j%2)+3)+"\" style=\"padding-left:5px;cursor:pointer;\" onMouseOver=\"altcell('ov',this)\" onMouseOut=\"altcell('ou',this,'stripe"+((j%2)+3)+"')\" onClick=\"setValSecond("+j+",'"+ch.name+"')\">"+airTxtArr[j][0]+" - "+airTxtArr[j][1]+" - "+airTxtArr[j][3]+" - "+airTxtArr[j][7]+"</td></tr>";
		}
		var flag =0;
	}
	if (!showAllLoc){
		if (checkMore_new>10){tmpArr[tmpArr.length]="<tr><td class=\"morebg\" style=\"padding-left:5px;cursor:pointer;\" onClick=\"showAllLoc=true;setCharSecond(document.forms['form1'].elements['"+ch.name+"'],'"+img+"')\" title=\"Show All\" align=\"center\"><img src=\"/images/more_arrow.gif\" width=\"9\" height=\"6\" border=\"0\" hspace=\"3\" alt=\"Show All\" align=\"absmiddle\">Show All</td></tr>";}
	}
	checkMore_new=0;
	
	var outSt="";
if (tmpArr.length==0){
	outSt="<table border=\"0\" width=\"100%\" cellspacing=\"1\" cellpadding=\"3\" class=\"databorder\"><tr><td class=\"stripe4\" onClick=\"setObjPos('ixpDataFrame',0,0,180,0,'none');setObjPos('maskFrame',0,0,180,0,'none');\" align=\"center\">... No Results Available ...</td></tr></table>";
	showRes(img,outSt,1);
}else{
	outSt="<table border=\"0\" width=\"100%\" cellspacing=\"1\" cellpadding=\"4\" class=\"databorder\">"+tmpArr.join("")+"</table>";
	showRes(img,outSt,tmpArr.length);
}
tmpArr.length=0;
}

function setValSecond(index,obj){
	//document.forms["form1"].elements["hid_car_Loc1"].value=airTxtArr[index].join("|");
	document.forms["form1"].elements["hid_air_Loc1"].value=airTxtArr[index].join("|");
	document.forms["form1"].elements[obj].value=airTxtArr[index][0];
			
	setObjPos('ixpDataFrame',0,0,180,0,'none');
	setObjPos('maskFrame',0,0,180,0,'none');
	showAllLoc=false;
}

function altcell(st,obj,cls){
	if (st=="ov"){
	obj.className="overclass";
	}else{
	obj.className=cls;
	}
}

function hidePart(){
	document.getElementById("pageheader").style.display="none";
	document.getElementById("pagebody").style.display="none";	
	document.getElementById("waitMsg").style.display="block";		
	
}