<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="text/html; charset=ISO-8859-1; IE=edge" >
	<title>QA Automation Testing</title>
	
	<meta name="keywords" content="MySchool Travel - Give something back! FlightSite.com for FLIGHTS, CARS, HOTELS & HOLIDAYS">
	<meta name="description" content="MySchool Travel - Give something back! FlightSite.com for FLIGHTS, CARS, HOTELS & HOLIDAYS">
	<meta name="robots" content="index,follow">
	<meta http-equiv="PRAGMA" content="NO-CACHE">
	<meta http-equiv="CACHE-CONTROL" content="NO-CACHE">

	<!--link href="css/jquery-ui-1.8.2.custom.css" rel="stylesheet" type="text/css"/> -->
	<link rel="stylesheet" type="text/css" href="css/daterangepicker.css" />
	<link rel="stylesheet" type="text/css" href="css/location_lookup.css" />
	<link href="css/style.css" rel="stylesheet" type="text/css" />
	<link href="css/browser-specific.css" rel="stylesheet" type="text/css">
	<link href='http://fonts.googleapis.com/css?family=Oxygen:400,300,700' rel='stylesheet' type='text/css'>
	<link href="css/font-awesome.min.css" rel="stylesheet">


	<script src="js/jquery-1.9.1.js"></script>
	<script src="js/moment.min.js"></script>
	<script language="JavaScript" src="js/pkg_scripts.js" type="text/JavaScript"></script>
	<script language="JavaScript" src="js/new_pkgScript.js" type="text/JavaScript"></script>
	<!-- <script src="js/jquery-ui-1.9.2.custom.min.js"></script> -->
	<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
	<script src="http://bookings.flightsite.co.za/content/flightsightv3/jqueryAutoComplete/airport.js"></script>
	<script src="http://bookings.flightsite.co.za/content/flightsightv3/jqueryAutoComplete/citylist.js"></script>
	<script src="js/autocomplete.js"></script>
	<style>.ui-helper-hidden-accessible{display:none;}</style>
	
	<style type="text/css">
		#prop{
			background-color: #480000;
			opacity: 0.8;
			font: Verdana;
			font-size: 10;
			color: white;
			margin-left: 10px;
			margin-right: 10px;
			margin-bottom: 10px;
			margin-bottom: 10px;
		}
		#prop div {
		    display: inline-block;
		    margin-bottom: 10px;
		    margin-top: 10px;
		    margin-left: 10px;
		    width: 100%;
		}
		#prop div input {
		    margin-left: 3px;
		    display: inline-block;
		    width: 30%;
		}
		#lblprop {
			font-weight: bold;
			width: 200px;
			display: inline-block;
		} 
		#PortalCurrency {
			 overflow-y: scroll;
		}
		body{
		background-image: url("images/hotel-du-pont.jpg");
		}
		
	</style>
</head>
<body>
<script type="text/javascript">
  function closeSelf (f) {
     f.submit();
     window.close();
  }
</script>
<form action="test" method="post" onsubmit="return closeSelf(this);">
	<div id="prop">
		<div id="">
        	<label id="lblprop" for="PortalCurrency">Portal Currency</label>
        	<select id="PortalCurrency" name="PortalCurrency">
				<option value="AED">AED</option>
				<option value="AUD">AUD</option>
				<option value="CNY">CNY</option>
				<option value="COP">COP</option>
				<option value="DCS">DCS</option>
				<option value="ETB">ETB</option>
				<option value="EUR">EUR</option>
				<option value="GBP">GBP</option>
				<option value="HTG">HTG</option>
				<option value="IDR">IDR</option>
				<option value="INR">INR</option>
				<option value="JPY">JPY</option>
				<option value="KWD">KWD</option>
				<option value="LKR">LKR</option>
				<option value="MUR">MUR</option>
				<option value="MWK">MWK</option>
				<option value="PKR">PKR</option>
				<option value="PLN">PLN</option>
				<option value="QAR">QAR</option>
				<option value="QTQ">QTQ</option>
				<option value="SAR">SAR</option>
				<option value="SGD">SGD</option>
				<option value="SSS">SSS</option>
				<option value="USD">USD</option>
				<option value="XCD">XCD</option>
				<option value="ZAR">ZAR</option>
			</select>
    	</div>
    	<div>
        	<label id="lblprop" for="AXIS_CONFIG_PATH">AXIS config path</label>
        	<input type="text" id="AXIS_CONFIG_PATH" name="AXIS_CONFIG_PATH"/>
    	</div>
    	<div>
        	<label id="lblprop" for="AXIS_XML_PATH">AXIS xml path</label>
        	<input type="text" id="AXIS_XML_PATH" name="AXIS_XML_PATH"/>
    	</div>
    	<div>
        	<label id="lblprop" for="EchoToken">EchoToken</label>
        	<input type="text" id="EchoToken" name="EchoToken"/>
    	</div>
    	<div>
        	<label id="lblprop" for="MaxResponses">Max Responses</label>
        	<input type="text" id="MaxResponses" name="MaxResponses"/>
    	</div>
    	<div>
        	<label id="lblprop" for="MinResponses">Min Responses</label>
        	<input type="text" id="MinResponses" name="MinResponses"/>
    	</div>
    	<div>
        	<label id="lblprop" for="Server">Server</label>
        	<input type="text" id="Server" name="Server"/>
        	<select id="Server" name="Server">
				<option value="AED">AED</option>
				<option value="AUD">AUD</option>
				<option value="CNY">CNY</option>
				<option value="COP">COP</option>
				<option value="DCS">DCS</option>
				<option value="ETB">ETB</option>
				<option value="EUR">EUR</option>
				<option value="GBP">GBP</option>
				<option value="HTG">HTG</option>
				<option value="IDR">IDR</option>
				<option value="INR">INR</option>
				<option value="JPY">JPY</option>
				<option value="KWD">KWD</option>
				<option value="LKR">LKR</option>
				<option value="MUR">MUR</option>
				<option value="MWK">MWK</option>
				<option value="PKR">PKR</option>
				<option value="PLN">PLN</option>
				<option value="QAR">QAR</option>
				<option value="QTQ">QTQ</option>
				<option value="SAR">SAR</option>
				<option value="SGD">SGD</option>
				<option value="SSS">SSS</option>
				<option value="USD">USD</option>
				<option value="XCD">XCD</option>
				<option value="ZAR">ZAR</option>
			</select>
    	</div>
    	<div>
        	<label id="lblprop" for="Url">URL</label>
        	<input type="text" id="Url" name="Url"/>
    	</div>
    	<div>
        	<label id="lblprop" for="UserName">User Name</label>
        	<input type="text" id="UserName" name="UserName"/>
    	</div>
    	<div>
        	<label id="lblprop" for="Password">Password</label>
        	<input type="text" id="Password" name="Password"/>
    	</div>
    	<div>
        	<label id="lblprop" for="CompanyName">Company Name</label>
        	<input type="text" id="CompanyName" name="CompanyName"/>
    	</div>
    	<div>
        	<label id="lblprop" for="AgentID">Agent ID</label>
        	<input type="text" id="AgentID" name="AgentID"/>
    	</div>
    	<div>
			<input type="submit" class="search-btn" value="Save" name="SaveProp">
		</div>
	</div>
</form>
</body>
</html>