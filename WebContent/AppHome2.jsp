<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="text/html; charset=ISO-8859-1; IE=edge" >
	<title>QA Automation Testing</title>
	
	<meta name="keywords" content="MySchool Travel - Give something back! FlightSite.com for FLIGHTS, CARS, HOTELS & HOLIDAYS">
	<meta name="description" content="MySchool Travel - Give something back! FlightSite.com for FLIGHTS, CARS, HOTELS & HOLIDAYS">
	<meta name="robots" content="index,follow">
	<meta http-equiv="PRAGMA" content="NO-CACHE">
	<meta http-equiv="CACHE-CONTROL" content="NO-CACHE">

	<!--link href="css/jquery-ui-1.8.2.custom.css" rel="stylesheet" type="text/css"/> -->
	<link rel="stylesheet" type="text/css" href="../css/daterangepicker.css" />
	<link rel="stylesheet" type="text/css" href="../css/location_lookup.css" />
	<link href="../css/style.css" rel="stylesheet" type="text/css" />
	<link href="../css/browser-specific.css" rel="stylesheet" type="text/css">
	<link href='http://fonts.googleapis.com/css?family=Oxygen:400,300,700' rel='stylesheet' type='text/css'>
	<link href="../css/font-awesome.min.css" rel="stylesheet">

	<script src="../js/jquery-1.9.1.js"></script>
	<script src="../js/moment.min.js"></script>
	<script language="JavaScript" src="../js/pkg_scripts.js" type="text/JavaScript"></script>
	<script language="JavaScript" src="../js/new_pkgScript.js" type="text/JavaScript"></script>
	<!-- <script src="js/jquery-ui-1.9.2.custom.min.js"></script> -->
	<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
	<script src="http://bookings.flightsite.co.za/content/flightsightv3/jqueryAutoComplete/airport.js"></script>
	<script src="http://bookings.flightsite.co.za/content/flightsightv3/jqueryAutoComplete/citylist.js"></script>
	<script src="../js/autocomplete.js"></script>
	<style>.ui-helper-hidden-accessible{display:none;}</style>
	<!--[if (gte IE 6)&(lte IE 8)]>
		<script src="../js/selectivizr-min.js"></script>	
	<![endif]-->		

	<!-- <script>
	$(document).ready(function (){
		
		
		$('#hotelhide').show();
		alert("sfsgdfgdfgd");
		$('#Hsearchbtn').on('click', function (e) {
			
			e.preventDefault();		
			$.ajax({
				url: 'http://localhost:8081/TestUI/test/',
				dataType: 'json',
				method: 'post',
				success: function(data) {
					console.log(data);
					
					$('#textarea ').val(data.message);
					
					//hide
				}
			});
			
		});
		
	});
	
	</script> -->

	<noscript>
		<div style="width: 300px; height:75px; background-color:#FFD7D7; border: red 1px solid; border-radius: 7px; -webkit-border-radius: 7px; position:absolute; top: 50%; left: 50%; margin-left: -150px; margin-top: -50px; text-align:center; font-family: Verdana, Helvetica, sans-serif;font-size: 12px;color: #A40000;">
			<b></BR>Your Browser does not support Javascript. please refer <a href="http://www.google.com" target="_blank">this link</a> on how to enable javascript.</b>
		</div>
	</noscript>
</head>

<frameset rows="60%,40%">
  <frame src="BEC.jsp" name="bec">
  <frameset cols="60%,40%">
    <%-- <%
		StringBuffer resultJSP = new StringBuffer("Results.jsp");
		resultJSP.append("?");
		java.util.Enumeration resultEnum = request.getParameterNames();
		while (resultEnum.hasMoreElements()) {
		Object resultObj = resultEnum.nextElement();
		resultJSP.append(resultObj.toString()).append("=").append(request.getParameter(resultObj.toString())).append("&");
		}
	%> --%>
   <%-- <frame SRC="<%=org.eclipse.jst.ws.util.JspUtils.markup(resultJSP.toString())%>" NAME="result" SCROLLING="yes"> --%>
   <frame src="Results.jsp" name="process" SCROLLING="yes">
   <frame src="Booking.jsp" name="process" SCROLLING="yes">
</frameset>
</frameset>

<body>

</body>
</html>