var nav=navigator.appName;
var uag=navigator.userAgent;
var brow="";
var errorMsg="";
if (uag.indexOf("Netscape/7")>-1){
brow="NS7";
}else if (uag.indexOf("Firefox")>-1){
brow="Firefox";
}else if (uag.indexOf("Opera")>-1){
brow="Opera";
}else{
brow="IE";
}
var aobjNM="";
var aobjIO="";
aobjNM=parent.activeDateObjNM;
aobjIO=parent.activeDateObjIO;
var tmp_today=new Date();

var mm_today= new Date(tmp_today.getFullYear(),tmp_today.getMonth(),tmp_today.getDate(),0,0,0,0);
var mm_today_date  = mm_today.getDate();
var mm_today_month = mm_today.getMonth();
var mm_today_year  = mm_today.getFullYear();

dateTDW=23;

function twoChar(vl){
var rts=""+vl+"";
if (rts.length<2){return "0"+rts+"";}else{return rts;}
}

var todayNum=Number(""+mm_today_year+""+twoChar(mm_today_month+1)+""+twoChar(mm_today_date)+"");

function getClassNm(strnum){
var checkNum=todayNum;
if (aobjIO=="_Out"){
var indtNum=Number(""+(parent.document.forms["form1"].elements[""+aobjNM+"_InYear"].options[parent.document.forms["form1"].elements[""+aobjNM+"_InYear"].selectedIndex].value)+""+twoChar(Number(parent.document.forms["form1"].elements[""+aobjNM+"_InMonth"].options[parent.document.forms["form1"].elements[""+aobjNM+"_InMonth"].selectedIndex].value)+1)+""+twoChar(parent.document.forms["form1"].elements[""+aobjNM+"_InDate"].options[parent.document.forms["form1"].elements[""+aobjNM+"_InDate"].selectedIndex].value)+"");
if (indtNum>checkNum){checkNum=indtNum}
}

var retSt="";
if (strnum<=checkNum){
	retSt="tdaydis";
	}else{
	retSt="tday";
	}
return retSt;
}

function chkleap(y){
if (y%4==0){return 29;}else{return 28;}
}
var TotDays = new Array(31,28,31,30,31,30,31,31,30,31,30,31);
var MMonths = new Array("January","February","March","April","May","June","July","August","September","October","November","December");
var Days = new Array("S","M","T","W","T","F","S");

function getMM(MM){
return MMonths[MM];
}

function getTotDays(Mm,Yy){
if (Number(Mm)==1 && Yy%4==0){
return 29;}else{
return TotDays[Mm]}
}

function getStday(Mm,Yy){
tmpDay= new Date(Yy,Mm,"01",0,0,0,0);
return Number(tmpDay.getDay());
}

function ddOv(obj,st){
obj.className="tday"+st+"";
}

function setDD(Yy,Mm,DD){
for (x=0; x<parent.document.forms["form1"].elements[""+aobjNM+aobjIO+"Year"].options.length; x++){
	if (parent.document.forms["form1"].elements[""+aobjNM+aobjIO+"Year"].options[x].value==Yy){
	parent.document.forms["form1"].elements[""+aobjNM+aobjIO+"Year"].selectedIndex=x;
	}
}
parent.document.forms["form1"].elements[""+aobjNM+aobjIO+"Month"].selectedIndex=Mm-1;
parent.loadOpts(parent.maxdays(Mm-1,Yy),""+aobjNM+aobjIO+"Date",(DD-1));
if (aobjIO=="_In"){
parent.setOutDate(aobjNM,aobjIO);
}else{
parent.setNights(aobjNM);
}

parent.setcalPos();
}

function countDays(type){
var tmpinD=retDateObj(""+type+"_In");
var tmpoutD=retDateObj(""+type+"_Out");
var ind_d = tmpinD.getDate();
var ind_m = tmpinD.getMonth();
var ind_y = tmpinD.getFullYear();
var oud_d = tmpoutD.getDate();
var oud_m = tmpoutD.getMonth();
var oud_y = tmpoutD.getFullYear();
var tmpDays=0;
		if (oud_y>ind_y){
				if (oud_m==0 && ind_m==11){
						tmpDays=(parent.maxdays(ind_m,ind_y)-ind_d)+oud_d;
						errorMsg="";
				}else{
						errorMsg="You can book maximum of 31 days";
						return false;
				}
		}else if (oud_y==ind_y){
				if (oud_m>ind_m){
					tmpDays=parent.maxdays(ind_m,ind_y)-ind_d;
						for (var ixpt=ind_m+1; ixpt<=(oud_m-1); ixpt++){
								tmpDays+=parent.maxdays(ixpt,ind_y);
						}
						tmpDays+=oud_d;
						errorMsg="";
				}else if (oud_m==ind_m){
						if (oud_d>ind_d){
								tmpDays=oud_d-ind_d;
								errorMsg="";
						}else{
								errorMsg="Check in date should be grater than the check out date";
								return false;
						}
				}else if (oud_m<ind_m){
						errorMsg="Check in date should be grater than the check out date";
						return false;
				}
		}else if (oud_y<ind_y){
				errorMsg="Check in date should be grater than the check out date";
				return false;
		}
		if (tmpDays>31){
				errorMsg="You can book maximum of 31 days";
				return false;
		}else{
				errorMsg="";
				return tmpDays;
		}
}

function retDateObj(type){
return new Date(parent.document.forms["form1"].elements[""+type+"Year"].options[parent.document.forms["form1"].elements[""+type+"Year"].selectedIndex].value,parent.document.forms["form1"].elements[""+type+"Month"].options[parent.document.forms["form1"].elements[""+type+"Month"].selectedIndex].value,parent.document.forms["form1"].elements[""+type+"Date"].options[parent.document.forms["form1"].elements[""+type+"Date"].selectedIndex].value,0,0,0,0);
}

function startHide(){
setTimeout("hideCal()",1500);
}



function buildMonth(Mm,Yy){
stDay=getStday(Mm,Yy);
totDays=getTotDays(Mm,Yy);
reqTDs=totDays+stDay;
if (reqTDs>35){toLoop=42;}else{toLoop=35;}
dayCount=0;
tdCount=0;
tmpStr="<table border=\"0\" cellpadding=\"0\" cellspacing=\"1\">\n";
tmpStr+="<tr height=\"16\" align=\"center\">\n";
tmpStr+="<td colspan=\"7\" align=\"right\"><table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\">\n";
tmpStr+="<tr height=\"16\">\n";
tmpStr+="<td class=\"txt1\">"+getMM(Mm)+" - "+Yy+"</td>\n";
if (Mm==0){
tmpStr+="<td align=\"right\"><a href=\"JavaScript:parent.setcalPos();\"><img src=\"/images/cal_close.gif\" width=\"12\" height=\"11\" hspace=\"1\" border=\"0\"></a><a href=\"JavaScript:buildCal(11,"+(Number(Yy)-1)+");\"><img src=\"/images/cal_prev.gif\" width=\"12\" height=\"11\" border=\"0\"  hspace=\"2\" ></a>";
}else{
tmpStr+="<td align=\"right\"><a href=\"JavaScript:parent.setcalPos();\"><img src=\"/images/cal_close.gif\" width=\"12\" height=\"11\" hspace=\"1\" border=\"0\"></a><a href=\"JavaScript:buildCal("+(Number(Mm)-1)+","+Yy+");\"><img src=\"/images/cal_prev.gif\" width=\"12\" height=\"11\" border=\"0\"  hspace=\"2\" ></a>";
}
if (Mm==11){
tmpStr+="<a href=\"JavaScript:buildCal(0,"+(Number(Yy)+1)+");\"><img src=\"/images/cal_next.gif\" width=\"12\" height=\"11\" border=\"0\"></a></td>\n";
}else{
tmpStr+="<a href=\"JavaScript:buildCal("+(Number(Mm)+1)+","+Yy+");\"><img src=\"/images/cal_next.gif\" width=\"12\" height=\"11\" border=\"0\"></a></td>\n";
}
tmpStr+="</tr>\n";
tmpStr+="</table></td>\n";
tmpStr+="</tr>\n";
tmpStr+="<tr height=\"16\" class=\"tdays\" align=\"center\">\n";
for (c=0; c<7; c++){
tmpStr+="<td width=\""+dateTDW+"\" class=\"tdays\">"+Days[c]+"</td>\n";
}
tmpStr+="</tr>\n";
tmpStr+="<tr>\n";
tmpStr+="<td colspan=\"7\"><img src=\"/images/s.gif\" width=\"1\" height=\"3\" border=\"0\"></td>\n";
tmpStr+="</tr>\n";
for (a=0; a<(reqTDs/7); a++){
tmpStr+="<tr height=\"22\" align=\"center\">\n";
for (b=0; b<7; b++){
	if (tdCount<stDay){
	tmpStr+="<td width=\""+dateTDW+"\" class=\"tdot\">.</td>\n";
	}else{
	dayCount+=1;
	if (dayCount<=totDays){
		var thisclass=getClassNm(Number(""+Yy+""+twoChar(Number(Mm)+1)+""+twoChar(dayCount)+""));
		if (thisclass=="tdaydis"){
			tmpStr+="<td width=\""+dateTDW+"\" class=\""+thisclass+"\">"+twoChar(dayCount)+"</td>\n";
		}else{
			tmpStr+="<td width=\""+dateTDW+"\" class=\""+thisclass+"\" onClick=\"setDD("+Yy+","+(Number(Mm)+1)+",this.innerHTML,'-');\" onMouseOver=\"ddOv(this,'ov')\" onMouseOut=\"ddOv(this,'')\">"+twoChar(dayCount)+"</td>\n";	
		}
		
	}else{
	tmpStr+="<td width=\""+dateTDW+"\" class=\"tdot\">.</td>\n";}
	}
tdCount+=1;	
}
tmpStr+="</tr>\n";
}

tmpStr+="</table>\n";
return tmpStr;
}

var tmp_date="";
var chk_tmp_date="";
function getDefaults(){
if (aobjNM+aobjIO!=""){
tmp_date=""+parent.document.forms["form1"].elements[""+aobjNM+aobjIO+"Month"].options[parent.document.forms["form1"].elements[""+aobjNM+aobjIO+"Month"].selectedIndex].value+"-"+parent.document.forms["form1"].elements[""+aobjNM+aobjIO+"Date"].options[parent.document.forms["form1"].elements[""+aobjNM+aobjIO+"Date"].selectedIndex].value+"-"+parent.document.forms["form1"].elements[""+aobjNM+aobjIO+"Year"].options[parent.document.forms["form1"].elements[""+aobjNM+aobjIO+"Year"].selectedIndex].value+"";
buildCal(parent.document.forms["form1"].elements[""+aobjNM+aobjIO+"Month"].options[parent.document.forms["form1"].elements[""+aobjNM+aobjIO+"Month"].selectedIndex].value,parent.document.forms["form1"].elements[""+aobjNM+aobjIO+"Year"].options[parent.document.forms["form1"].elements[""+aobjNM+aobjIO+"Year"].selectedIndex].value);
}
setInterval("stay_alert()","200");
}


function stay_alert(){
aobjNM=parent.activeDateObjNM;
aobjIO=parent.activeDateObjIO;
	if (aobjNM+aobjIO!=""){
		chk_tmp_date=""+parent.document.forms["form1"].elements[""+aobjNM+aobjIO+"Month"].options[parent.document.forms["form1"].elements[""+aobjNM+aobjIO+"Month"].selectedIndex].value+"-"+parent.document.forms["form1"].elements[""+aobjNM+aobjIO+"Date"].options[parent.document.forms["form1"].elements[""+aobjNM+aobjIO+"Date"].selectedIndex].value+"-"+parent.document.forms["form1"].elements[""+aobjNM+aobjIO+"Year"].options[parent.document.forms["form1"].elements[""+aobjNM+aobjIO+"Year"].selectedIndex].value+"";
		if (chk_tmp_date!=tmp_date){
		tmp_date=chk_tmp_date;
		buildCal(parent.document.forms["form1"].elements[""+aobjNM+aobjIO+"Month"].options[parent.document.forms["form1"].elements[""+aobjNM+aobjIO+"Month"].selectedIndex].value,parent.document.forms["form1"].elements[""+aobjNM+aobjIO+"Year"].options[parent.document.forms["form1"].elements[""+aobjNM+aobjIO+"Year"].selectedIndex].value);
		}
	}
}

function buildCal(m,y){
document.getElementById("calWinID").innerHTML=buildMonth(m,y);
}

