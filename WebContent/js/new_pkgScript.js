//var schemaName="fsv3";
var schemaName="flightsightv3";
//var subURL="http://192.168.1.252:8080/"+schemaName+"";


//var subURL="http://stg.bookings.flightsite.co.za/"+schemaName+"";
var subURL="http://bookings.flightsite.co.za/"+schemaName+"";


var v_cutoff = 2;
var f_cutoff = 2;
var h_cutoff = 2;
var a_cutoff = 2;
var c_cutoff = 2;

var v_max_pax = 6;
var f_max_pax = 9;

var h_min_nights = 1
var h_max_nights = 31

var v_error_dep ="Departure Date cannot be after the Return date!";
var v_error_from ="Please type the first few letters of your departure point and select from the list displayed.";
var v_error_to ="Please type the first few letters of your destination and select from the list displayed.";
var v_error_same_loc ="The departure point and and destination cannot be the same!";
var v_error_adult_inf ="Only 1 infant per adult permitted.";
var v_error_max_pax ="Your enquiry has exceeded the maximum passenger count. Please contact us for group booking requests.";
var v_error_cutoff_1 ="Departure Date should be later than ";
var v_error_cutoff_2 =" days from the current time!";
var v_error_11="Your Search Date Period Should be Less Than 11 Months from the current date!"

var f_error_multides_date ="Please pick your departure date";
var f_error_cutoff_1 ="Departure Date should be later than ";
var f_error_cutoff_2 =" days from the current time!";
var f_error_dep ="Departure Date cannot be after the Return date.";
var f_error_from ="Please type the first few letters of your departure point and select from the list displayed.";
var f_error_to ="Please type the first few letters of your destination and select from the list displayed.";
var f_error_same_loc ="The departure point and and destination cannot be the same!";
var f_error_adult_inf ="Only 1 infant per adult permitted.";
var f_error_max_pax ="Your enquiry has exceeded the maximum passenger count. Please contact us for group booking requests.";
var f_error_11="Your Search Date Period Should be Less Than 11 Months from the current date!"
var f_error_depdatepass="Invalid Search Dates. Please Re Enter.";
var h_error_loc ="Please type the first few letters of your departure point then select from the list displayed.";
var h_error_cutoff_1 ="Departure Date should be later than ";
var h_error_cutoff_2 =" days from the current time!";
var h_error_max ="Number of nights can not exceed 31. Please modify your search dates and try again.";
var h_error_min ="Number of nights can not be less than 1. Please modify your search dates and try again.";
var h_error_dep ="Departure Date cannot be after the Return date!";

var a_error_res ="Please select your country of residence from the drop down!";
var a_error_loc ="Please type the first few letters of your departure point then select from the list displayed.";
var a_error_dep ="Departure Date cannot be after the Return date!";
var a_error_cutoff_1 ="Departure Date should be later than ";
var a_error_cutoff_2 =" days from the current time!";

var c_error_res ="Please select your country of residence from the drop down!";
var c_error_from ="Please type the first few letters of the pick up location and then select from the list displayed.";
var c_error_to ="Please type the first few letters of the drop off location and then select from the list displayed.";
var c_error_dep ="Pickup Date cannot be after the Return date!";
var c_error_cutoff_1 ="Pickup Date should be later than ";
var c_error_cutoff_2 =" days from the current time!";


var airTxtArr = new Array();
var _month = new Array("January","February","March","April","May","June","July","August","September","October","November","December");


$(document).ready(function() {
	
	
	moment.lang('fr', {
	    
	    longDateFormat : {
	        LT : "HH:mm",
	        L : "DD/MM/YYYY",
	        LL : "D MMMM YYYY",
	        LLL : "D MMMM YYYY LT",
	        LLLL : "dddd D MMMM YYYY LT"
	       
	    },
	
	
	 calendar : {
	        lastDay : 'L',
	        sameDay : 'L',
	        nextDay : 'L',
	        lastWeek : 'L',
	        nextWeek : 'L',
	        sameElse : 'L'
	    }
	  
	});
	
});

function dateSplitVacation(){
	
	var rangedate = document.getElementById('vac-dep-date').value;
	////alert("vacation range date ------------->"+rangedate);
	var chekingcheckoutdate = rangedate.split("-");
	var checkinDate = chekingcheckoutdate[0].trim();
	var checkoutDate = chekingcheckoutdate[1].trim();

	var datesplit = checkinDate;
	var depart = datesplit.split("/");
	var arrival = checkoutDate.split("/");
	
	vacation_InDate=depart[0];
	vacation_InMonth=depart[1];
	vacation_InYear = depart[2];

	vacation_OutDate=arrival[0];
	vacation_OutMonth=arrival[1];
	vacation_OutYear = arrival[2];
	
	var innout=[vacation_InDate,vacation_InMonth,vacation_InYear,vacation_OutDate,vacation_OutMonth,vacation_OutYear];
	return innout;
	
	
}

function loadDataNew_V(){
	setFields("F");
	document.forms["ResPkgSearchForm"].action=subURL+"/packaging/reservation/ResPkgSearchCriteria.do?ActionType=ResSearchAir";
	if (chkDates()){showError();return;}
	var depSearchStr = document.forms["form1"].V_DepFromHid.value.split(",");
	var retSearchStr = document.forms["form1"].V_RetLocHid.value.split(",");		

	document.forms["ResPkgSearchForm"].cmbNgt.value= 2;		
	document.forms["ResPkgSearchForm"].tripType_Air.value = "R";	
	
	
	
	/*DATE ******************************/
	var vactionchekingcheckout =  dateSplitVacation();
	
	/*var air_InDate		= document.forms["form1"].elements["air_InDate"].options[document.forms["form1"].elements["air_InDate"].selectedIndex].value;
	var air_InMonth 	= Number(document.forms["form1"].elements["air_InMonth"].options[document.forms["form1"].elements["air_InMonth"].selectedIndex].value)+1;*/
	
	var air_InDate		= vactionchekingcheckout[0];
	var air_InMonth 	= vactionchekingcheckout[1];
	
	var air_InYear		= vactionchekingcheckout[2];
	
//	if(air_InMonth<10){
//		air_InMonth ="0"+air_InMonth;
//	}
	
	/*var	air_OutDate		= document.forms["form1"].elements["air_OutDate"].options[document.forms["form1"].elements["air_OutDate"].selectedIndex].value;
	var air_OutMonth 	= Number(document.forms["form1"].elements["air_OutMonth"].options[document.forms["form1"].elements["air_OutMonth"].selectedIndex].value)+1;*/
	
	var	air_OutDate		= vactionchekingcheckout[3];
	var air_OutMonth 	= vactionchekingcheckout[4];
	
	var air_OutYear 	= vactionchekingcheckout[5];
	
	/*if(air_OutMonth<10){
		air_OutMonth ="0"+air_OutMonth;
	}*/
	
	/*var inDateObj=new Date(Number(document.forms["form1"].elements["air_InYear"].options[document.forms["form1"].elements["air_InYear"].selectedIndex].value),Number(air_InMonth)-1,Number(air_InDate),0,0,0,0);
	var outDateObj=new Date(Number(document.forms["form1"].elements["air_OutYear"].options[document.forms["form1"].elements["air_OutYear"].selectedIndex].value),Number(air_OutMonth)-1,Number(air_OutDate),0,0,0,0);*/
	
	var inDateObj=new Date(Number(air_InYear),Number(air_InMonth)-1,Number(air_InDate),0,0,0,0);
	var outDateObj=new Date(Number(air_OutYear),Number(air_OutMonth)-1,Number(air_OutDate),0,0,0,0);
	
	
	var dtDiff=Number(get_deference(inDateObj,outDateObj));
	/*DATE ******************************/
	
	document.forms["ResPkgSearchForm"].vacationpkg.value	   	= 'Y'
		
		/*DATE ******************************/	
	
	/*document.forms["ResPkgSearchForm"].depDate_Air.value	   	= Number(document.forms["form1"].elements["air_InYear"].options[document.forms["form1"].elements["air_InYear"].selectedIndex].value)+"-"+air_InMonth+"-"+air_InDate;	*/
	document.forms["ResPkgSearchForm"].depDate_Air.value	   	= Number(air_InYear)+"-"+air_InMonth+"-"+air_InDate;	
	
	
	document.forms["ResPkgSearchForm"].cmbDepTime_Air.value 		= document.forms["form1"].Air_DepTime.value;
	
	document.forms["ResPkgSearchForm"].depLoc_Air.value	   		= depSearchStr[1];
	document.forms["ResPkgSearchForm"].depLocName_Air.value 	= depSearchStr[0]+"&nbsp;&nbsp;("+depSearchStr[3]+"-"+depSearchStr[7]+")";		
		
	document.forms["ResPkgSearchForm"].vacationSearchDepStr.value= document.forms["form1"].V_DepFromHid.value.replace(/,/g,"|");
	document.forms["ResPkgSearchForm"].vacationSearchRetStr.value= document.forms["form1"].V_RetLocHid.value.replace(/,/g,"|");
	
	document.forms["ResPkgSearchForm"].arrLoc_Air.value	   		= retSearchStr[1];
	document.forms["ResPkgSearchForm"].arrLocName_Air.value 	= retSearchStr[0]+"&nbsp;&nbsp;("+retSearchStr[3]+"-"+retSearchStr[7]+")";
	
	/*DATE ******************************/	
	/*document.forms["ResPkgSearchForm"].arrDate_Air.value	   	= Number(document.forms["form1"].elements["air_OutYear"].options[document.forms["form1"].elements["air_OutYear"].selectedIndex].value)+"-"+air_OutMonth+"-"+air_OutDate;*/
	document.forms["ResPkgSearchForm"].arrDate_Air.value	   	= Number(air_InYear)+"-"+air_OutMonth+"-"+air_OutDate;
	document.forms["ResPkgSearchForm"].cmbArrDt_Air.value 		= air_OutDate;
	document.forms["ResPkgSearchForm"].cmbArrMn_Air.value 		= air_OutMonth;
	document.forms["ResPkgSearchForm"].cmbArrYr_Air.value 
	
	/*DATE ******************************/
	
	
	document.forms["ResPkgSearchForm"].cmbArrTime_Air.value 		= document.forms["form1"].Air_RetTime.value;
	document.forms["ResPkgSearchForm"].cmbInMn_Hotels.value 		= air_InMonth;
	document.forms["ResPkgSearchForm"].cmbInDt_Hotels.value 		= air_InDate;
	
	/*DATE ******************************/
	/*document.forms["ResPkgSearchForm"].cmbInYr_Hotels.value 		= document.forms["form1"].elements["air_InYear"].options[document.forms["form1"].elements["air_InYear"].selectedIndex].value;*/
	document.forms["ResPkgSearchForm"].cmbInYr_Hotels.value 		= air_InYear;
	
	document.forms["ResPkgSearchForm"].cmbOutDt_Hotels.value 		= air_OutDate;
	document.forms["ResPkgSearchForm"].cmbOutMn_Hotels.value 		= air_OutMonth;
	
/*	document.forms["ResPkgSearchForm"].cmbOutYr_Hotels.value 		= document.forms["form1"].elements["air_OutYear"].options[document.forms["form1"].elements["air_OutYear"].selectedIndex].value;*/
	document.forms["ResPkgSearchForm"].cmbOutYr_Hotels.value 		= air_OutYear;
	document.forms["ResPkgSearchForm"].cmbNgt_Hotels.value 			= Number(dtDiff);
	//document.forms["ResPkgSearchForm"].consumerRegionId.value 		= document.forms["form1"].V_Country.value;
	
	
	
	document.forms["ResPkgSearchForm"].cmbInMn_Activities.value 	= air_InMonth;
	document.forms["ResPkgSearchForm"].cmbInDt_Activities.value 	= air_InDate;
	//document.forms["ResPkgSearchForm"].cmbInYr_Activities.value 	= document.forms["form1"].elements["air_InYear"].options[document.forms["form1"].elements["air_InYear"].selectedIndex].value;
	document.forms["ResPkgSearchForm"].cmbInYr_Activities.value 	= air_InYear;
	
	document.forms["ResPkgSearchForm"].cmbOutDt_Activities.value 	= air_OutDate;
	document.forms["ResPkgSearchForm"].cmbOutMn_Activities.value 	= air_OutMonth;
	
	/*document.forms["ResPkgSearchForm"].cmbOutYr_Activities.value 	= document.forms["form1"].elements["air_OutYear"].options[document.forms["form1"].elements["air_OutYear"].selectedIndex].value;*/
	document.forms["ResPkgSearchForm"].cmbOutYr_Activities.value 	= air_OutYear;
	document.forms["ResPkgSearchForm"].cmbNgt_Activities.value 		= Number(dtDiff);

	
	/*DATE ******************************/
	
	var OutMonth=document.forms["ResPkgSearchForm"].cmbOutMn.value;
var InMonth=document.forms["ResPkgSearchForm"].cmbInMn.value;
var cMonth=Number(_InDate.getMonth()+1);

var d = new Date();
var curr_year = d.getFullYear();
	var adlts = 0;
	var seniors = 0;
	var children = 0;
	var infant =0;
	
	var Roomlen=Number(eval("document.forms['form1'].H_cmbNoOfRooms.value"));
	
	
	
	for (var oc=0; oc<Roomlen; oc++){	
		adlts = Number(adlts) +  Number(eval("document.forms['form1'].R"+(oc+1)+"occAdults.value"));
		children = Number(children) + Number(eval("document.forms['form1'].R"+(oc+1)+"occChi.value"));
		infant = Number(infant) + Number(eval("document.forms['form1'].R"+(oc+1)+"occInfants.value"));
		
	}
	
	document.forms["ResPkgSearchForm"].cmbNoOfAdults_Activities.value 	= Number(adlts) +  Number(eval("document.forms['form1'].R"+(oc+1)+"occAdults.value"));
	
	document.forms["ResPkgSearchForm"].cmbNoOfChildren_Activities.value = Number(children) + Number(eval("document.forms['form1'].R"+(oc+1)+"occChi.value"));
	
	var roomChildren= Number(document.forms["ResPkgSearchForm"].cmbNoOfChildren_Activities.value);
	document.forms["ResPkgSearchForm"].gstChildAges_Activities.value = "";	
	document.forms["ResPkgSearchForm"].cmbAdults_Air.value		= adlts; 	
	document.forms["ResPkgSearchForm"].cmbSeniors_Air.value		= seniors; 
	document.forms["ResPkgSearchForm"].cmbChildren_Air.value	= children; 
	document.forms["ResPkgSearchForm"].cmbInfant_Air.value		= infant; 

	document.forms["ResPkgSearchForm"].cmbNoRm_Hotels.value =    Number(document.forms['form1'].H_cmbNoOfRooms.options[document.forms['form1'].H_cmbNoOfRooms.selectedIndex].value);
	
	if(document.forms["form1"].elements["Air_FlightClass"][0].checked){
		document.forms["ResPkgSearchForm"].seatClass_Air.value = "Economy";
	}else if(document.forms["form1"].elements["Air_FlightClass"][1].checked){
		document.forms["ResPkgSearchForm"].seatClass_Air.value = "Premium";
	}else if(document.forms["form1"].elements["Air_FlightClass"][2].checked){
		document.forms["ResPkgSearchForm"].seatClass_Air.value = "Business";
	}else if(document.forms["form1"].elements["Air_FlightClass"][3].checked){	
		document.forms["ResPkgSearchForm"].seatClass_Air.value = "First";	
	}
	

		document.forms["ResPkgSearchForm"].nonStopStatus_Air.value = "";	
	var arrcityid = retSearchStr[2]; 
	document.forms["ResPkgSearchForm"].cmbCty.value  = arrcityid; 
	document.forms["ResPkgSearchForm"].cmbDepCityCode.value = depSearchStr[2];
	document.forms["ResPkgSearchForm"].cmbArrCityCode.value = arrcityid;
	document.forms["ResPkgSearchForm"].cmbArrCtry.value = retSearchStr[5];
    
	document.forms["ResPkgSearchForm"].cmbCty_Hotels.value = arrcityid;
	document.forms["ResPkgSearchForm"].cmbCtry_Hotels.value = retSearchStr[5];	
	document.forms["ResPkgSearchForm"].cityLookup_Hotels.value = retSearchStr[3];
	
	if(retSearchStr[4]=="-" || retSearchStr[4]==""){
			document.forms["ResPkgSearchForm"].cmbSte_Hotels.value = "0";
		}else{
			document.forms["ResPkgSearchForm"].cmbSte_Hotels.value = retSearchStr[4];
	}
	var selroomcount = document.forms["form1"].elements["H_cmbNoOfRooms"].value;
	document.forms["ResPkgSearchForm"].totRooms_Hotels.value	= selroomcount;
	document.forms["ResPkgSearchForm"].cmbNoRm_Hotels.value  = selroomcount;
	document.forms["ResPkgSearchForm"].accommodationType.value="HT"; 
	document.forms["ResPkgSearchForm"].cmbtourOpt_Hotels.value="0";
	document.forms["ResPkgSearchForm"].shtcd_Hotels.value="0";
	
	document.forms["ResPkgSearchForm"].cmbCty_Activities.value = arrcityid;
	document.forms["ResPkgSearchForm"].cmbCtry_Activities.value = retSearchStr[5];
		
	document.forms["ResPkgSearchForm"].tripTypeNew_Air.value = "R";
	document.forms["ResPkgSearchForm"].tripTypeNew.value = "R";	
	document.forms["ResPkgSearchForm"].tripType_Air.value = "R";
	
	var totadltchil=Number(adlts) + Number(children);	
	document.forms["ResPkgSearchForm"].pickupLocName_Car.value 	= retSearchStr[0]; 
	document.forms["ResPkgSearchForm"].pickupLoc_Car.value	  		= retSearchStr[1];
		
	document.forms["ResPkgSearchForm"].retupLocName_Car.value  	= retSearchStr[0]; 
	document.forms["ResPkgSearchForm"].retupLoc_Car.value	  		= retSearchStr[1]; 			
		
	document.forms["ResPkgSearchForm"].pickupDate_Car.value	= document.forms["ResPkgSearchForm"].depDate_Air.value;
	document.forms["ResPkgSearchForm"].pickupTime_Car.value	= document.forms["ResPkgSearchForm"].cmbDepTime_Air.value;
	document.forms["ResPkgSearchForm"].returnDate_Car.value	= document.forms["ResPkgSearchForm"].arrDate_Air.value;
	document.forms["ResPkgSearchForm"].retTime_Car.value		= document.forms["ResPkgSearchForm"].cmbArrTime_Air.value;	
	document.forms["ResPkgSearchForm"].carType_Car.value		= "ALL";
	
	var currentDate = new Date(new Date().getFullYear(),new Date().getMonth(),(new Date().getDate()+v_cutoff));
	var elevenmonths = new Date(new Date().getFullYear(),(new Date().getMonth()+11),(new Date().getDate()));
	var vac_back_date = Number(get_deference(inDateObj,currentDate));
	if(document.forms["form1"].elements["V_DepFromHid"].value=="") {
		show_error_msg(v_error_from);
		} else if(document.forms["form1"].elements["V_RetLocHid"].value=="") {
		show_error_msg(v_error_to);
		} else if(document.forms["form1"].elements["V_DepFromHid"].value==document.forms["form1"].elements["V_RetLocHid"].value) {
		show_error_msg(v_error_same_loc);
		} else if (inDateObj>outDateObj){
		show_error_msg(v_error_dep);
		}else if(inDateObj<(currentDate)){
			show_error_msg(v_error_cutoff_1+v_cutoff+v_error_cutoff_2)
		}else if(inDateObj<(currentDate)){
			show_error_msg(v_error_cutoff_1+v_cutoff+v_error_cutoff_2)
		}else if(inDateObj>=elevenmonths || outDateObj>=elevenmonths) {
			show_error_msg(v_error_11);
		}else if (Number(adlts)<Number(infant)){
			show_error_msg(v_error_adult_inf);
		}else if (totadltchil>v_max_pax){
			show_error_msg(v_error_max_pax);
		}else{
		befSubmit('V');
		document.forms["ResPkgSearchForm"].submit();
		}
}

function get_deference(ind,oud){
var milSec=parseInt(oud-ind);
var ret_val=milSec/86400000;
return ret_val;
}
/*
function chkMultDates(iny,inm,ind,preouty,preoutm,preoutd){
	var todayNum=Number(""+new Date().getFullYear()+""+twoChar(Number(new Date().getMonth())+1)+""+twoChar(new Date().getDate())+"");
	var _inDt=Number(""+iny+""+twoChar(inm)+""+twoChar(ind)+"");
	var _perDt=Number(""+preouty+""+twoChar(preoutm)+""+twoChar(preoutd)+"");
	
	if (_inDt<todayNum){
	 ErrMsg="parse search date";
		return true;
	}else if (_inDt<=_perDt){
		ErrMsg="invalid";
		return true;
	}
	ErrMsg="";
	return false;
}


	var predepD="";
 	for(var i=2; i<(num+3); i++){
		try{
			if(i==2){
				predepD=document.forms[0].elements[engType+"_DepDate"].value.split("/");
			}else{
				predepD=document.forms[0].elements[engType+"_DepDateMult"+(i-1)].value.split("/");
			}
			depD=document.forms[0].elements[engType+"_DepDateMult"+i].value.split("/");
			
			if (chkMultDates(depD[2],depD[0],depD[1],predepD[2],predepD[0],predepD[1])){
					document.getElementById("errMsgId").innerHTML=""+ErrMsg+"";
					document.getElementById("errMsgId").style.display="block";
					setTimeout("hideMsg()",5000);
			return true;
			}
		}catch(ex){
			//////alert('ex----'+ex);
		}
 	}
 	
 	return false;
 	
}*/


///  FLLIGHT FLIGHT FLIGH

function dateSpliting(i){

//	alert("dateSpliting-------> ");
	
	var rangedate = "";
	//var rangedate = document.getElementById('f-dep-date').value;
	if(i==0){
		rangedate = document.getElementById('f-des'+i+'-dep-date_single').value;
	}else{
		 rangedate = document.getElementById('f-des'+i+'-dep-date').value;	
	}
	
	
	var rangedatesplit = rangedate.split("/");
	var arrdateformed = rangedatesplit[2].trim() + "-"
			+ rangedatesplit[1].trim() + "-" + rangedatesplit[0].trim();
	var momentjsdate = moment(arrdateformed, [ "MM-DD-YYYY",
			"YYYY-MM-DD" ]);
	//alert("momentjsdate-------->" + momentjsdate);
	var newdepdarture = moment(arrdateformed,
			[ "MM-DD-YYYY", "YYYY-MM-DD" ], 'fr').add('days', 1)
			.calendar();
	//alert("dummydep----------->" + newdepdarture);
	rangedate = rangedate + "-" + newdepdarture;
	//rangedate = newdepdarture + "-" + rangedate;
	////alert(rangedate);
	var chekingcheckoutdate = rangedate.split("-");
	var checkinDate = chekingcheckoutdate[0].trim();
	var checkoutDate = chekingcheckoutdate[1].trim();

	var datesplit = checkinDate;
	var depart = datesplit.split("/");
	var arrival = checkoutDate.split("/");
	
	air_InDate=depart[0];
	air_InMonth=depart[1];
	air_InYear = depart[2];

	air_OutDate=arrival[0];
	air_OutMonth=arrival[1];
	air_OutYear = arrival[2];
	
/*	air_InDate=arrival[0];
	air_InMonth=arrival[1];
	air_InYear = arrival[2];

	air_OutDate=depart[0];
	air_OutMonth=depart[1];
	air_OutYear = depart[2];*/
	
	
	var innout=[air_InDate,air_InMonth,air_InYear,air_OutDate,air_OutMonth,air_OutYear];
	//var innout=[air_OutDate,air_OutMonth,air_OutYear,air_InDate,air_InMonth,air_InYear];
	return innout;
}

// Flight Search
function loadDataNew_F(){
setFields("F");
document.forms["ResPkgSearchForm"].action=subURL+"/packaging/reservation/ResPkgSearchCriteria.do?ActionType=ResSearchAir";
if (chkDates()){showError();return;}
		
if($('#Air_TripType3').is(':checked')) {
	
	////alert("Multidetination-------------> 02");
	
	
var depSearchStr=[];
var retSearchStr=[];
var count=2;
if(document.getElementById("multi_des_04").style.display!="none"){
count=5;	
}else if(document.getElementById("multi_des_03").style.display!="none"){
count=4;	
}else if(document.getElementById("multi_des_02").style.display!="none"){
count=3;	
}
var noofdes=count;
var i;
multidestionationDateValidation(count);
for (i=0 ; i<count; i++){
	//alert("range date");
	var rangedate = "";
	//var rangedate = document.getElementById('f-dep-date').value;
	if(i==0){
		rangedate = document.getElementById('f-des'+i+'-dep-date_single').value;
	}else{
		 rangedate = document.getElementById('f-des'+i+'-dep-date').value;	
	}
	//alert("rangedate-------->"+rangedate);
	multidestinationValidation();
	
	/*if(document.getElementById("air_Loc_mul_"+i+"") == "" || document.getElementById("air_Loc_mul_"+i+"") == null ){
		show_error_msg(f_error_from);
	}else if(rangedate == "" || rangedate ==null){
		
		show_error_msg(f_error_multides_date);
	}*/
	
	
	var rangedatesplit = rangedate.split("/");
	var arrdateformed = rangedatesplit[2].trim() + "-"
			+ rangedatesplit[1].trim() + "-" + rangedatesplit[0].trim();
	var momentjsdate = moment(arrdateformed, [ "MM-DD-YYYY",
			"YYYY-MM-DD" ]);
	//alert("momentjsdate-------->" + momentjsdate);
	var newdepdarture = moment(arrdateformed,
			[ "MM-DD-YYYY", "YYYY-MM-DD" ], 'fr').add('days', 1)
			.calendar();
	//alert("dummydep----------->" + newdepdarture);
	rangedate = rangedate + "-" + newdepdarture;
	
	var chekingcheckoutdate = rangedate.split("-");
	var departdate = chekingcheckoutdate[0].trim();
	var returndate = chekingcheckoutdate[1].trim();

	/*var datesplit = checkinDate;
	var depart = datesplit.split("/");
	var arrival = checkoutDate.split("/");*/
	
	//var datesplit = checkinDate;
	var depart = departdate.split("/");
	var arrival = returndate.split("/");
	
	
depSearchStr[i] = document.getElementById("hid_air_Loc_mul_"+i).value.split(",");
retSearchStr[i] = document.getElementById("hid_air_Loc1_mul_"+i).value.split(",");
document.forms[0].elements["m_DepLoc_Air["+(i)+"]"].value=depSearchStr[i][1];
document.forms[0].elements["m_DepLocName_Air["+(i)+"]"].value= depSearchStr[i][0];	

document.forms[0].elements["m_ArrLoc_Air["+(i)+"]"].value= retSearchStr[i][1];
document.forms[0].elements["m_ArrLocName_Air["+(i)+"]"].value= retSearchStr[i][0];

document.forms[0].elements["m_CmbDepCityCode["+(i)+"]"].value = depSearchStr[i][2];  // 100125
document.forms[0].elements["m_CmbArrCityCode["+(i)+"]"].value = retSearchStr[i][2];  // 100124


/////////  IN DATE

/*document.forms[0].elements["m_CmbDepMn_Air["+i+"]"].value   = Number(document.forms["form1"].elements["air"+i+"_InMonth"].options[document.forms["form1"].elements["air"+i+"_InMonth"].selectedIndex].value)+1;
document.forms[0].elements["m_CmbDepDt_Air["+(i)+"]"].value   = document.forms["form1"].elements["air"+i+"_InDate"].options[document.forms["form1"].elements["air"+i+"_InDate"].selectedIndex].value;
document.forms[0].elements["m_CmbDepYr_Air["+(i)+"]"].value   = document.forms["form1"].elements["air"+i+"_InYear"].options[document.forms["form1"].elements["air"+i+"_InYear"].selectedIndex].value
*/

/*air_InDate=depart[0];
air_InMonth=depart[1];
air_InYear = depart[2];

air_OutDate=arrival[0];
air_OutMonth=arrival[1];
air_OutYear = arrival[2];*/
air_InDate=arrival[0];
air_InMonth=arrival[1];
air_InYear = arrival[2];

air_OutDate=depart[0];
air_OutMonth=depart[1];
air_OutYear = depart[2];
	



document.forms[0].elements["m_CmbDepMn_Air["+i+"]"].value   = Number(air_InMonth);
document.forms[0].elements["m_CmbDepDt_Air["+(i)+"]"].value   = air_InDate;
document.forms[0].elements["m_CmbDepYr_Air["+(i)+"]"].value   = air_InYear;


// OUT date
//var	air_OutDate_m		= document.forms["form1"].elements["air"+i+"_OutDate"].options[document.forms["form1"].elements["air"+i+"_OutDate"].selectedIndex].value;

if(air_OutDate_m<10){
//air_OutDate_m ="0"+air_OutDate_m;
}
var air_OutMonth_m 	= Number(document.forms["form1"].elements["air"+i+"_InMonth"].options[document.forms["form1"].elements["air"+i+"_InMonth"].selectedIndex].value)+1;
if(air_OutMonth_m<10){
//air_OutMonth_m ="0"+air_OutMonth_m;
}		
//document.forms[0].elements["m_DepDate_Air["+(i)+"]"].value	   	= Number(document.forms["form1"].elements["air"+i+"_OutYear"].options[document.forms["form1"].elements["air"+i+"_OutYear"].selectedIndex].value)+"-"+air_OutMonth_m+"-"+(air_OutDate_m-1);

document.forms[0].elements["m_DepDate_Air["+(i)+"]"].value	   	= Number(air_OutYear)+"-"+air_OutMonth+"-"+(air_OutDate);

document.forms[0].elements["m_CmbDepTime_Air["+(i)+"]"].value = document.forms["form1"].elements["Air_DepTime_"+i+""].options[document.forms["form1"].elements["Air_DepTime_"+i+""].selectedIndex].text;
document.forms[0].elements["m_CmbDepCtry["+(i)+"]"].value = depSearchStr[i][5];  // 28
document.forms[0].elements["m_CmbArrCtry["+(i)+"]"].value = retSearchStr[i][5];  // 26	
document.forms[0].noOfDestination.value	= count;



}
document.forms["ResPkgSearchForm"].tripType_Air.value = "M";
	} else {

		// var rangedate = document.getElementById('f-dep-date').value;
		var rangedate = document.getElementById('f-des0-dep-date').value;
		//alert("rangedate---------------->" + rangedate);

		if ($('#Air_TripType1').is(':checked')) {
			
			
			
			if(document.forms["form1"].elements["air_Loc_mul_0"].value=="" || document.forms["form1"].elements["hid_air_Loc_mul_0"].value=="") {
			show_error_msg(f_error_from);
			}
			
			var rangedate = document.getElementById('f-des0-dep-date_single').value;
			var rangedatesplit = rangedate.split("/");
			var arrdateformed = rangedatesplit[2].trim() + "-" + rangedatesplit[1].trim() + "-" + rangedatesplit[0].trim();
			var momentjsdate = moment(arrdateformed, [ "MM-DD-YYYY","YYYY-MM-DD" ]);
			//alert("momentjsdate-------->" + momentjsdate);
			var newdepdarture = moment(arrdateformed,
					[ "MM-DD-YYYY", "YYYY-MM-DD" ], 'fr').add('days', 1)
					.calendar();
			
			//alert("dummydep----------->" + newdepdarture);
			rangedate = rangedate + "-" + newdepdarture;
			
			
		}
		
		var chekingcheckoutdate = rangedate.split("-");
		var checkinDate = chekingcheckoutdate[0].trim();
		var checkoutDate = chekingcheckoutdate[1].trim();
		var datesplit = checkinDate;
		var depart = datesplit.split("/");
		var arrival = checkoutDate.split("/");

		var depSearchStr_a = document.forms["form1"].hid_air_Loc_mul_0.value
				.split(",");
		var retSearchStr_a = document.forms["form1"].hid_air_Loc1_mul_0.value
				.split(",");
		document.forms["ResPkgSearchForm"].cmbNgt.value = 2;
		if (document.forms["form1"].elements["Air_TripType"][0].checked) {
			document.forms["ResPkgSearchForm"].tripType_Air.value = "R";
		} else if (document.forms["form1"].elements["Air_TripType"][1].checked) {
			document.forms["ResPkgSearchForm"].tripType_Air.value = "O";
			document.ResPkgSearchForm.isFlex.value = 'N';
		}

		air_InDate = depart[0];
		air_InMonth = depart[1];

		air_OutDate = arrival[0];
		air_OutMonth = arrival[1];
		// var air_InDate =
		// document.forms["form1"].elements["air0_InDate"].options[document.forms["form1"].elements["air0_InDate"].selectedIndex].value;
		// var air_InMonth =
		// Number(document.forms["form1"].elements["air0_InMonth"].options[document.forms["form1"].elements["air0_InMonth"].selectedIndex].value)+1;
		if (air_InMonth < 10) {
			// air_InMonth ="0"+air_InMonth;
		}
		// var air_OutDate =
		// document.forms["form1"].elements["air0_OutDate"].options[document.forms["form1"].elements["air0_OutDate"].selectedIndex].value;
		// var air_OutMonth =
		// Number(document.forms["form1"].elements["air0_OutMonth"].options[document.forms["form1"].elements["air0_OutMonth"].selectedIndex].value)+1;
		if (air_OutMonth < 10) {
			// air_OutMonth ="0"+air_OutMonth;
		}
		if (document.ResPkgSearchForm.isFlex.value == "Y") {
			document.ResPkgSearchForm.dep_flex.value = document
					.getElementById('dep_flexx').value;
			document.ResPkgSearchForm.ret_flex.value = document
					.getElementById('ret_flexx').value;
			document.ResPkgSearchForm.depFlex.value = document
					.getElementById('dep_flexx').value;
			document.ResPkgSearchForm.retFlex.value = document
					.getElementById('ret_flexx').value;
		}
		var inDateObj = new Date((depart[2]), Number(air_InMonth) - 1,
				Number(air_InDate), 0, 0, 0, 0);
		var outDateObj = new Date((arrival[2]), Number(air_OutMonth) - 1,
				Number(air_OutDate), 0, 0, 0, 0);
		var dtDiff = Number(get_deference(inDateObj, outDateObj));

		document.forms["ResPkgSearchForm"].depDate_Air.value = Number(depart[2])
				+ "-" + air_InMonth + "-" + air_InDate;

		document.forms["ResPkgSearchForm"].cmbDepTime_Air.value = document.forms["form1"].Air_DepTime_0.value;
		document.forms["ResPkgSearchForm"].cmbArrTime_Air.value = document.forms["form1"].Air_RetTime_0.value;

		document.forms["ResPkgSearchForm"].depLoc_Air.value = depSearchStr_a[1];
		document.forms["ResPkgSearchForm"].depLocName_Air.value = depSearchStr_a[0];

		document.forms["ResPkgSearchForm"].arrLoc_Air.value = retSearchStr_a[1];
		document.forms["ResPkgSearchForm"].arrLocName_Air.value = retSearchStr_a[0];

		document.forms["ResPkgSearchForm"].arrDate_Air.value = Number(arrival[2])
				+ "-" + air_OutMonth + "-" + air_OutDate;
		//end of normal flights

	}




document.forms["ResPkgSearchForm"].vacationpkg.value	   	= 'N'
document.forms["ResPkgSearchForm"].cmbAdults_Air.value		= document.forms["form1"].Air_cmbNoOfAdults.value;
document.forms["ResPkgSearchForm"].cmbSeniors_Air.value		= document.forms["form1"].Air_cmbNoOfSeniors.value;
document.forms["ResPkgSearchForm"].cmbChildren_Air.value	= document.forms["form1"].Air_cmbNoOfChildren.value;
document.forms["ResPkgSearchForm"].cmbInfant_Air.value		= document.forms["form1"].Air_cmbNoOfInfants.value;
	
	if(document.forms["form1"].elements["Aira_FlightClass"][0].checked){
	document.forms["ResPkgSearchForm"].seatClass_Air.value = "Economy";
	}else if(document.forms["form1"].elements["Aira_FlightClass"][1].checked){
	document.forms["ResPkgSearchForm"].seatClass_Air.value = "Premium";

	}else if(document.forms["form1"].elements["Aira_FlightClass"][2].checked){
	document.forms["ResPkgSearchForm"].seatClass_Air.value = "Business";
	}else if(document.forms["form1"].elements["Aira_FlightClass"][3].checked){	
	document.forms["ResPkgSearchForm"].seatClass_Air.value = "First";	
	}
	
if(document.forms["form1"].elements["nonStopStatusNew"].checked){
	document.forms["ResPkgSearchForm"].nonStopStatus_Air.value="Nonstop";
	}		
	document.forms["ResPkgSearchForm"].nonStopStatus_Air.value = "";		
	var currentDate = new Date(new Date().getFullYear(),new Date().getMonth(),(new Date().getDate()+f_cutoff));
	var elevenmonths = new Date((new Date().getFullYear()),(new Date().getMonth()+11),(new Date().getDate()));
	
	document.forms["ResPkgSearchForm"].cmbAirLineNew.value = document.forms["form1"].Air_AirLine.value;
	document.forms["ResPkgSearchForm"].cmbAirLine.value = document.forms["form1"].Air_AirLine.value;
	document.forms["ResPkgSearchForm"].airLine_Codes.value = document.forms["form1"].Air_AirLine.value;
	document.forms["ResPkgSearchForm"].airLine_CodesNew.value = document.forms["form1"].Air_AirLine.value;
	
	var airAdult = document.forms["ResPkgSearchForm"].cmbAdults_Air.value;
	var airInf = document.forms["ResPkgSearchForm"].cmbInfant_Air.value
	var totadltchil=Number(document.forms["ResPkgSearchForm"].cmbAdults_Air.value) + Number(document.forms["ResPkgSearchForm"].cmbChildren_Air.value);	
	
	
	// EDITING 
	if ($('#Air_TripType3').is(':checked')) {

		//alert("Multidetination-------------> 01");

		var count = 2;
		if (document.getElementById("multi_des_04").style.display == "block") {
			count = 5;
		} else if (document.getElementById("multi_des_03").style.display == "block") {
			count = 4;
		} else if (document.getElementById("multi_des_02").style.display == "block") {
			count = 3;
		}
		var inDateObj_m_Last = "";
		var i;

		for (i = 0; i < count; i++) {

			var newdatearry = dateSpliting(i);
			// //alert(newdatearry);

			// var air_InMonth_m =
			// Number(document.forms["form1"].elements["air"+i+"_InMonth"].options[document.forms["form1"].elements["air"+i+"_InMonth"].selectedIndex].value)+1;
			var air_InMonth_m = newdatearry[1];

			if (air_InMonth_m < 10) {
				air_InMonth_m = "0" + air_InMonth_m;
			}
			// var air_InDate_m=
			// document.forms["form1"].elements["air"+i+"_InDate"].options[document.forms["form1"].elements["air"+i+"_InDate"].selectedIndex].value;
			var air_InDate_m = newdatearry[0];
			//alert("air_InDate_m----------->"+newdatearry[0]);
			// var air_OutDate_m=
			// document.forms["form1"].elements["air"+i+"_OutDate"].options[document.forms["form1"].elements["air"+i+"_OutDate"].selectedIndex].value;
			var air_OutDate_m = newdatearry[3];
			//alert("air_OutDate_m----------->"+newdatearry[3]);

			if (air_OutDate_m < 10) {
				air_OutDate_m = "0" + air_OutDate_m;
			}
			// var air_OutMonth_m =
			// Number(document.forms["form1"].elements["air"+i+"_InMonth"].options[document.forms["form1"].elements["air"+i+"_InMonth"].selectedIndex].value)+1;
			var air_OutMonth_m = newdatearry[4];
			if (air_OutMonth_m < 10) {
				air_OutMonth_m = "0" + air_OutMonth_m;
			}
			/*
			 * var inDateObj_m=new
			 * Date(Number(document.forms["form1"].elements["air"+i+"_InYear"].options[document.forms["form1"].elements["air"+i+"_InYear"].selectedIndex].value),Number(air_InMonth_m)-1,Number(air_InDate_m),0,0,0,0);
			 * var outDateObj_m=new
			 * Date(Number(document.forms["form1"].elements["air"+i+"_OutYear"].options[document.forms["form1"].elements["air"+i+"_OutYear"].selectedIndex].value),Number(air_OutMonth_m)-1,Number(air_OutDate_m),0,0,0,0);
			 */

			var inDateObj_m = new Date(Number(newdatearry[2]),
					Number(newdatearry[1]), Number(newdatearry[0]), 0, 0, 0, 0);
			var outDateObj_m = new Date(Number(newdatearry[5]),
					Number(newdatearry[4]), Number(newdatearry[3]), 0, 0, 0, 0);

			var dtDiff_m = Number(get_deference(inDateObj_m, outDateObj_m));
			var today = new Date();
			var newdate_m = today.getDate();

			if (document.forms["form1"].elements["air_Loc_mul_" + i].value == ""
					|| document.forms["form1"].elements["hid_air_Loc_mul_" + i].value == "") {
			//	alert("empty");
				show_error_msg(f_error_from);
				return false;
			} else if (document.forms["form1"].elements["air_Loc1_mul_" + i].value == ""
					|| document.forms["form1"].elements["hid_air_Loc1_mul_" + i].value == "") {
				show_error_msg(f_error_to);
				return false;
			} else if (document.forms["form1"].elements["hid_air_Loc_mul_" + i].value == document.forms["form1"].elements["hid_air_Loc1_mul_"
					+ i].value) {
				show_error_msg(f_error_same_loc);
				return false;
			} else if (inDateObj_m > outDateObj_m) {
				show_error_msg(f_error_dep);
				return false;
			} else if (inDateObj_m <= today) {
				show_error_msg(f_error_cutoff_1 + f_cutoff + f_error_cutoff_2);
				return false;
			} else if (inDateObj_m <= inDateObj_m_Last) {
				show_error_msg(f_error_depdatepass);
				return false;
			} else if (inDateObj_m >= elevenmonths
					|| outDateObj_m >= elevenmonths) {
				show_error_msg(f_error_11);
				return false;
			} else if (Number(airAdult) < Number(airInf)) {
				show_error_msg(f_error_adult_inf);
				return false;
			} else if (totadltchil > f_max_pax) {
				show_error_msg(f_error_max_pax);
				return false;
			}

			if (inDateObj_m != inDateObj_m_Last) {
				inDateObj_m_Last = inDateObj_m;
			}

		}
		befSubmit('F');
		document.forms["ResPkgSearchForm"].submit();

	}


else{
		if(document.forms["form1"].elements["air_Loc_mul_0"].value=="" || document.forms["form1"].elements["hid_air_Loc_mul_0"].value=="") {
		show_error_msg(f_error_from);
		} else if(document.forms["form1"].elements["air_Loc1_mul_0"].value=="" || document.forms["form1"].elements["hid_air_Loc1_mul_0"].value=="") {
		show_error_msg(f_error_to);
		return false;
		} else if(document.forms["form1"].elements["hid_air_Loc_mul_0"].value==document.forms["form1"].elements["hid_air_Loc1_mul_0"].value) {
		show_error_msg(f_error_same_loc);
		return false;
		}else if(inDateObj>outDateObj){
		show_error_msg(f_error_dep);
		return false;
		}else if(inDateObj<(currentDate)){
		show_error_msg(f_error_cutoff_1+f_cutoff+f_error_cutoff_2);
		return false;
		}else if(inDateObj>=elevenmonths || outDateObj>=elevenmonths) {
		show_error_msg(f_error_11);
		return false;
		}else if (Number(airAdult)<Number(airInf)){
		show_error_msg(f_error_adult_inf);
		return false;		
		}else if (totadltchil>f_max_pax){
		show_error_msg(f_error_max_pax);
		return false;
		}else{
		befSubmit('F');
		document.forms["ResPkgSearchForm"].submit();
		}
		}
		
				
}


function dateSplitHotel(){
	
	var rangedate = document.getElementById('hotel-arrival-date').value;
	////alert("hotel range date ------------->"+rangedate);
	var chekingcheckoutdate = rangedate.split("-");
	var checkinDate = chekingcheckoutdate[0].trim();
	var checkoutDate = chekingcheckoutdate[1].trim();

	var datesplit = checkinDate;
	var depart = datesplit.split("/");
	var arrival = checkoutDate.split("/");
	
	hotel_InDate=depart[0];
	hotel_InMonth=depart[1];
	hotel_InYear = depart[2];

	hotel_OutDate=arrival[0];
	hotel_OutMonth=arrival[1];
	hotel_OutYear = arrival[2];
	
	var innout=[hotel_InDate,hotel_InMonth,hotel_InYear,hotel_OutDate,hotel_OutMonth,hotel_OutYear];
	return innout;
	
	
}

function loadDataNew_H(){
	setFields("H");
	document.forms["ResPkgSearchForm"].action=subURL+"/packaging/reservation/ResPkgSearchCriteria.do?ActionType=ResSearchHotels";
	if (chkDates()){showError();return;}

	var searchStr = document.forms["form1"].hid_H_Loc.value.split(",");
	document.forms["ResPkgSearchForm"].searchCityId.value		= searchStr[0];
	document.forms["ResPkgSearchForm"].searchCityName.value	= searchStr[1];
	document.forms["ResPkgSearchForm"].searchStateId.value	= searchStr[2];
	document.forms["ResPkgSearchForm"].searchCountryId.value	= searchStr[3];
	
	document.forms["ResPkgSearchForm"].cmbCty_Hotels.value	= document.forms["ResPkgSearchForm"].searchCityId.value;
	document.forms["ResPkgSearchForm"].cityLookup_Hotels.value	= document.forms["ResPkgSearchForm"].searchCityName.value;
	document.forms["ResPkgSearchForm"].cmbSte_Hotels.value	= document.forms["ResPkgSearchForm"].searchStateId.value;
	document.forms["ResPkgSearchForm"].cmbCtry_Hotels.value	= document.forms["ResPkgSearchForm"].searchCountryId.value;
	
	
	
	
	
	/*DATE ******************************/
	/*Hotel Date:  --------------------------------------------------*/
	
	
	var hotelhcekingcheckout =  dateSplitHotel();
	////alert("hotelhcekingcheckout---------------->"+hotelhcekingcheckout);
	
	/*document.forms["ResPkgSearchForm"].cmbInYr_Hotels.value	= Number(document.forms["form1"].elements["H_InYear"].options[document.forms["form1"].elements["H_InYear"].selectedIndex].value);
	document.forms["ResPkgSearchForm"].cmbInMn_Hotels.value	= Number(document.forms["form1"].elements["H_InMonth"].options[document.forms["form1"].elements["H_InMonth"].selectedIndex].value)+1;
	document.forms["ResPkgSearchForm"].cmbInDt_Hotels.value	= document.forms["form1"].elements["H_InDate"].options[document.forms["form1"].elements["H_InDate"].selectedIndex].value;	
	document.forms["ResPkgSearchForm"].cmbNgt_Hotels.value	= Number(document.forms["form1"].elements["H_nights"].options[document.forms["form1"].elements["H_nights"].selectedIndex].value);*/
	
	document.forms["ResPkgSearchForm"].cmbInYr_Hotels.value	= Number(hotelhcekingcheckout[2]);
	document.forms["ResPkgSearchForm"].cmbInMn_Hotels.value	= Number(hotelhcekingcheckout[1]);
	document.forms["ResPkgSearchForm"].cmbInDt_Hotels.value	=hotelhcekingcheckout[0] ;	
	
//	document.forms["ResPkgSearchForm"].cmbNgt_Hotels.value	= Number(document.forms["form1"].elements["H_nights"].options[document.forms["form1"].elements["H_nights"].selectedIndex].value);
	var numberofnights  = document.getElementById('nightCmb').value;
	document.forms["ResPkgSearchForm"].cmbNgt_Hotels.value	= Number(numberofnights);
	
	////alert("nights----------->"+numberofnights);
	
	document.forms["ResPkgSearchForm"].cmbOutYr_Hotels.value	= Number(hotelhcekingcheckout[5]);
	document.forms["ResPkgSearchForm"].cmbOutMn_Hotels.value	= Number(hotelhcekingcheckout[4]);
	document.forms["ResPkgSearchForm"].cmbOutDt_Hotels.value	= hotelhcekingcheckout[3];
	
	
	/*Hotel Date:  --------------------------------------------------*/
	
	document.forms["ResPkgSearchForm"].cmbNoRm_Hotels.value	= Number(document.forms["form1"].elements["H_cmbNoOfRooms"].options[document.forms["form1"].elements["H_cmbNoOfRooms"].selectedIndex].value);	
	document.forms["ResPkgSearchForm"].totRooms_Hotels.value	= Number(document.forms["form1"].elements["H_cmbNoOfRooms"].options[document.forms["form1"].elements["H_cmbNoOfRooms"].selectedIndex].value);
	
	
	
	
	
	document.forms["ResPkgSearchForm"].cmbInMn.value= Number(hotelhcekingcheckout[1]);
	document.forms["ResPkgSearchForm"].cmbInYr.value=Number(hotelhcekingcheckout[2]);
	document.forms["ResPkgSearchForm"].cmbOutMn.value= Number(hotelhcekingcheckout[4]);	
	document.forms["ResPkgSearchForm"].cmbOutYr.value= Number(hotelhcekingcheckout[5]);
	document.forms["ResPkgSearchForm"].cmbInDt.value= hotelhcekingcheckout[0] ;			
	document.forms["ResPkgSearchForm"].cmbOutDt.value= hotelhcekingcheckout[3];
	//document.forms["ResPkgSearchForm"].consumerRegionId.value 		= document.forms["form1"].H_Country.value;
	
	
	/*Hotel Date:  --------------------------------------------------*/
	
	/*DATE ******************************/
	
	
	
	var OutMonth=document.forms["ResPkgSearchForm"].cmbOutMn.value;
	var InMonth=document.forms["ResPkgSearchForm"].cmbInMn.value;
	var cMonth=Number(_InDate.getMonth()+1);
	var d = new Date();
	var curr_year = d.getFullYear();
	var totalAdults 	= 0;
	var totalChildren	= 0;
	var occupancyStr	= "";
	
	if(document.forms["ResPkgSearchForm"].hotelCode_Hotels.value==""){
		document.forms["ResPkgSearchForm"].shtcd_Hotels.value="0";
	}
	if(document.forms["ResPkgSearchForm"].occupancy_Details_Hotels.value==""){
		document.forms["ResPkgSearchForm"].occupancy_Details_Hotels.value = "<room1Adult>1</room1Adults><room1Children>0</room1Children>";
	}
	document.forms["ResPkgSearchForm"].cmbtourOpt_Hotels.value="0";
	document.forms["ResPkgSearchForm"].pkgType.value	= "H";
	
	
	
	
	/*DATE ******************************/
	var currentDate 	= new Date(new Date().getFullYear(),new Date().getMonth(),(new Date().getDate()));
	var checkinDate 	= new Date(Number(document.forms["ResPkgSearchForm"].cmbInYr.value),Number(document.forms["ResPkgSearchForm"].cmbInMn.value)-1,Number(document.forms["ResPkgSearchForm"].cmbInDt.value));
	var checkoutDate 	= new Date(Number(document.forms["ResPkgSearchForm"].cmbOutYr.value),Number(document.forms["ResPkgSearchForm"].cmbOutMn.value)-1,Number(document.forms["ResPkgSearchForm"].cmbOutDt.value));
	/*DATE ******************************/
	
	
	
	
	document.forms["ResPkgSearchForm"].elements["hotelStarId_Hotels"].value    =document.forms["form1"].elements["hstarCat"].options[document.forms["form1"].elements["hstarCat"].selectedIndex].value;
	
	document.forms["ResPkgSearchForm"].elements["hotelType_Hotels"].value    =document.forms["form1"].elements["hType"].options[document.forms["form1"].elements["hType"].selectedIndex].value;
	
	document.forms["ResPkgSearchForm"].hotelName_Hotels.value = document.forms["form1"].hotelName.value;
	
	var newHotelInDate=new Date(Number(document.forms["ResPkgSearchForm"].cmbInYr_Hotels.value),Number(document.forms["ResPkgSearchForm"].cmbInMn_Hotels.value)-1,Number(document.forms["ResPkgSearchForm"].cmbInDt_Hotels.value),0,0,0,0);
	
	var newHotelOutDate=new Date(Number(document.forms["ResPkgSearchForm"].cmbOutYr_Hotels.value),Number(document.forms["ResPkgSearchForm"].cmbOutMn_Hotels.value)-1,Number(document.forms["ResPkgSearchForm"].cmbOutDt_Hotels.value),0,0,0,0);
	
	var hotelDtDiff=Number(get_deference(newHotelInDate,newHotelOutDate));	
	var today = new Date(new Date().getFullYear(),new Date().getMonth(),(new Date().getDate()));
	var checkHotPastDt = Number(get_deference(today,newHotelInDate));	
	
	if(hotelDtDiff > 0 && hotelDtDiff < 31){
		//document.forms["form1"].elements["H_nights"].options.value = hotelDtDiff;
		// setting date
		var selectnights = document.getElementById("nightCmb");
		selectnights[hotelDtDiff].selected = true;
	}	
	if (searchStr=="" || document.forms["form1"].elements["hid_H_Loc"].value==""){
		show_error_msg(h_error_loc);
	}else if(Number(checkHotPastDt) < h_cutoff){
	show_error_msg(h_error_cutoff_1+h_cutoff+h_error_cutoff_2);
}else if(Number(hotelDtDiff) > h_max_nights){
		show_error_msg(h_error_max);			

}else if(Number(hotelDtDiff) < h_min_nights && Number(hotelDtDiff) > 0){
	show_error_msg(h_error_min);
	}else if(Number(hotelDtDiff) < 0){
	show_error_msg(h_error_dep);
	} else {	
		befSubmit('H');
		document.forms["ResPkgSearchForm"].submit();
	}
}


function dateSplitActivity(){
	
	var rangedate = document.getElementById('act-chk-in-date').value;
	////alert("activity range date ------------->"+rangedate);
	var chekingcheckoutdate = rangedate.split("-");
	var checkinDate = chekingcheckoutdate[0].trim();
	var checkoutDate = chekingcheckoutdate[1].trim();

	var datesplit = checkinDate;
	var depart = datesplit.split("/");
	var arrival = checkoutDate.split("/");
	
	activity_InDate=depart[0];
	activity_InMonth=depart[1];
	activity_InYear = depart[2];

	activity_OutDate=arrival[0];
	activity_OutMonth=arrival[1];
	activity_OutYear = arrival[2];
	
	var innout=[activity_InDate,activity_InMonth,activity_InYear,activity_OutDate,activity_OutMonth,activity_OutYear];
	return innout;
	
	
}


//activity
function loadDataNew_A(){
	
	setFields("A");
	document.forms["ResPkgSearchForm"].action=subURL+"/packaging/reservation/ResPkgSearchCriteria.do?ActionType=ResSearchActivities";
	if (chkDates()){showError();return;}
	
	var searchStr = document.forms["form1"].hid_A_Loc.value.split(",");
	document.forms["ResPkgSearchForm"].searchCityId.value	= searchStr[0];
	document.forms["ResPkgSearchForm"].searchCityName.value	= searchStr[1];
	document.forms["ResPkgSearchForm"].searchStateId.value	= searchStr[2];
	document.forms["ResPkgSearchForm"].searchCountryId.value	= searchStr[3];
	document.forms["ResPkgSearchForm"].cmbCty_Activities.value	= document.forms["ResPkgSearchForm"].searchCityId.value;
	document.forms["ResPkgSearchForm"].cityLookup_Activities.value	= document.forms["ResPkgSearchForm"].searchCityName.value;
	document.forms["ResPkgSearchForm"].cmbCtry_Activities.value	= document.forms["ResPkgSearchForm"].searchCountryId.value;
	
	
	
	/*DATE ******************************/
	
	
	/*document.forms["ResPkgSearchForm"].cmbInYr_Activities.value	= Number(document.forms["form1"].elements["activity_InYear"].options[document.forms["form1"].elements["activity_InYear"].selectedIndex].value);
	document.forms["ResPkgSearchForm"].cmbInMn_Activities.value	= Number(document.forms["form1"].elements["activity_InMonth"].options[document.forms["form1"].elements["activity_InMonth"].selectedIndex].value)+1;
	document.forms["ResPkgSearchForm"].cmbInDt_Activities.value	= document.forms["form1"].elements["activity_InDate"].options[document.forms["form1"].elements["activity_InDate"].selectedIndex].value;
	
	document.forms["ResPkgSearchForm"].cmbOutYr_Activities.value	= Number(document.forms["form1"].elements["activity_OutYear"].options[document.forms["form1"].elements["activity_OutYear"].selectedIndex].value);
	document.forms["ResPkgSearchForm"].cmbOutMn_Activities.value	= Number(document.forms["form1"].elements["activity_OutMonth"].options[document.forms["form1"].elements["activity_OutMonth"].selectedIndex].value)+1;
	document.forms["ResPkgSearchForm"].cmbOutDt_Activities.value	= document.forms["form1"].elements["activity_OutDate"].options[document.forms["form1"].elements["activity_OutDate"].selectedIndex].value;
	
	document.forms["ResPkgSearchForm"].cmbInMn.value=Number(document.forms["form1"].elements["activity_InMonth"].options[document.forms["form1"].elements["activity_InMonth"].selectedIndex].value)+1;
	document.forms["ResPkgSearchForm"].cmbInYr.value=document.forms["form1"].elements["activity_InYear"].options[document.forms["form1"].elements["activity_InYear"].selectedIndex].value;
	document.forms["ResPkgSearchForm"].cmbOutMn.value=Number(document.forms["form1"].elements["activity_OutMonth"].options[document.forms["form1"].elements["activity_OutMonth"].selectedIndex].value)+1;
	document.forms["ResPkgSearchForm"].cmbOutYr.value=document.forms["form1"].elements["activity_OutYear"].options[document.forms["form1"].elements["activity_OutYear"].selectedIndex].value;
	
	document.forms["ResPkgSearchForm"].cmbInDt.value=document.forms["form1"].elements["activity_InDate"].options[document.forms["form1"].elements["activity_InDate"].selectedIndex].value;
	document.forms["ResPkgSearchForm"].cmbOutDt.value=document.forms["form1"].elements["activity_OutDate"].options[document.forms["form1"].elements["activity_OutDate"].selectedIndex].value;
	*/
	
	var activitycheckingcheckout    =   dateSplitActivity();
	
	
	document.forms["ResPkgSearchForm"].cmbInYr_Activities.value	= Number(activitycheckingcheckout[2]);
	document.forms["ResPkgSearchForm"].cmbInMn_Activities.value	= activitycheckingcheckout[1];
	document.forms["ResPkgSearchForm"].cmbInDt_Activities.value	= activitycheckingcheckout[0];
	
	document.forms["ResPkgSearchForm"].cmbOutYr_Activities.value	= Number(activitycheckingcheckout[5]);
	document.forms["ResPkgSearchForm"].cmbOutMn_Activities.value	= Number(activitycheckingcheckout[4]);
	document.forms["ResPkgSearchForm"].cmbOutDt_Activities.value	= activitycheckingcheckout[3];
	
	
	
	
	
	
	
	/*DATE ******************************/
	
	
	
	document.forms["ResPkgSearchForm"].cmbNoOfAdults_Activities.value 	= document.forms["form1"].elements["R1occAdults_A"].value;
	document.forms["ResPkgSearchForm"].cmbNoOfChildren_Activities.value = document.forms["form1"].elements["R1occChi_A"].value;
	
	document.forms[0].cmbProgTyp_Activities.value	= "0";
	document.forms[0].actType_Activities.value	= "0";
	
	var roomChildren= Number(document.forms["ResPkgSearchForm"].cmbNoOfChildren_Activities.value);
	document.forms["ResPkgSearchForm"].gstChildAges_Activities.value = "";
	
	for(var childCount=0;childCount<Number(roomChildren);childCount++){
		document.forms["ResPkgSearchForm"].gstChildAges_Activities.value+=document.forms["form1"].elements["R1occAge"+(childCount+1)+"_A"].value;
		if(childCount!=(Number(roomChildren)-1)){
			document.forms["ResPkgSearchForm"].gstChildAges_Activities.value+=",";
		}
	}
	
	
	
	var OutMonth=document.forms["ResPkgSearchForm"].cmbOutMn.value;
	var InMonth=document.forms["ResPkgSearchForm"].cmbInMn.value;
	var cMonth=Number(_InDate.getMonth()+1);

	var d = new Date();
	var curr_year = d.getFullYear();
	
	
	document.forms["ResPkgSearchForm"].cmbNgt_Activities.value	= Number(document.forms["form1"].elements["activity_nights"].options[document.forms["form1"].elements["activity_nights"].selectedIndex].value);
	document.forms["ResPkgSearchForm"].actType_Activities.value	= document.forms["form1"].elements["Act_type"].options[document.forms["form1"].elements["Act_type"].selectedIndex].value;
	document.forms["ResPkgSearchForm"].pkgType.value	= "A";
	document.forms["ResPkgSearchForm"].consumerRegionId.value 		= document.forms["form1"].A_Country.value;
	//document.forms["ResPkgSearchForm"].consumerRegionId.value 		= "0~ZA";
	var currentDate 	= new Date(new Date().getFullYear(),new Date().getMonth(),(new Date().getDate()+a_cutoff));
	var checkinDate 	= new Date(Number(document.forms["ResPkgSearchForm"].cmbInYr.value),Number(document.forms["ResPkgSearchForm"].cmbInMn.value)-1,Number(document.forms["ResPkgSearchForm"].cmbInDt.value));
	var checkoutDate 	= new Date(Number(document.forms["ResPkgSearchForm"].cmbOutYr.value),Number(document.forms["ResPkgSearchForm"].cmbOutMn.value)-1,Number(document.forms["ResPkgSearchForm"].cmbOutDt.value));
	
	if(document.getElementById("A_Country").value=="null"){
		show_error_msg(a_error_res);
	}else if (searchStr==""){
		show_error_msg(a_error_loc);	
	}else if (checkinDate>=checkoutDate){
		show_error_msg(a_error_dep);
	}else if (checkinDate<currentDate){
		show_error_msg(a_error_cutoff_1+a_cutoff+a_error_cutoff_2);
	}else {
		befSubmit('A');
		document.forms["ResPkgSearchForm"].submit();
	}
	
}




function dateSplitCar(){
	
	var rangedate = document.getElementById('c-ret-date').value;
	////alert("car range date ------------->"+rangedate);
	var chekingcheckoutdate = rangedate.split("-");
	var checkinDate = chekingcheckoutdate[0].trim();
	var checkoutDate = chekingcheckoutdate[1].trim();

	var datesplit = checkinDate;
	var depart = datesplit.split("/");
	var arrival = checkoutDate.split("/");
	
	car_InDate=depart[0];
	car_InMonth=depart[1];
	car_InYear = depart[2];

	car_OutDate=arrival[0];
	car_OutMonth=arrival[1];
	car_OutYear = arrival[2];
	
	var innout=[car_InDate,car_InMonth,car_InYear,car_OutDate,car_OutMonth,car_OutYear];
	return innout;
	
	
}


//car
function loadDataNew_C(){

	
	setFields("C");
	document.forms["ResPkgSearchForm"].action=subURL+"/packaging/reservation/ResPkgSearchCriteria.do?ActionType=ResSearchCar";
	if (chkDates()){showError();return;}
	
	var depSearchStr = document.forms["form1"].hid_car_Loc.value.split(",");
	
	document.forms["ResPkgSearchForm"].pickupLocName_Car.value 	= depSearchStr[1];
	document.forms["ResPkgSearchForm"].pickupLoc_Car.value		= depSearchStr[0];
	document.forms["ResPkgSearchForm"].pickupAirport_Car.value	= depSearchStr[3];

	
      var carcheckingcheckout  =	dateSplitCar();
      ////alert("carcheckingcheckout--------------->"+carcheckingcheckout);
	
	/*DATE ******************************/
/*	var InDate		= document.forms["form1"].elements["car_InDate"].options[document.forms["form1"].elements["car_InDate"].selectedIndex].value;
	var InMonth 	= Number(document.forms["form1"].elements["car_InMonth"].options[document.forms["form1"].elements["car_InMonth"].selectedIndex].value)+1;*/
	
	var InDate		=  carcheckingcheckout[0];
	var InMonth 	= Number(carcheckingcheckout[1]);
	/*DATE ******************************/
	var InYear = carcheckingcheckout[2];
	
	if(InMonth<10){
		InMonth ="0"+InMonth;
	}
	
	
	
	/*DATE ******************************/
	/*var	OutDate		= document.forms["form1"].elements["car_OutDate"].options[document.forms["form1"].elements["car_OutDate"].selectedIndex].value;
	var OutMonth 	= Number(document.forms["form1"].elements["car_OutMonth"].options[document.forms["form1"].elements["car_OutMonth"].selectedIndex].value)+1;*/
	
	var	OutYear		= carcheckingcheckout[5];
	var	OutDate		= carcheckingcheckout[3];
	var OutMonth 	= Number(carcheckingcheckout[4]);
	if(OutMonth<10){
		OutMonth ="0"+OutMonth;
	}	
	/*DATE ******************************/
	
	var cMonth=Number(_InDate.getMonth()+1);
	var d = new Date();
	var curr_year = d.getFullYear();
	
	
	/*DATE ******************************/
	/*document.forms["ResPkgSearchForm"].pickupDate_Car.value		= Number(document.forms["form1"].elements["car_InYear"].options[document.forms["form1"].elements["car_InYear"].selectedIndex].value)+"-"+InMonth+"-"+InDate;*/
	document.forms["ResPkgSearchForm"].pickupDate_Car.value		= Number(InYear)+"-"+InMonth+"-"+InDate;
	document.forms["ResPkgSearchForm"].pickupTime_Car.value		= document.forms["form1"].PickTime.value;
	
	/*document.forms["ResPkgSearchForm"].returnDate_Car.value		= Number(document.forms["form1"].elements["car_OutYear"].options[document.forms["form1"].elements["car_OutYear"].selectedIndex].value)+"-"+OutMonth+"-"+OutDate;*/
	document.forms["ResPkgSearchForm"].returnDate_Car.value		=  Number(OutYear)+"-"+OutMonth+"-"+OutDate;
	
	document.forms["ResPkgSearchForm"].retTime_Car.value		= document.forms["form1"].ReturnTime.value;
	document.forms["ResPkgSearchForm"].pkgType.value			= "C";
	document.forms["ResPkgSearchForm"].consumerRegionId.value 		= document.forms["form1"].C_Country.value;	
	document.forms["ResPkgSearchForm"].carType_Car.value		= document.forms["form1"]._CarType.value;
	
	
	
	/*DATE ******************************/
	
	
	
	if(document.forms["form1"].elements["_ReturnCar"].checked){
		document.forms["ResPkgSearchForm"].retupLoc_Car.value=document.forms["ResPkgSearchForm"].pickupLoc_Car.value;
		document.forms["ResPkgSearchForm"].retupLocName_Car.value=document.forms["ResPkgSearchForm"].pickupLocName_Car.value;
	}else{
		var retSearchStr = document.forms["form1"].hid_car_Loc1.value.split(",");
		document.forms["ResPkgSearchForm"].retupLocName_Car.value 	= retSearchStr[1];
		document.forms["ResPkgSearchForm"].retupLoc_Car.value		= retSearchStr[0];
		document.forms["ResPkgSearchForm"].retupAirport_Car.value	   	= retSearchStr[3];
	}
	
	
	if(document.forms["form1"].elements["_ReturnCar"].checked){
		var arrcityid = depSearchStr[2]; 
		document.forms["ResPkgSearchForm"].cmbCty_Hotels.value  = arrcityid; 
		document.forms["ResPkgSearchForm"].cmbDepCityCode.value = depSearchStr[2];
		document.forms["ResPkgSearchForm"].cmbArrCityCode.value = arrcityid;
		document.forms["ResPkgSearchForm"].cmbArrCtry.value = depSearchStr[5];
	}else{
		var arrcityid = retSearchStr[2]; 
		document.forms["ResPkgSearchForm"].cmbCty_Hotels.value  = arrcityid; 
		document.forms["ResPkgSearchForm"].cmbDepCityCode.value = depSearchStr[2];
		document.forms["ResPkgSearchForm"].cmbArrCityCode.value = arrcityid;
		document.forms["ResPkgSearchForm"].cmbArrCtry.value = retSearchStr[5];
	}
	
	var currentDate 	= new Date(new Date().getFullYear(),new Date().getMonth(),(new Date().getDate()+c_cutoff));
	var checkinDate 	= new Date(Number(document.forms["ResPkgSearchForm"].pickupDate_Car.value.split("-")[0]),Number(document.forms["ResPkgSearchForm"].pickupDate_Car.value.split("-")[1])-1,Number(document.forms["ResPkgSearchForm"].pickupDate_Car.value.split("-")[2]));
	var checkoutDate 	= new Date(Number(document.forms["ResPkgSearchForm"].returnDate_Car.value.split("-")[0]),Number(document.forms["ResPkgSearchForm"].returnDate_Car.value.split("-")[1])-1,Number(document.forms["ResPkgSearchForm"].returnDate_Car.value.split("-")[2]));
	
	
	/*DATE ******************************/
	/*var newCarInDate=new Date(Number(document.forms["form1"].elements["car_InYear"].options[document.forms["form1"].elements["car_InYear"].selectedIndex].value),Number(InMonth)-1,Number(InDate),0,0,0,0);
	var newCarOutDate=new Date(Number(document.forms["form1"].elements["car_OutYear"].options[document.forms["form1"].elements["car_OutYear"].selectedIndex].value),Number(OutMonth)-1,Number(OutDate),0,0,0,0);*/
	
	var newCarInDate=new Date(Number(InYear),Number(InMonth),Number(InDate),0,0,0,0);
	var newCarOutDate=new Date(Number(OutYear),Number(OutMonth),Number(OutDate),0,0,0,0);
	
	/*DATE ******************************/
	
	
	var carDtDiff=Number(get_deference(newCarInDate,newCarOutDate));	
	/*var today = new Date();*/
	var today = new Date(new Date().getFullYear(),new Date().getMonth(),(new Date().getDate()));
	var checkCarPastDt = Number(get_deference(today,newCarInDate));	
	if(document.getElementById("C_Country").value=="null"){
		show_error_msg(c_error_res);
	}else if(depSearchStr==""){
		show_error_msg(c_error_from);
	}else if(retSearchStr==""){
		show_error_msg(c_error_to);	
	}else if(Number(checkCarPastDt) < c_cutoff){
	show_error_msg(c_error_cutoff_1+c_cutoff+c_error_cutoff_2);
}else if (Number(carDtDiff)<0){
		show_error_msg(c_error_dep);
}else { 
	befSubmit('C');
	document.forms["ResPkgSearchForm"].submit();
}
}

var checkMore_new=0;
function setCharSecond(ch,img){
var flag =0;
var tmpArr = new Array();
	for (var j=0; j<airTxtArr.length; j++){
		if (airTxtArr[j][0].toUpperCase().indexOf(ch.value.toUpperCase())==0){
			if (!showAllLoc){checkMore_new++;if (checkMore_new>10){break;}}
			tmpArr[tmpArr.length]="<tr><td class=\"stripe"+((j%2)+3)+"\" style=\"padding-left:5px;cursor:pointer;\" onMouseOver=\"altcell('ov',this)\" onMouseOut=\"altcell('ou',this,'stripe"+((j%2)+3)+"')\" onClick=\"setValSecond("+j+",'"+ch.name+"')\">"+airTxtArr[j][0]+" - "+airTxtArr[j][1]+"</td></tr>";
			flag = 1;
		}
		if ((airTxtArr[j][1].toUpperCase().indexOf(ch.value.toUpperCase())==0) && flag==0){
			if (!showAllLoc){checkMore_new++;if (checkMore_new>10){break;}}
			tmpArr[tmpArr.length]="<tr><td class=\"stripe"+((j%2)+3)+"\" style=\"padding-left:5px;cursor:pointer;\" onMouseOver=\"altcell('ov',this)\" onMouseOut=\"altcell('ou',this,'stripe"+((j%2)+3)+"')\" onClick=\"setValSecond("+j+",'"+ch.name+"')\">"+airTxtArr[j][0]+" - "+airTxtArr[j][1]+"</td></tr>";
		}
		var flag =0;
	}
	if (!showAllLoc){
		if (checkMore_new>10){tmpArr[tmpArr.length]="<tr><td class=\"morebg\" style=\"padding-left:5px;cursor:pointer;\" onClick=\"showAllLoc=true;setCharSecond(document.forms['form1'].elements['"+ch.name+"'],'"+img+"')\" title=\"Show All\" align=\"center\"><img src=\"images/more_arrow.gif\" width=\"9\" height=\"6\" border=\"0\" hspace=\"3\" alt=\"Show All\" align=\"absmiddle\">Show All</td></tr>";}
	}
	checkMore_new=0;
	
	var outSt="";
if (tmpArr.length==0){
	outSt="<table border=\"0\" width=\"100%\" cellspacing=\"1\" cellpadding=\"3\" class=\"databorder\"><tr><td class=\"stripe4\" onClick=\"setObjPos('ixpDataFrame',0,0,180,0,'none');setObjPos('maskFrame',0,0,180,0,'none');\" align=\"center\">... No Results Available ...</td></tr></table>";
	showRes(img,outSt,1);
}else{
	outSt="<table border=\"0\" width=\"100%\" cellspacing=\"1\" cellpadding=\"4\" class=\"databorder\">"+tmpArr.join("")+"</table>";
	showRes(img,outSt,tmpArr.length);
}
tmpArr.length=0;
}

function setValSecond(index,obj){
	document.forms["form1"].elements["hid_air_Loc1_a"].value=airTxtArr[index].join("|");
	document.forms["form1"].elements["hid_car_Loc1"].value=airTxtArr[index].join("|");
	document.forms["form1"].elements["V_RetLocHid"].value=airTxtArr[index].join("|");
	
	document.forms["form1"].elements[obj].value=airTxtArr[index][0];
			
	setObjPos('ixpDataFrame',0,0,180,0,'none');
	setObjPos('maskFrame',0,0,180,0,'none');
	showAllLoc=false;
}

function altcell(st,obj,cls){
	if (st=="ov"){
	obj.className="overclass";
	}else{
	obj.className=cls;
	}
}



function setDeaprtureDateToDateRange() {
	////alert("setDeaprtureDateToDateRange");
	var rangedate = document.getElementById('hotel-arrival-date').value;
	var chekingcheckoutdate = rangedate.split("-");
	var checkinDate = chekingcheckoutdate[0];
	var checkoutDate = chekingcheckoutdate[1];

	var datesplit = checkinDate;
	var res = datesplit.split("/");
	var arrdateformed = res[2].trim() + "-" + res[1].trim() + "-" + res[0].trim();
	////alert("form date-------->"+arrdateformed);
	
	
	var nightsCmb = document.getElementById('nightCmb');
	var nights = nightsCmb.options[nightsCmb.selectedIndex].value;
	var numberofnights = Number(nights);

	 ////alert("numberofnights-------->"+numberofnights);
	var myDate = new Date("" + arrdateformed + "");
	////alert("transfer date-------->"+myDate);
	myDate.setDate(myDate.getDate() + numberofnights);
	var newdepdate = myDate.getDate();
	////alert("transfer newdepdate-------->"+newdepdate);

	var newdepmonth = myDate.getMonth() + 1;
	if (newdepmonth < 10) {
		newdepmonth = "0" + newdepmonth;
	}
	if (newdepdate < 10) {
		newdepdate = "0" + newdepdate;
	}
	var newdepyear = myDate.getFullYear();
	var newfordepdate = newdepdate + "/" + newdepmonth + "/" + newdepyear;

	var checkoutDate = newfordepdate;

	// var setdepdate = document.getElementById("depart_date");
	var newrangedate = checkinDate + "-" + checkoutDate;

	 ////alert("new range-------->"+newrangedate);
	var newdaterangetext = document.getElementById("hotel-arrival-date");
	newdaterangetext.value = newrangedate;
	// setdepdate.value = newfordepdate;
}



function setNoOfNightsToDateRange() {

	var rangedate = document.getElementById('hotel-arrival-date').value;
	// ////alert("date range---------->"+rangedate);

	var rangedate = document.getElementById('hotel-arrival-date').value;
	var chekingcheckoutdate = rangedate.split("-");
	var checkinDate = chekingcheckoutdate[0];
	var checkoutDate = chekingcheckoutdate[1];

	var cin = checkinDate.split('/');
	var cout = checkoutDate.split('/');

	var newcin = cin[1].trim() + "/" + cin[0].trim() + "/" + cin[2].trim();
	var newcout = cout[1].trim() + "/" + cout[0].trim() + "/" + cout[2].trim();

	// ////alert(newcin);
	// ////alert(newcout);

	var date1 = new Date(newcin);
	var date2 = new Date(newcout);
	var diffDays = date2.getTime() - date1.getTime();
	// ////alert(diffDays/(1000 * 60 * 60 * 24));
	var dif = diffDays / (1000 * 60 * 60 * 24)
	var numberofngt = Number(dif) - 1;
	var selectnights = document.getElementById("nightCmb");
	selectnights[numberofngt].selected = true;


}

function multidestinationValidation(){
	
	if ($('#Air_TripType3').is(':checked')) {

		//alert("Multidetination-------------> multidestinationValidation");
		
		
		
		var count = 2;
		if (document.getElementById("multi_des_04").style.display == "block") {
			count = 5;
		} else if (document.getElementById("multi_des_03").style.display == "block") {
			count = 4;
		} else if (document.getElementById("multi_des_02").style.display == "block") {
			count = 3;
		}
		var inDateObj_m_Last = "";
		var i;

		
		if(document.getElementById("f-des0-dep-date_single") == "" || document.getElementById("f-des0-dep-date_single") == null){
			
			show_error_msg(f_error_multides_date);
		}
		
		for (i = 0; i < count; i++) {


			if (document.forms["form1"].elements["air_Loc_mul_" + i].value == ""
					|| document.forms["form1"].elements["hid_air_Loc_mul_" + i].value == "") {
			//	alert("empty");
				show_error_msg(f_error_from);
				return false;
			} else if (document.forms["form1"].elements["air_Loc1_mul_" + i].value == ""
					|| document.forms["form1"].elements["hid_air_Loc1_mul_" + i].value == "") {
				show_error_msg(f_error_to);
				return false;
			} else if (document.forms["form1"].elements["hid_air_Loc_mul_" + i].value == document.forms["form1"].elements["hid_air_Loc1_mul_"
					+ i].value) {
				show_error_msg(f_error_same_loc);
				return false;
			} else if(document.getElementById("f-des0-dep-date_single") == "" || document.getElementById("f-des0-dep-date_single") == null){
				
				show_error_msg(f_error_multides_date);
				return false;
				
			} else if(document.getElementById("f-des"+(i+1)+"-dep-date") == "" || document.getElementById("f-des"+(i+1)+"-dep-date") == null){
				
				show_error_msg(f_error_multides_date);
				return false;
			
				
			}

		}
	

	}
}



function multidestionationDateValidation(count){
	var ret = true;
//	show_error_msg("how in this word you book a flight with out date. Bloody moron!! ");
	//alert("date validation------>"+document.getElementById("f-des0-dep-date_single").value);
	document.getElementById("f-des0-dep-date_single").value;
	
	if (document.getElementById("f-des0-dep-date_single").value === "") {
		//alert("date validation------> inside if");
		show_error_msg(f_error_multides_date);
		ret = false;
		/*return false;*/
	}
	
	
	for (i = 0; i < (count-1); i++) {
		
		var x = document.getElementById("f-des"+(i+1)+"-dep-date").value;
		if (x === "") {
			
			show_error_msg("Please Select The Departure Date");
		}
	}
	
	return ret;
}

$(document).ready(function(){
  $("#Air_TripType1").click(function(){
    $("#ret_flexx").hide();
  });
  $("#Air_TripType3").click(function(){
    $("#ret_flexx").hide();
  });
}); 
