$(function() {
	$('#RezEngine-selector').tabs();
});

$(function() {

	$('.flights').click(function(){
		setEngType('F');
	});
	$('.hotels').click(function(){
		setEngType('H');
	});
	$('.activities').click(function(){
		setEngType('A');
	});
	$('.vacations').click(function(){
		setEngType('V');
	});
	$('.cars').click(function(){
		setEngType('C');
	});
})

function setEngType(val){
	hide_error_msg();
//alert("1");
	var roomComboData = "<table border='0' cellspacing='0' cellpadding='0'>"+
						"<tr>"+
						"<td valign='top' width='55' class='entxt'>Room(s)<br><select name='H_cmbNoOfRooms' style='width:40px;' onChange='showRooms(Number(this.options[this.selectedIndex].value));'>"+opts(1,6,1)+
						"</select></td><td>"+buildOcc()+"</td><td class='entxt'></td></tr></table></span><span id='adultsCombo' style='display:none;'></span></td></tr><tr>"+
						"<td colspan='2'><img src='images/ixps.gif' width='1' height='1'><span id='occBox' style='display:block;'>";

	var roomComboData1 = "<table border='0' cellspacing='0' cellpadding='0'>"+
						 "<tr><td width='45' class='entxt' valign='top' style='padding-right: 20px; display:block;'>Room(s)"+
						 "<br><select name='H_cmbNoOfRooms' onChange='showRooms(Number(this.options[this.selectedIndex].value));' style='width:42px;'>"+opts(1,6,1)+"</select>"+
						 "</td><td valign='top'>"+buildVac()+"</td></tr></table>";
	
	var roomComboData3=buildActivityOcc();

	document.forms["form1"].pkgType.value = val;
	document.forms["ResPkgSearchForm"].pkgType.value = val;
	document.forms["ResPkgSearchForm"].elements["engineType"].value=val;
	
if (val=="H"){
		document.getElementById("closetheCladnder").value = "Y";
		document.getElementById("setCalType").value = "H";
		document.getElementById("roomComboV").innerHTML="";
		document.getElementById("roomComboH").innerHTML=roomComboData;	
//alert("2");	
		document.getElementById("hotelDisplay").style.display="block";
		document.getElementById("activityDisplay").style.display="none";
		document.getElementById("carDisplay").style.display="none";
		document.getElementById("vacationDisplay").style.display="none";
		document.getElementById("airDisplay").style.display="none";
		document.forms["form1"].H_Loc.value="";		
		showRooms(1);
		document.forms["form1"].elements["H_nights"].value="2";
		document.forms["form1"].elements["hid_H_Loc"].value="";
		document.forms["form1"].elements["hid_A_Loc"].value="";
		document.forms["form1"].elements["hid_car_Loc"].value="";
		document.forms["form1"].elements["V_DepFromHid"].value="";
		document.forms["form1"].elements["hid_car_Loc1"].value="";
		document.forms["form1"].elements["V_RetLocHid"].value="";
		document.forms["form1"].elements["hid_air_Loc_mul_0"].value="";
		document.forms["form1"].elements["hid_air_Loc1_mul_0"].value="";	
	}else if (val=="A"){
		document.getElementById("closetheCladnder").value = "N";		
		document.getElementById("setCalType").value = "A";
		
		document.getElementById("hotelDisplay").style.display="none";
		document.getElementById("vacationDisplay").style.display="none";
		document.getElementById("activityDisplay").style.display="block";
		document.getElementById("airDisplay").style.display="none";
		document.getElementById("carDisplay").style.display="none";
		document.forms["form1"].activity_Loc.value="";
		document.forms["form1"].elements["activity_nights"].value="1";
		document.forms["form1"].elements["Act_type"].value="0";
		document.forms["form1"].elements["hid_H_Loc"].value="";
		document.forms["form1"].elements["hid_A_Loc"].value="";
		document.forms["form1"].elements["hid_car_Loc"].value="";
		document.forms["form1"].elements["V_DepFromHid"].value="";
		document.forms["form1"].elements["hid_car_Loc1"].value="";
		document.forms["form1"].elements["V_RetLocHid"].value="";	
		document.forms["form1"].elements["hid_air_Loc_mul_0"].value="";
		document.forms["form1"].elements["hid_air_Loc1_mul_0"].value="";
		document.getElementById("roomComboA").innerHTML=roomComboData3;			
		
	}else if (val=="V"){
		document.getElementById("closetheCladnder").value = "N";
		document.getElementById("setCalType").value = "V";		
		
		document.getElementById("roomComboV").innerHTML=roomComboData1;
		document.getElementById("roomComboH").innerHTML="";
		document.getElementById("hotelDisplay").style.display="none";
		document.getElementById("activityDisplay").style.display="none";
		document.getElementById("carDisplay").style.display="none";
		document.getElementById("vacationDisplay").style.display="block";
		document.getElementById("airDisplay").style.display="none";
		
		document.forms["form1"].V_DepFrom.value="City or Airport";
		document.forms["form1"].V_RetLoc.value="City or Airport";
		document.forms["form1"].Air_RetTime.value="AnyTime";
		document.forms["form1"].Air_DepTime.value="AnyTime";
		document.forms["form1"].elements["Air_FlightClass"][0].checked="1";
		document.forms["form1"].elements["Air_FlightClass"][1].checked="";
		document.forms["form1"].elements["Air_FlightClass"][2].checked="";	
		document.forms["form1"].elements["V_DepFromHid"].value="";		
		document.forms["form1"].elements["V_RetLocHid"].value="";
		document.forms["form1"].elements["hid_car_Loc"].value="";
		document.forms["form1"].elements["hid_car_Loc1"].value="";
		document.forms["form1"].elements["hid_air_Loc_mul_0"].value="";
		document.forms["form1"].elements["hid_air_Loc1_mul_0"].value="";
		document.forms["form1"].elements["air_nights"].value="1";	
			
	
	}else if (val=="C"){
		document.getElementById("closetheCladnder").value = "N";
		document.getElementById("setCalType").value = "C";
		
		document.getElementById("hotelDisplay").style.display="none";
		document.getElementById("activityDisplay").style.display="none";
		document.getElementById("vacationDisplay").style.display="none";
		document.getElementById("carDisplay").style.display="block";
		document.getElementById("airDisplay").style.display="none";
		document.forms["form1"].car_Loc.value="City or Airport";
		document.forms["form1"].car_Loc1.value="City or Airport";
		document.forms["form1"].PickTime.value="12:00:00";
		document.forms["form1"].ReturnTime.value="12:00:00";		
		document.forms["form1"].elements["_ReturnCar"].checked="";
		document.forms["form1"].elements["hid_H_Loc"].value="";
		document.forms["form1"].elements["hid_A_Loc"].value="";
		document.forms["form1"].elements["hid_car_Loc"].value="";
		document.forms["form1"].elements["V_DepFromHid"].value="";
		document.forms["form1"].elements["hid_car_Loc1"].value="";
		document.forms["form1"].elements["V_RetLocHid"].value="";
		document.forms["form1"].elements["hid_air_Loc_mul_0"].value="";
		document.forms["form1"].elements["hid_air_Loc1_mul_0"].value="";
		document.forms["form1"].elements["car_nights"].value="1";
		document.forms["form1"]._CarType.value="ALL";
		returnLocHide();
						

}else if (val=="F"){
	document.getElementById("closetheCladnder").value = "N";
	
		document.getElementById("setCalType").value = "F";
		
		document.getElementById("hotelDisplay").style.display="none";
		document.getElementById("vacationDisplay").style.display="none";
		document.getElementById("activityDisplay").style.display="none";
		document.getElementById("airDisplay").style.display="block";
		document.getElementById("carDisplay").style.display="none";
		document.forms["form1"].air_Loc_mul_0.value="City or Airport";
		document.forms["form1"].air_Loc1_mul_0.value="City or Airport";
		//document.forms["form1"].Air_RetTime_a.value="AnyTime";
		//document.forms["form1"].Air_DepTime_a.value="AnyTime";
		document.forms["form1"].Air_cmbNoOfAdults.value="1";
		document.forms["form1"].elements["Aira_FlightClass"][0].checked="1";
		document.forms["form1"].elements["Aira_FlightClass"][1].checked="";
		document.forms["form1"].elements["Aira_FlightClass"][2].checked="";
		document.forms["form1"].elements["Aira_FlightClass"][3].checked="";
		document.forms["form1"].Air_cmbNoOfSeniors.value="0";
		document.forms["form1"].Air_cmbNoOfChildren.value="0";
		document.forms["form1"].Air_cmbNoOfInfants.value="0";
		document.forms["form1"].elements["hid_H_Loc"].value="";
		document.forms["form1"].elements["hid_A_Loc"].value="";
		document.forms["form1"].elements["hid_car_Loc"].value="";
		document.forms["form1"].elements["hid_air_Loc_mul_0"].value="";
		document.forms["form1"].elements["hid_air_Loc1_mul_0"].value="";				
		document.forms["form1"].elements["V_DepFromHid"].value="";		
		document.forms["form1"].elements["V_RetLocHid"].value="";
		document.forms["form1"].elements["hid_car_Loc1"].value="";	
		document.forms["form1"].elements["nonStopStatusNew"].checked="";
		document.forms["form1"].elements["air0_nights"].value="1";
		
	}
}
function search(){
	document.forms["ResPkgSearchForm"].elements["pkgType"].value=document.forms["form1"].pkgType.value;
	////alert("XXX" + document.forms["form1"].pkgType.value); 
	eval("loadDataNew_"+document.forms["form1"].pkgType.value+"()");
}
getDtFromOriginalObjcts("air0","_In");
getDtFromOriginalObjcts("air0","_Out");
getDtFromOriginalObjcts("air1","_In");
getDtFromOriginalObjcts("air2","_In");
getDtFromOriginalObjcts("air3","_In");
getDtFromOriginalObjcts("air4","_In");
getDtFromOriginalObjcts("car","_In");
getDtFromOriginalObjcts("car","_Out");
getDtFromOriginalObjcts("H","_In");
getDtFromOriginalObjcts("H","_Out");
getDtFromOriginalObjcts("activity","_In");
getDtFromOriginalObjcts("activity","_Out");
setEngType('F');
var elem = document.getElementsByName("engType");

for(var ii=0; ii<elem.length;ii++){
	if(elem[ii].value=='F'){
		elem[ii].checked=true;
	}
}
function flexset(){
	var elem_f=document.getElementsByName("isflex");
	for(var j=0; j<elem_f.length;j++){
		if(elem_f[j].id=='exact'){
			elem_f[j].checked=true;
		}
	}
}
