<!--
function findPos(){
  if(bw.ns4){   //Netscape 4
    x = document.layers.layerMenu.pageX;
    y = document.layers.layerMenu.pageY;
  }else{ //other browsers
    x=0; y=0; var el,temp
    el = bw.ie4?document.all["divMenu"]:document.getElementById("divMenu");
    if(el.offsetParent){
      temp = el
      while(temp.offsetParent){ //Looping parent elements to get the offset of them as well
        temp=temp.offsetParent; 
        x+=temp.offsetLeft
        y+=temp.offsetTop;
      }
    }
    x+=el.offsetLeft + 1
    y+=el.offsetTop
  }
  //Returning the x and y as an array
  if (bw.moz) { y = y - 11; x = x - 2; }
  return [x,y]
  //return [258, 116];
}

pos = findPos() ;
			
//Menu object creation
oCMenu=new makeCM("oCMenu"); //Making the menu object. Argument: menuname
oCMenu.fromLeft=pos[0]
oCMenu.fromTop=pos[1]
oCMenu.onresize="pos = findPos();oCMenu.fromLeft=pos[0];oCMenu.fromTop=pos[1];"

//Menu properties   
oCMenu.pxBetween=1;
oCMenu.rows=0;
oCMenu.menuPlacement=1;                                                        
oCMenu.offlineRoot="/" ;
oCMenu.onlineRoot="";
oCMenu.resizeCheck=1;
oCMenu.wait=300;
//oCMenu.fillImg="/images/menu/cm_fill.gif";
oCMenu.zIndex=90;

//Background bar properties
//oCMenu.useBar=0;
//oCMenu.barWidth="menu";
//oCMenu.barHeight="menu" ;
//oCMenu.barClass="clBar";
oCMenu.barX="menu";
oCMenu.barY="menu";
oCMenu.barBorderX=0;
oCMenu.barBorderY=0;
oCMenu.barBorderClass="";

//Level properties - ALL properties have to be spesified in level 0
oCMenu.level[0]=new cm_makeLevel(); //Add this for each new level
oCMenu.level[0].width=83;
oCMenu.level[0].height=20;
oCMenu.level[0].regClass="clLevel0";
oCMenu.level[0].overClass="clLevel0over";
oCMenu.level[0].borderX=0;
oCMenu.level[0].borderY=0;
oCMenu.level[0].borderClass="clLevel0border";
oCMenu.level[0].offsetX=0;
oCMenu.level[0].offsetY=23;
oCMenu.level[0].rows=0;
//oCMenu.level[0].align="bottom";
//oCMenu.level[0].filter="progid:DXImageTransform.Microsoft.Fade(duration=0.4)" ;
//oCMenu.level[0].arrow="/images/arrow_red.gif";
//oCMenu.level[0].slidepx=10;
//oCMenu.level[0].slidetim=10;

oCMenu.level[1]=new cm_makeLevel(); //Add this for each new level
oCMenu.level[1].width=170;
oCMenu.level[1].height=20;
oCMenu.level[1].regClass="clLevel1";
oCMenu.level[1].overClass="clLevel1over";
oCMenu.level[1].borderClass="clLevel1border";
//oCMenu.level[1].align="right";
oCMenu.level[1].offsetX=175;
oCMenu.level[1].offsetY=0;
oCMenu.level[1].rows=0;
//oCMenu.level[1].arrow="/images/arrow_red.gif";
//oCMenu.level[1].arrowWidth=5;
//oCMenu.level[1].arrowHeight=5;

oCMenu.level[2]=new cm_makeLevel(); //Add this for each new level
oCMenu.level[2].width=170;
oCMenu.level[2].height=20;
oCMenu.level[2].regClass="clLevel1";
oCMenu.level[2].overClass="clLevel1over";
oCMenu.level[2].borderClass="clLevel1border";
//oCMenu.level[2].align="right";
oCMenu.level[2].offsetX=175;
oCMenu.level[2].offsetY=0;
oCMenu.level[2].rows=0;
//oCMenu.level[2].arrow="/images/arrow_red.gif";
//oCMenu.level[2].arrowWidth=5;
//oCMenu.level[2].arrowHeight=5;

/******************************************
Menu item creation:
myCoolMenu.makeMenu(name, parent_name, text, link, target, width, height, regImage, overImage, regClass, overClass , align, rows, nolink, onclick, onmouseover, onmouseout) 
*************************************/

<?php
include ($_SERVER["DOCUMENT_ROOT"] . "/admin/includes/connections.php");
include ($_SERVER["DOCUMENT_ROOT"] . "/admin/includes/functions_database.php");
include ($_SERVER["DOCUMENT_ROOT"] . "/admin/includes/functions_string.php");
include ($_SERVER["DOCUMENT_ROOT"] . "/admin/includes/functions_menus.php");

$Current_URL = $_SERVER['URL'];
echo doBuildMenu($Current_URL)
?>

oCMenu.menuPlacement=1;
oCMenu.fromLeft=pos[0]
oCMenu.fromTop=pos[1]
oCMenu.rows=1;
oCMenu.construct()
oCMenu.rows=0;
oCMenu.pxBetween=5;

//-->
